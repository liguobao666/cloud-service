package com.dh.pas;

public class PassWordUtil {

    /**
     * @param mnBoardNo 版本号
     * @param mnBreakDay 发作日
     * @param mnGapCount 剩余的密码个数
     * @param mGaps 发作时间间隔
     * @param mnMachineId 特征码
     */
    public static void pass(int mnBoardNo,int mnBreakDay,
                            int mnGapCount,char[] mGaps,int mnMachineId){
        // 保存信息
        PasswordInfo PwdInfo=new PasswordInfo();
        // 保存板号
        int nMainBoardId = mnBoardNo;

        // 保存发作日
        PwdInfo.ManuPasswordinfo.iBreakDay = mnBreakDay;

        // 保存日期信息
        PwdInfo.ManuPasswordinfo.iPasswordCount = mnGapCount;
        for (int i = 0; i < mnGapCount; ++i)
        {
            PwdInfo.ManuPasswordinfo.Gaps[i] = mGaps[i];
        }
        // 特征码
        PwdInfo.iMachineId = mnMachineId;
    }
}
