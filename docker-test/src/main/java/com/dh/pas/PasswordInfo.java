package com.dh.pas;

public class PasswordInfo {

    int iCompanySign;					//公司标志

    GROUPPASSWORDINFO2 ManuPasswordinfo; //厂家密码信息
    //GROUPPASSWORDINFO AgentPasswordinfo;//代理密码信息

    int iMachineTypeSign;      //机型标志(8090)
    //unsigned int reserved[2];

    int iMachineId;            //特征码

    int iFactoryId;            //厂家Id
    //char filled[2];			//为了对齐保留

    short iCheckSum;			//校验码

    public int getiCompanySign() {
        return iCompanySign;
    }

    public void setiCompanySign(int iCompanySign) {
        this.iCompanySign = iCompanySign;
    }

    public GROUPPASSWORDINFO2 getManuPasswordinfo() {
        return ManuPasswordinfo;
    }

    public void setManuPasswordinfo(GROUPPASSWORDINFO2 manuPasswordinfo) {
        ManuPasswordinfo = manuPasswordinfo;
    }

    public int getiMachineTypeSign() {
        return iMachineTypeSign;
    }

    public void setiMachineTypeSign(int iMachineTypeSign) {
        this.iMachineTypeSign = iMachineTypeSign;
    }

    public int getiMachineId() {
        return iMachineId;
    }

    public void setiMachineId(int iMachineId) {
        this.iMachineId = iMachineId;
    }

    public int getiFactoryId() {
        return iFactoryId;
    }

    public void setiFactoryId(int iFactoryId) {
        this.iFactoryId = iFactoryId;
    }

    public short getiCheckSum() {
        return iCheckSum;
    }

    public void setiCheckSum(short iCheckSum) {
        this.iCheckSum = iCheckSum;
    }

    class GROUPPASSWORDINFO2{
        int iHavePassword;				// 有无密码标志
        int iTotalPassword;				// 总密码

        char[] Gaps=new char[PassConstact.MAX_OUTBREAKTIME_NUM];		 // 发作时间月份间隔数
        int[] iOutBreakTimePassword=new int[PassConstact.MAX_OUTBREAKTIME_NUM]; // 发作时间解密密码

        int iSysTime;			// 设置该组密码时的标准备时间
        int iTimeDiff;					// 设置该组密码时的显示时间与标准时间的时间差
        int iBreakDay;			// 密码发作日
        int[] Reserved=new int[6];		// 保留，暂写死为0

        int iPasswordCount;				// 剩余的密码个数
        int iNextOutBreakTimeIndex;		// 下一次发作时间在数组中的序号
        int NextOutBreakTime;	// 下一次发作时间
        int iDeferOutBreakSwitch;       // 暂缓执行开关

        public int getiHavePassword() {
            return iHavePassword;
        }

        public void setiHavePassword(int iHavePassword) {
            this.iHavePassword = iHavePassword;
        }

        public int getiTotalPassword() {
            return iTotalPassword;
        }

        public void setiTotalPassword(int iTotalPassword) {
            this.iTotalPassword = iTotalPassword;
        }

        public char[] getGaps() {
            return Gaps;
        }

        public void setGaps(char[] gaps) {
            Gaps = gaps;
        }

        public int[] getiOutBreakTimePassword() {
            return iOutBreakTimePassword;
        }

        public void setiOutBreakTimePassword(int[] iOutBreakTimePassword) {
            this.iOutBreakTimePassword = iOutBreakTimePassword;
        }

        public int getiSysTime() {
            return iSysTime;
        }

        public void setiSysTime(int iSysTime) {
            this.iSysTime = iSysTime;
        }

        public int getiTimeDiff() {
            return iTimeDiff;
        }

        public void setiTimeDiff(int iTimeDiff) {
            this.iTimeDiff = iTimeDiff;
        }

        public int getiBreakDay() {
            return iBreakDay;
        }

        public void setiBreakDay(int iBreakDay) {
            this.iBreakDay = iBreakDay;
        }

        public int[] getReserved() {
            return Reserved;
        }

        public void setReserved(int[] reserved) {
            Reserved = reserved;
        }

        public int getiPasswordCount() {
            return iPasswordCount;
        }

        public void setiPasswordCount(int iPasswordCount) {
            this.iPasswordCount = iPasswordCount;
        }

        public int getiNextOutBreakTimeIndex() {
            return iNextOutBreakTimeIndex;
        }

        public void setiNextOutBreakTimeIndex(int iNextOutBreakTimeIndex) {
            this.iNextOutBreakTimeIndex = iNextOutBreakTimeIndex;
        }

        public int getNextOutBreakTime() {
            return NextOutBreakTime;
        }

        public void setNextOutBreakTime(int nextOutBreakTime) {
            NextOutBreakTime = nextOutBreakTime;
        }

        public int getiDeferOutBreakSwitch() {
            return iDeferOutBreakSwitch;
        }

        public void setiDeferOutBreakSwitch(int iDeferOutBreakSwitch) {
            this.iDeferOutBreakSwitch = iDeferOutBreakSwitch;
        }
    }
}
