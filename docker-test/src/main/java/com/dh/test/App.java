package com.dh.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
@EnableCaching
public class App {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(App.class);
		ApplicationContext ctx = app.run(args);
	}


}
