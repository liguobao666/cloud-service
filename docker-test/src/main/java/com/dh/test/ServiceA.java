package com.dh.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class ServiceA {

    @Value("${com.lgb.flag}")
    private String str;

    Log log = LogFactory.getLog(ServiceA.class);

    @Cacheable(value = "usercache",key = "#id")
    public String get(String id){
        log.info("无缓存的时候调用这里");
        return id;
    }

    public String getStr(){
        return str;
    }

}
