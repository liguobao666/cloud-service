package com.dh.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerTest {

    @Autowired
    private ServiceA serviceA;

    @GetMapping("/getCache")
    public String getCache(){
        return serviceA.get("lgb");
    }

    @GetMapping("/getStr")
    public String getStr(){
        return serviceA.getStr();
    }
}
