Dockerfile Maven 插件使用

（注意：原来的项目docker-maven-plugin 已经不建议使用）

参考资料

    https://github.com/spotify/dockerfile-maven

构建命令：

    mvn clean install  dockerfile:build dockerfile:push

设计目标：

    不要试图做任何事情。 这个插件使用Dockerfiles构建Docker项目的而且是强制性的。
    将Docker构建过程集成到Maven构建过程中。如果绑定默认phases，那么当你键入mvn package时，你会得到一个Docker镜像。 当你键入mvn deploy时，你的图像被push。
    让goals记住你在做什么。 你可以输入 mvn dockerfile:build及后面的 mvn dockerfile:build和mvn dockerfile:push 都没有问题。这也消除了之前像 mvn dockerfile:build -DalsoPush这样的命令；相反，你可以只使用 mvn dockerfile:build dockerfile:push。
    与Maven build reactor集成。你可以在一个项目中依赖另一个项目所构建的Docker image，Maven将按照正确的顺序构建项目。当你想要运行涉及多个服务的集成测试时，这非常有用。
    该项目遵守 Open Code of Conduct.。 参与贡献代码，你需要遵守此代码规则。


Set-up

    该插件需要Java 7或更高版本以及Apache Maven 3或更高版本。
        要运行集成测试或在开发中使用该插件，需要有一个能正常工作的Docker。

优点

    使用这个插件进行项目构建有很多优点。
    1:更快的构建时间
    2:这个插件让你更好地利用Docker缓存，通过让你在你的image中缓存Maven依赖关系，极大地加速你的构建。 它还鼓励避免 maven-shade-plugin，这也大大加快了构建速度。
    4:一致的构建生命周期
        你不再需要像下面这样了：
        mvn package
        mvn dockerfile:build
        mvn verify
        mvn dockerfile:push
        mvn deploy
        用下面这一行命令就可以了：mvn deploy
            
    通过基本配置，这将确保image在正确的时间被构建和push。


身份验证和私有Docker注册中心支持：

    从版本1.3.0开始，当你pulling, pushing, 或 building images 到private registries
    中时插件将自动使用 ~/.dockercfg 或 ~/.docker/config.json文件中的配置。
   
    1：使用maven settings.xml进行身份验证
  
       pom文件中的配置
        <plugin>
            <groupId>com.spotify</groupId>
            <artifactId>dockerfile-maven-plugin</artifactId>
            <version>1.4.10</version>
            <executions>
                <execution>
                    <id>default</id>
                    <goals>
                        <goal>build</goal>
                        <goal>push</goal>
                    </goals>
                </execution>
            </executions>
            <configuration>
                <repository>liguobao111/test</repository>
                <tag>docker-test.${project.version}</tag>
                <dockerfile>./</dockerfile>
                <buildArgs>
                    <JAR_FILE>target/${project.build.finalName}.jar</JAR_FILE>
                </buildArgs>
                <useMavenSettingsForAuth>true</useMavenSettingsForAuth>
            </configuration>
        </plugin>
        
       然后，在你的maven settings.xml中为服务器添加配置：
      
       <servers>
         <server>
           <id>docker.io</id>
           <username>me</username>
           <password>mypassword</password>
         </server>
       </servers>
   
   
    2：使用maven pom.xml进行身份验证
        从版本1.3.XX开始，你可以使用pom本身的配置进行身份验证。 只需添加类似于以下配置（
            经测试，1.4.0版本Windows10下向私有仓库push时会报错 denied: requested access to the resource is denied，
            所以建议使用maven settings.xml进行身份验证；但Linux环境该方案可行 ）：  
           <plugin>
               <groupId>com.spotify</groupId>
               <artifactId>dockerfile-maven-plugin</artifactId>
               <version>1.4.10</version>
               <executions>
                   <execution>
                       <id>default</id>
                       <goals>
                           <goal>build</goal>
                           <goal>push</goal>
                       </goals>
                   </execution>
               </executions>
               <configuration>
                   <username>repoUserName</username>
                   <password>repoPassword</password>
                   <repository>${docker.image.prefix}/${project.artifactId}</repository>
                   <buildArgs>
                       <JAR_FILE>target/${project.build.finalName}.jar</JAR_FILE>
                   </buildArgs>
               </configuration>
           </plugin>
           
           
           
docker run -u root -d -p 8070:8080 -p 50000:50000 -v jenkins-data:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock  jenkinsci/blueocea


Jenkins 构建上传私有云

    1：修改如下文件
    vi /usr/lib/systemd/system/docker.service
    ExecStart=/usr/bin/dockerd --insecure-registry 192.168.7.197
    
    或者：修改/etc/docker/daemon.json
        "insecure-registries": ["192.168.7.197"]
     
    3:修改文件权限
        chmod 777 /var/run/docker.sock