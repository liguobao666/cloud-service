package com.sm.server;

import com.sm.server.config.HostAddrKeyResolver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

/**
 * Route（路由）：这是网关的基本构建块。它由一个 ID，一个目标 URI，一组断言和一组过滤器定义。如果断言为真，则路由匹配。
 * Predicate（断言）：这是一个 Java 8 的 Predicate。输入类型是一个 ServerWebExchange。我们可以使用它来匹配来自 HTTP 请求的任何内容，例如 headers 或参数。
 * Filter（过滤器）：这是org.springframework.cloud.gateway.filter.GatewayFilter的实例，我们可以使用它修改请求和响应。
 *
 *Predicate    dateTime   AfterRoutePredicateFactory 请求时间满足在配置时间之后
 *                        BeforeRoutePredicateFactory 请求时间满足在配置时间之前
 *                        BetweenRoutePredicateFactory 请求时间满足在配置时间之间
 *
 *             Cookie     CookieRoutePredicateFactory  请求指定Cookie正则匹配指定值
 *
 *             Header     HeaderRoutePredicateFactory 请求指定Header正则匹配指定值
 *                        CloueFoundryRouteServiceRoutePredicateFactory 请求Header是否包含指定的名称
 *
 *             Host       HostRoutePredicateFactory 请求Host匹配指定值
 *
 *             Method     MethodRoutePredicateFactory  请求method匹配指定的method
 *
 *             Path       PathRoutePredicateFactory 请求路劲正则匹配指定值
 *
 *             Queryparam QueryRoutePredicateFactory 请求查询参数正则匹配指定值
 *
 *             RemoteAddr RemoteAddrRoutePredicateFactory 请求远程地址匹配指定值
 *
 */
@SpringBootApplication
public class GateWayApp {
    public static void main(String[] args) {
        SpringApplication.run(GateWayApp.class, args);
    }

    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        //http://localhost:8080/about时会自动转发到地址：http://www.ityouknow.com/about
        return builder.routes()
                .route("path_route", r -> r.path("/about")
                        .uri("http://ityouknow.com"))
                .build();
    }

    @Bean
    HostAddrKeyResolver hostAddrKeyResolver(){
        return new HostAddrKeyResolver();
    }
}
