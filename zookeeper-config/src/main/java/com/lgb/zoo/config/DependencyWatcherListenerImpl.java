package com.lgb.zoo.config;

import org.springframework.cloud.zookeeper.discovery.watcher.DependencyState;
import org.springframework.cloud.zookeeper.discovery.watcher.DependencyWatcherListener;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DependencyWatcherListenerImpl implements DependencyWatcherListener {

    /**
     * spring.cloud.zookeeper.dependencies：如果不设置此属性，则不能使用 Zookeeper 依赖项。
     * @param dependencyName
     * @param newState
     */
    @Override
    public void stateChanged(String dependencyName, DependencyState newState) {
        System.out.println(dependencyName);
        newState.name();
    }
}
