package com.lgb.zoo.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.netflix.ribbon.RibbonLoadBalancerClient;
import org.springframework.cloud.netflix.ribbon.SpringClientFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    /**
     * LoadBalancerClient 注入LoadBalancerClient，否则spring.cloud.zookeeper.dependencies.path 无法路由
     * @param factory
     * @return
     */
    @Bean
    public LoadBalancerClient loadBalancerClient(SpringClientFactory factory) {
        return new RibbonLoadBalancerClient(factory);
    }

    @Bean
    public SpringClientFactory springClientFactory(){
       return  new SpringClientFactory();
    }


}
