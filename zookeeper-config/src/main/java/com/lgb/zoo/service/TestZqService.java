package com.lgb.zoo.service;

import com.lgb.zoo.mapper.TestZqMapper;
import com.lgb.zoo.model.TestZq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TestZqService {
    @Autowired
    TestZqMapper testZqMapper;

    @Transactional
    public void get(){
        TestZq zq=testZqMapper.selectByPrimaryKey(1);
        zq.setTestId(zq.getTestId()+20);
        testZqMapper.updateByPrimaryKey(zq);
        System.out.println("123123123123");
    }
}
