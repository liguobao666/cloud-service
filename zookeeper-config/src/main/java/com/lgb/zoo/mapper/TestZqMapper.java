package com.lgb.zoo.mapper;

import com.lgb.zoo.model.TestZq;
import org.springframework.stereotype.Repository;

@Repository
public interface TestZqMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TestZq record);

    int insertSelective(TestZq record);

    TestZq selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TestZq record);

    int updateByPrimaryKey(TestZq record);
}