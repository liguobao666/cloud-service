package com.lgb.zoo.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("bbb")
public interface NewsletterService {
    @RequestMapping(method = RequestMethod.GET, value = "/test")
    String getNewsletters();
}