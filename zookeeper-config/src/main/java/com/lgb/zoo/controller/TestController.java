package com.lgb.zoo.controller;

import com.lgb.zoo.service.TestZqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class TestController {

    @Value("${dev.debug}")
    private String debug;

    @Value("${debug}")
    private String debug1;
    @Autowired
    NewsletterService newsletterService;

    @Autowired
    TestZqService testZqService;
    @RequestMapping("/")
    public String home() {
        return newsletterService.getNewsletters();
    }

    @RequestMapping("/get")
    public String config() {
        return debug+"......"+debug1;
    }

    @RequestMapping("/get/zq")
    public String testZq(){
        testZqService.get();
        return "success";
    }



}
