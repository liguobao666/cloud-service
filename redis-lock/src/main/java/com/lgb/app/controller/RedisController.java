package com.lgb.app.controller;

import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/v1/test")
public class RedisController {

    private  static String lockKey="lock";
    @Autowired
    private RedissonClient redissonClient;

    //jvm 内存，记录库存是否存在，不用去链接数据库判断
    private ConcurrentHashMap<String,Boolean> concurrentHashMap=new ConcurrentHashMap();

    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @RequestMapping(value = "/add")
    public String saveRedis() {
        concurrentHashMap.put("liguobao",true);
        stringRedisTemplate.opsForValue().set("store","100");
        return "success";
    }

    /**
     * redission框架实现分布式锁
     * @return
     */
    @RequestMapping(value = "/deduct")
    public String deduct(){
        //setNx,自己加的锁只能自己释放
        String threadid=Thread.currentThread().getId()+"";
        try {
            //某个线程执行时间很长，超过30s时如何解决：后台启动一个线程，每隔10s检查锁是否存在，进行锁续秘。
            Boolean b=stringRedisTemplate.opsForValue().setIfPresent(lockKey,threadid,30, TimeUnit.SECONDS);
            if (!b){
                return "请稍后重试";
            }
            int stock=Integer.valueOf(stringRedisTemplate.opsForValue().get("store"));
            if (stock>0){
                int relStock=stock-1;
                stringRedisTemplate.opsForValue().set("store",relStock+"");
                System.out.println("剩余库存"+relStock);
            }else {
                System.out.println("库存不足");
            }
            return "success";
        }finally {
            //自己的锁只能自己释放
            if (threadid.equals(stringRedisTemplate.opsForValue().get(lockKey))){
                stringRedisTemplate.delete(lockKey);
            }
        }
    }

    /**
     * 使用redis 主从架构会有问题，当主节点加锁成功，锁信息还没同步到从节点时主节点挂掉，从节点被选举为主节点，
     * 但是之前线程还没执行完成，也会存在并发问题。
     * redLock
     * @return
     */
    @RequestMapping(value = "/deduct1")
    public String deduct1(){
        /**
         *使用JVM内存，可以减少连接redis的时间，大幅提高程序性能，
         *但是有个问题，当用户退货时，JVM内存无法知道用户退货，内存中库存标记无法修改。
         *解决办法，可以在用户退货时，去修改jvm内存标记，但是只能修改到退货的JVM，对于其他JVM中库存标志无法修改
         *
         */
        if(!concurrentHashMap.get("liguobao")){
            return "fail:库存不足";
        }
        RLock rLoc=redissonClient.getLock(lockKey);//获取锁
        if(rLoc.tryLock()){//当库存不足的时候，每次还得去加锁，做减库存操作，浪费性能
            int stock=Integer.valueOf(stringRedisTemplate.opsForValue().get("store"));
            if (stock>0){
                int relStock=stock-1;
                stringRedisTemplate.opsForValue().set("store",relStock+"");
                System.out.println("剩余库存"+relStock);
            }else {
                concurrentHashMap.put("liguobao",false);
                System.out.println("库存不足");
            }
            rLoc.unlock();//解锁

            return "success";
        }
        return "fail";
    }
}
