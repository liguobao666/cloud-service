package com.dh.java8.methodReference;

public class CompareUtil {

    public int compareByScore(Student s1,Student s2){
        return  s1.getScore()-s2.getScore();
    }
}
