package com.dh.java8.date;


import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.Weeks;
import org.joda.time.format.DateTimeFormat;
import org.junit.Test;

import java.time.Clock;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import static java.lang.System.*;

public class JodaTime1 {

    @Test
    public void test1(){
        //今天
        DateTime today=new DateTime();
        //明天
        DateTime tomorrow=today.plusDays(1);

        out.println(today.toString("yyyy-MM-dd HH:mm:ss"));
        out.println(tomorrow.toString("yyyy-MM-dd HH:mm:ss"));
    }

    @Test
    public void test2(){
        DateTime today=new DateTime();
        //今天
        DateTime d1=today.withDayOfMonth(1);
        out.println(today.toString("yyyy-MM-dd HH:mm:ss"));
        out.println(d1.toString("yyyy-MM-dd HH:mm:ss"));
        out.println("----------------------");

        //当前时区的时间
        LocalDate localDate=new LocalDate();

        //当前时间加三个月，将这个月最后一天的时间设置为新的时间
        localDate=localDate.plusMonths(3).dayOfMonth().withMaximumValue();
        out.println(localDate);
        //当前时间的俩周后
        LocalDate localDate1=localDate.plusWeeks(2);
        System.out.println(localDate1);
        localDate.plus(Weeks.weeks(1));
        out.println("----------------------");
        //当前时间减一年
        localDate.minusYears(1);
        out.println("---------时钟-------------");
        Clock clock=Clock.systemDefaultZone();

        System.out.println("-----------------------");
        LocalDate localDate2=LocalDate.now();

        Period period= Period.weeks(1);
        System.out.println(period.toString());


    }

    /**
     * 计算俩年前第三个月最后一天的日期
     */
    @Test
    public void test3(){
        DateTime today=new DateTime();
        DateTime dateTime=today.minusYears(2).monthOfYear().
                setCopy(3).dayOfMonth().withMaximumValue();
        out.println(dateTime.toString("yyyy-MM-dd"));
    }

    /**
     * UTC时间比北京时间早8小时
     */
    @Test
    public void test(){
        out.println(covertUTC2Date("2019-12-12 12:12:12.123"));
    }

    public static Date covertUTC2Date(String date){
        DateTime dateTime=DateTime.parse(date, DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
        return  dateTime.toDate();
    }

    /**
     * 时区
     */
    @Test
    public void test5(){
        Set<String> set= ZoneId.getAvailableZoneIds();
        Set<String> treeSet=new TreeSet<String>(){
            {
                addAll(set);
            }
        };

        System.out.println("==============");
        YearMonth ye=YearMonth.now();
        ye.lengthOfMonth();//月有几天
        ye.isLeapYear();//是不是闰年


    }
}
