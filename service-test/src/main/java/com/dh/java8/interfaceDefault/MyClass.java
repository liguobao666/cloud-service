package com.dh.java8.interfaceDefault;

public class MyClass extends Interface1Impl implements Interface2{

   /* @Override
    public void a() {
        //这个supper 是Interface1Impl，因为java约定 extends的优先级比 interface高
        super.a();
    }*/

    public static void main(String[] args) {
        MyClass myClass=new MyClass();
        myClass.a();
    }
}
