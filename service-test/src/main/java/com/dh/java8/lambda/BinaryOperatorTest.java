package com.dh.java8.lambda;

import java.util.Comparator;
import java.util.function.BinaryOperator;

public class BinaryOperatorTest {

    public static void main(String[] args) {
        BinaryOperatorTest test=new BinaryOperatorTest();
        System.out.println(test.getvalue(1,2,(a,b)-> a+b));

        System.out.println(test.minBy(1,2, Integer::compareTo));
    }

    public int getvalue(int a, int b, BinaryOperator<Integer> binaryOperator){
        return binaryOperator.apply(a,b);
    }

    public int minBy(int a, int b, Comparator<Integer> comparable){
        return  BinaryOperator.minBy(comparable).apply(a,b);
    }
}
