package com.dh.java8.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class PredicateTest {
    public static void main(String[] args) {
        /*Predicate<String> p= a->{
            if(a.equals("false"))
                return false;
            return true;
        };*/
        List<Integer> list= Arrays.asList(1,2,3,4,5,6,7,8,9,10);
        PredicateTest test=new PredicateTest();
        test.get(list, a -> {if(a>2) return true;else return false;});
        System.out.println("--------------------------------");
        test.get(list , item -> true);

        System.out.println("-------------------------------");

        System.out.println(test.isequal("123").test("123"));
    }

    public void  get(List<Integer> list,Predicate<Integer> predicate){
        list.forEach( model ->{
            if(predicate.test(model)){
                System.out.println(model);
            }
        });
    }

    public void conditonFilter(List<Integer> list,Predicate<Integer> predicate,
                         Predicate<Integer> predicate2){
           for(Integer integer:list){
               if(predicate.and(predicate2).test(integer)){
                   System.out.println(integer);
               }
           }
    }

    public Predicate<String> isequal(Object o){
        return  Predicate.isEqual(o);
    }
}
