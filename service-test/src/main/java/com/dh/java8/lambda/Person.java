package com.dh.java8.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class Person {

    private String username;

    private int age;

    public Person(String username, int age) {
        this.username = username;
        this.age = age;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }

    public static void main(String[] args) {
        Person person1=new Person("张三",20);
        Person person2=new Person("lisi",30);
        Person person3=new Person("wangwu",30);

        List<Person> list=Arrays.asList(person1,person2,person3);

        List<Person> liss=gerPersonsName("lisi",list);
        liss.forEach(person -> System.out.println(person.getUsername()));

        System.out.println("--------------------------------------");
        List<Person> li1= getPersonAge(20,list,
                (a,list1)->  list1.stream().filter(
                        person -> person.getAge()==a
                ).collect(Collectors.toList()));
        li1.forEach(person -> System.out.println(person.getUsername()));
    }

    public static List<Person> gerPersonsName(String username,List<Person> persons){
        return persons.stream().filter(
                person -> {return person.getUsername().equals(username);}
                )
                .collect(Collectors.toList());
    }

    public static List<Person> gerPersonsAge(Integer age,List<Person> persons){
        BiFunction<Integer,List<Person>,List<Person>> biFunction=(a,b)->{
           return persons.stream().filter(
                        person ->  person.getAge()==age
                    )
                   .collect(Collectors.toList());

        };
        return  biFunction.apply(age,persons);
    }

    public static List<Person> getPersonAge(Integer age,List<Person> list,BiFunction<Integer,List<Person>,List<Person>> biFunction){
       return biFunction.apply(age,list);
    }
}
