package com.dh.java8.stream2;

import com.dh.queue.BlockingQueueTest;

import java.util.function.Consumer;
import java.util.function.IntConsumer;

public class ConsumerTest {
    public static void main(String[] args) {
        ConsumerTest test=new ConsumerTest();

        Consumer<Integer> consumer=(a)->{
            System.out.println(a);
        };

        IntConsumer intConsumer=(a)->{
            System.out.println(a);
        };

        System.out.println(consumer instanceof IntConsumer);

        test.test(consumer);
        test.test(consumer::accept);
        test.test(intConsumer::accept);
    }

    public void test(Consumer<Integer> consumer){
        consumer.accept(100);
    }
}
