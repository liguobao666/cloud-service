package com.dh.java8.stream2;

public class LambdaTest {
    Runnable r1 = ()->System.out.println(this);

    Runnable r2 = new Runnable() {
        public void run() {
            System.out.println(this);
        }
    };

    /**
     * lambda 表达式与匿名内部类的区别
     * 虽然他们能完成相容的事情，但是对于匿名内部类来说它是开辟了一块新的空间
     * lambda还是用的外部的空间
     *
     * @param args
     */
    public static void main(String[] args) {
        LambdaTest lambdaTest=new LambdaTest();

        //com.dh.java8.stream2.LambdaTest@1297b437
        //这里的this是LambdaTest 这个类的实例
        Thread t1=new Thread(lambdaTest.r1);
        t1.start();
        System.out.println("--------");

        //com.dh.java8.stream2.LambdaTest$1@176a36d8
        //$1 表示一个匿名内部类，这是输出的是LambdaTest的一个内部类对象
        Thread t2=new Thread(lambdaTest.r2);
        t2.start();

    }

}
