package com.dh.java8.stream2;

import com.sun.xml.internal.ws.api.pipe.helper.AbstractPipeImpl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Stream2 {
    public static void main(String[] args) {
        List<Integer> list= Arrays.asList(2,3,4,5,6);
        List<Integer> li=list.stream().map(i->i*2).filter(i->i>4).collect(Collectors.toList());
        li.forEach(System.out::println);

    }
}
