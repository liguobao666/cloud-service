package com.dh.java8.optional;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class OptionalTest {
    public static void main(String[] args) {
        //不推介的用法
        Optional<String> optional=Optional.of("hello");
        if(optional.isPresent()){
            System.out.println(optional.get());
        }
        //之前
        String str="";
        if(null!=str){
            System.out.println(str);
        }
        //推介使用
        optional.ifPresent(item -> System.out.println(str));
        //如果为空的话返回一个值
        optional.orElse("wprld");

        optional.orElseGet(()->"123123");
    }
    public void a(){
       Company company=new Company();
       company.setName("A公司");
           Employee employee=new Employee();
           employee.setName("zhagnsna");
           Employee employee1=new Employee();
           employee1.setName("李四");
       List<Employee> li=Arrays.asList(employee,employee1);
       company.setEmployeeList(li);


       List<Employee> list=company.getEmployeeList();

       Optional<Company> optional=Optional.of(company);

       List<Employee> lis= optional.map(theCompany -> theCompany.getEmployeeList()).orElse(Collections.emptyList());
    }
}
