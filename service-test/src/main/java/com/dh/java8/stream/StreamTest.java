package com.dh.java8.stream;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 流操作包含三部分
 *   1：源
 *   2：零个或多个中间操作
 *   3：终止操作
 *  流操作的分类
 *      1:lazy求职   stream.xxx().yyy().count() count()为终止操作，
 *                  只有终止操作触发中间的 xxx().yyy()的中间操作才会触发
 *      2:及早求职   count()为及早求职
 *
 *  流支持并行化：多个线程可以同时并行处理
 *
 */
public class StreamTest {

    /**
     * 流的创建方式
     */
    @Test
    public  void demo1(){
        //创建流方式1
        Stream stream1=Stream.of("hello","world","stream");

        //创建流方式2
        String[] arr=new String[]{"hello","world","stream"};
        Stream stream2=Stream.of(arr);

        //创建流方式3
        Stream stream3= Arrays.stream(arr);

        //创建流方式4
        List<String> list=Arrays.asList(arr);
        Stream stream4=list.stream();
   }

   @Test
   public  void  demo2(){
       IntStream.of(new int[]{1,2,3}).forEach(System.out::println);
       System.out.println("-------------");
       IntStream.range(3,8).forEach(System.out::println);
       System.out.println("-------------");
       IntStream.rangeClosed(3,8).forEach(System.out::println);
   }

    /**
     * 需求有一个集合，每一个整数乘以2，然后把每个乘以2的结果相加作
     */
    @Test
   public  void  demo3(){
       List<Integer> list=Arrays.asList(1,2,3,4,5,6,7,8,9);
       //原:list 中间操作：map  终止操作：reduce();
       System.out.println(list.stream().map(i->2*i).reduce(0,Integer::sum));
       System.out.println("----------------------");
       list.forEach(System.out::println);
   }

    @Test
   public void demo4(){
        Stream<Integer> stream=Stream.iterate(1,item->item+2).limit(6);
        //stream.forEach(System.out::println);
       //stream 值大2(过滤)，然后值乘以2（映射map），忽略掉前俩个元素
       System.out.println(stream.filter(item->item>2).mapToInt(item->item*2).skip(2).limit(2).sum());
   }


}
