package com.dh.java8.stream;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 并行流，串行流基本使用
 */
public class StreamTest4 {

    /**
     * 串行流，一个线程
     */
    @Test
    public void test1(){
        List<String> list=new ArrayList<>(5000000);
        for(int i=0;i<5000000;i++){
            list.add(UUID.randomUUID().toString());
        }
        System.out.println("开始排序");
        long startaTime=System.nanoTime();
        list.stream().sorted().count();
        long endRime=System.nanoTime();
        System.out.println(TimeUnit.NANOSECONDS.toMillis(endRime-startaTime));
    }

    /**
     * 并行流，
     */
    @Test
    public void test2(){
        List<String> list=new ArrayList<>(5000000);
        for(int i=0;i<5000000;i++){
            list.add(UUID.randomUUID().toString());
        }
        System.out.println("开始排序");
        long startaTime=System.nanoTime();
        list.parallelStream().sorted().count();
        long endRime=System.nanoTime();
        System.out.println(TimeUnit.NANOSECONDS.toMillis(endRime-startaTime));
    }

    /**
     * 输出hello 5，
     * 将中间操作封装到一个容器
     * 对每一个元素进行中间操作，元素之间会存在短路运算。
     * 对于本程序只要找到符合元素的，后面的元素都不会执行。
     *
     */
    @Test
    public  void test3(){
        List<String> list= Arrays.asList("hello","world","hello world");

        list.stream().mapToInt(item->{
            int lenth=item.length();
            System.out.println(item);
            return lenth;
        }).filter( length -> length==2).findFirst().ifPresent(System.out::println);


    }
}
