package com.dh.java8.stream;


import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamTest2 {

    @Test
    public void test1(){
        Stream<String> stream=Stream.of("hello","world","nihao");
        /*String[] string=stream.toArray(length -> new String[length]);
        Arrays.asList(string).forEach(System.out::println);*/
        String[] string1=stream.toArray(String[]::new);
        Arrays.asList(string1).forEach(System.out::println);
    }

    @Test
    public void test2(){
        Stream<String> stream=Stream.of("hello","world","nihao");

        //List<String> list=stream.collect(Collectors.toList());

        List<String> list =stream.collect(
                ()->new ArrayList(),
                (thelist,item)->thelist.add(item),//累加器，第一个参数是返回的集合，第二个参数是stream中的对象
                (list1,list2)->list1.addAll(list2)//第三个参数：将第二次遍历得到的listt添加到list1
        );

        List<String> list1=stream.collect(
                ArrayList::new,
                ArrayList::add,//累加器，第一个参数是返回的集合，第二个参数是stream中的对象
                ArrayList::addAll//第三个参数：将第二次遍历得到的listt添加到list1
        );

        list.forEach(System.out::println);
    }


}
