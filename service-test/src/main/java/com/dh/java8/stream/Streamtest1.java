package com.dh.java8.stream;

import com.dh.java8.collector.Student;
import com.sun.javafx.css.Combinator;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * Collector：一个可变的汇聚操作，将输入元素累积到可变容器中，
 * 在所有元素都处理完之后，将累积的结果转换为一个最终的表示（可选的），
 * 支持串行与并行俩种方式执行。
 *
 * 可变的汇聚操作的例子：
 *      1：将元素累计到Collection
 *      2：使用stringBuilder拼接字符串
 *      3：计算元素的求和，最大值，平均值等
 *      4：数据透视图（分组）
 * Collectors提供了关于Collector的常见汇聚实现（工厂）
 *
 * 一个Collector通过四个function协同工作，四个函数将条目累积到一个可变容器中。一个可选操作：将最终结果进行转换
 * 四个函数：1：一个结果容器supplier。
 *           2：将新的数据元素合并到结果容器accumulator(把一个元素添加到集合当中，这里是流中的每个原色)
 *           3:将俩个结果容器合并成一个combiner（并行流紧密相关）
 *                  combiner函数：有多个线程同时执行，就会生成四个结果，combiner将四个结果合并成一个
 *           4:可选操作，将中间的累积类型转换为最终的结果类型
 *
 * 一个串行实现会使用supplier创建一个单个容器，没一个输入元素都会调用accumulator一次
 *
 * 一个并行实现：对输入进行分区，针对每一分区都会创建一个结果容器，累积每一个分区的内容，
 * 使用combiner合并他们。
 *
 * 为了确保串行与并行操作结果的等价性，Collector需要满足
 *      同一性：a==combiner.apply(a, supplier.get()) a是部分累积的结果
 *      结合性：分割结合也会得到一个一样的结果
 *
 *      泛型
 *      T(流中的元素类型)
 *      A（累积类型，集合的类型，中间的累积类型）
 *      R（result类型）
 *
 */
public class Streamtest1 {


    @Test
    public void demo1(){

        com.dh.java8.collector.Student s1=new com.dh.java8.collector.Student("zs",100);
        com.dh.java8.collector.Student s2=new com.dh.java8.collector.Student("zs",90);
        com.dh.java8.collector.Student s3=new com.dh.java8.collector.Student("ww",90);
        com.dh.java8.collector.Student s4=new com.dh.java8.collector.Student("zl",80);

        List<Student> li= Arrays.asList(s1,s2,s3,s4);

        List studentds=li.stream().collect(Collectors.toList());
        li.forEach(System.out::println);
        //越具体的方式越好
        System.out.println("count:"+li.stream().collect(Collectors.counting()));

        System.out.println("count:"+li.stream().count());
    }
}
