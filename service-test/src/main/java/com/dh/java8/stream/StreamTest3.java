package com.dh.java8.stream;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 集合关注的是数据与数据存储本身
 * 流关注的是对数据所执行的计算。
 * stream:内部迭代，并行化（fork-join,将大任务分解成小任务）。
 *          中间操作都会返回一个stream对象。
 */
public class StreamTest3 {

    /**
     * stream 转list
     */
    @Test
    public void test1(){
        Stream<String> stream=Stream.of("hello","world","helloWorld");
        List<String> list=stream.collect(Collectors.toCollection(ArrayList::new));
        list.forEach(System.out::println);
    }

    /**
     * stream转set
     */
    @Test
    public void test2(){
        Stream<String> stream=Stream.of("hello","world","helloWorld");
        Set<String> set=stream.collect(Collectors.toCollection(HashSet::new));
        set.forEach(System.out::println);
    }

    /**
     * 把stream拼接成字符串
     */
    @Test
    public void test3(){
        Stream<String> stream=Stream.of("hello","world","helloWorld");
        System.out.println(stream.collect(Collectors.joining()).toString());
    }

    /**
     * 将集合元素转换成大写
     */
    @Test
    public void test4(){
        List<String> list= Arrays.asList("hello","world","helloWorld");
        //map给定一个值返回一个值
        List<String> list1=list.stream().map(String::toUpperCase).collect(Collectors.toList());

        list1.forEach(System.out::println);
    }

    @Test
    public void test5(){
        List<Integer> list= Arrays.asList(1,2,3,4);
        //map给定一个值返回一个值
        List<Integer> list1=list.stream().map(item->item*2).collect(Collectors.toList());

        list1.forEach(System.out::println);
    }

    /**
     *generate:生成stream对象
     */
    @Test
    public void test6(){
        Stream<String> str=Stream.generate(UUID.randomUUID()::toString);
        str.findFirst().ifPresent(System.out::println);
    }

    /**
     * iterate 返回一个无限 串行 有序的 stream
     */
    @Test
    public void iterateTest(){

        Stream.iterate(1,item->item+2).limit(10).forEach(System.out::println);

    }

    /**
     * 取出大于2 的，然后将每个元素乘以2，忽略掉前俩个值，剩下的元素取前俩个相加
     */
    @Test
    public void test7(){
        Stream<Integer> stream=Stream.iterate(1,item->item+2).limit(10);

        //filter：过滤     map：映射
        //System.out.println(stream.filter(item->item >2).mapToInt(item -> item*2).skip(2).limit(2).sum());

        //stream.filter(item->item >200).mapToInt(item -> item*2).skip(2).limit(2).min().ifPresent(System.out::println);
        IntSummaryStatistics intSummaryStatistics= stream.filter(item->item >2).mapToInt(item -> item*2).skip(2).limit(2).summaryStatistics();
        System.out.println(intSummaryStatistics.getMax());
        System.out.println(intSummaryStatistics.getMin());
    }

    @Test
    public void test8(){
        Stream<String> stream=Stream.of("hello","world","helloWorld");

        //map为中间操作，没有终止操作这个代码不会执行
        stream.map(item ->{
            System.out.println("test");
           return item;
        });
    }

    @Test
    public void test9(){
        List<AA>  list=new ArrayList<AA>(){{
            add(new AA(1));
            add(new AA(1));
            add(new AA(2));
            add(new AA(2));
            add(new AA(2));
            add(new AA(3));
        }};
        Long a=list.stream().filter(model->model.getStatus()==1).count();
        System.out.println(a);

    }

    public static class AA {

        private Long mapPublicTime;

        private Double rankingListValue;

        private Integer status;

        public AA(Integer status) {
            this.status = status;
        }

        public Long getMapPublicTime() {
            return mapPublicTime;
        }

        public void setMapPublicTime(Long mapPublicTime) {
            this.mapPublicTime = mapPublicTime;
        }

        public Double getRankingListValue() {
            return rankingListValue;
        }

        public void setRankingListValue(Double rankingListValue) {
            this.rankingListValue = rankingListValue;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }
    }
}
