package com.dh.java8.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 实现单词去重
 */
public class FlatMap {
    public static void main(String[] args) {
        List<String> list= Arrays.asList("hello welcome","world hello","hello world hello","hello welcome");

        list.stream().map(item->item.split(" ")).distinct().collect(Collectors.toList());
        //Stream<String[]> Stream<String>
        List<String> list1= list.stream().map(item->item.split(" ")).flatMap(Arrays::stream).distinct().collect(Collectors.toList());
        list1.forEach(System.out::println);

        //List<String[]> list2= list.stream().map(item->item.split(" ")).distinct().collect(Collectors.toList());
        //list2.forEach(item->Arrays.asList(item).forEach(System.out::println));

    }
}
