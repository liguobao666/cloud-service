package com.dh.java8.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamTest7 {
    /**
     * 单词去重
     * @param args
     */
    public static void main(String[] args) {
        List<String> list=Arrays.asList("hello welcome","world hello","hello world hello");

        /*list.stream().map(item->item.split(" ")).
                distinct().collect(Collectors.toList()).
                forEach(System.out::println);*/
        //flatmap 将结果打平，
        /*list.stream().map(item->item.split(" ")).flatMap(str->Arrays.stream(str))
                .distinct().collect(Collectors.toList())
                .forEach(System.out::println);*/

        list.stream().reduce((a,b)->{
            return a+"sss"+b;
        }).ifPresent(System.out::println)
        ;
    }

}
