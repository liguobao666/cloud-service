package com.dh.java8.collector;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

public class CollectorTest {

    /**
     * 分数最低的
     */
    @Test
    public void test1(){
        Student s1=new Student("zs",100);
        Student s2=new Student("zs",90);
        Student s3=new Student("ww",90);
        Student s4=new Student("zl",80);

        List<Student> li= Arrays.asList(s1,s2,s3,s4);
        li.stream().min(Comparator.comparingInt(Student::getCore)).ifPresent(System.out::print);

        int i=li.stream().collect(Collectors.summingInt(Student::getCore));

        //拼接名字
        System.out.println(li.stream().map(Student::getName).collect(Collectors.joining(",")));
    }


    /**
     * 多级分组先根据分数，然后根据名字分组
     */
    @Test
    public void test2(){
        Student s1=new Student("zs",100);
        Student s2=new Student("zs",90);
        Student s3=new Student("ww",90);
        Student s4=new Student("zl",80);
        Student s5=new Student("zl",80);
        List<Student> li= Arrays.asList(s1,s2,s3,s4,s5);

        Map<Integer,Map<String,List<Student>>> maps =li.stream().collect(Collectors.groupingBy(Student::getCore,Collectors.groupingBy(Student::getName)));

        System.out.println("==========");
        //分数大于80，然后求每个分区学生数
        Map<Boolean,Long> map=li.stream().collect(Collectors.partitioningBy(student->student.getCore()>80,Collectors.counting()));

        //按照名字分组，然后求出每组分组中分数最低的学员。
        Map<String,Student> ma=li.stream().collect(Collectors.groupingBy(Student::getName,Collectors.collectingAndThen(Collectors.minBy(Comparator.comparingInt(Student::getCore)), Optional::get)));
    }
}
