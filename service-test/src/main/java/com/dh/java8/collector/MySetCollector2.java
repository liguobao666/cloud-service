package com.dh.java8.collector;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 *
 * 收集器
 * 1输入set<String></>
 * 输出Map<String,String></>
 *
 * 示例["hello","word"]
 * {"hello","hello"}
 */
public class MySetCollector2<T> implements Collector<T,Set<T>,Map<T,T>> {
    @Override
    public Supplier<Set<T>> supplier() {
        return HashSet::new;
    }

    /**
     * 将下一个元素累计到结果容器
     * @return
     */
    @Override
    public BiConsumer<Set<T>, T> accumulator() {
        return (set,item)->{
            //并发流 配置CONCURRENT选项下 遍历集合时可能会报错。通常累accumulator中建议不要执行多余的操作。
            /**
             * if (modCount != expectedModCount)
                    throw new ConcurrentModificationException();
             */
            System.out.println("accumulator:"+set+"....."+Thread.currentThread().getName());
            set.add(item);
        };
    }

    /**
     * 并发流合并
     * @return
     */
    @Override
    public BinaryOperator<Set<T>> combiner() {

        System.out.println("123123");
        return (set1,set2)->{
           set1.addAll(set2);
           return set1;
        };
    }

    /**
     * 中间结果类型和最终结果一样的话这个方法不会被调用
     * 但是这个类的最终结果和中间类型不一样
     * @return
     */
    @Override
    public Function<Set<T>, Map<T, T>> finisher() {
        return t->{
            Map<T,T> map=new HashMap<>();
            t.stream().forEach(item->map.put(item,item));
            return map;
        };
    }

    /**
     * 定义收集器的特性。
     * IDENTITY_FINISH:中间结果类型和最后结果类型是一样的，finisher函数是最终结果，这里A中间类型会强制转换为最后结果R
     *                  定义了这个选项，finisher不会被调用，直接强制类型转换。
     * UNORDERED:
     * CONCURRENT:并发流多个线程同时操作一个结果容器，那么combiner就不会被调用
     *             如果没有这个选项，使用并发流则会有多个线程操作多个结果容器，combiner会调用，合并多个结果容器
     * @return
     */
    @Override
    public Set<Characteristics> characteristics() {
        return Collections.unmodifiableSet(EnumSet.of(
                Characteristics.UNORDERED,Characteristics.CONCURRENT));
    }

    /**
     * 超线程技术
     accumulator:[].....main
     accumulator:[].....ForkJoinPool.commonPool-worker-4
     accumulator:[].....ForkJoinPool.commonPool-worker-1
     accumulator:[].....ForkJoinPool.commonPool-worker-2
     accumulator:[].....ForkJoinPool.commonPool-worker-3
     accumulator:[a, b].....main
     * @param args
     */
    public static void main(String[] args) {
        List<String> list= Arrays.asList("123","hello","world","a","b","d");

       /* Map<String,String> set=list.stream().collect(new MySetCollector2<>());
        System.out.println(set);*/

        Map<String,String> set=list.parallelStream().collect(new MySetCollector2<>());
        System.out.println(set);
    }
}
