package com.dh.java8.collector;

public class AutoCloseableStudy {
    public static void main(String[] args) {
        try(MyResource resource = new MyResource()){
            resource.test();
        }
    }
}

class MyResource implements AutoCloseable{

    public void test(){
        System.out.println("test");
    }

    @Override
    public void close() {
        System.out.println("closed");
    }
}