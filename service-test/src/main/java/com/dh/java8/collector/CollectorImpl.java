package com.dh.java8.collector;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * 自定义收集器
 */
public class CollectorImpl<T> implements Collector<T,Set<T>,Set<T>> {
    /**
     * 用于中间收集的结果容器A
     * @return
     */
    @Override
    public Supplier<Set<T>> supplier() {
        System.out.println("supplier");
        return  HashSet::new;
    }

    /**
     * 累加器
     * @return
     */
    @Override
    public BiConsumer<Set<T>,T> accumulator() {
        System.out.println("accumulator");
        return (set,item)->{
            System.out.println("accumulator:"+set+"....."+Thread.currentThread().getName());
            set.add(item);
        };
    }

    /**
     *将并行结果合并到一起
     * @return
     */
    @Override
    public BinaryOperator<Set<T>> combiner() {
        System.out.println("combiner");
        return (set1,set2)->{
            set1.addAll(set2);
            return set1;
        };
    }

    /**
     * 可选操作，完成器，将A转换为一个新的结果容器
     * @return
     */
    @Override
    public Function<Set<T>,Set<T>> finisher() {
        System.out.println("finisher");
        return Function.identity();
    }

    /**
     * 当前收集器的特性
     * @return
     */
    @Override
    public Set<Characteristics> characteristics() {
        System.out.println("characteristics");
        return Collections.unmodifiableSet(EnumSet.of(Characteristics.IDENTITY_FINISH));
    }

    public static void main(String[] args) {
        List<String> list= Arrays.asList("123","123","heoo","sfasf");

        Set<String> set=list.stream().collect(new CollectorImpl<>());
        System.out.println(set);
    }
}
