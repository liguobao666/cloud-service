package com.dh.java8.collector;

public class Student {

    private String name;
    private int core;

    public Student(String name, int core) {
        this.name = name;
        this.core = core;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCore() {
        return core;
    }

    public void setCore(int core) {
        this.core = core;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", core=" + core +
                '}';
    }
}
