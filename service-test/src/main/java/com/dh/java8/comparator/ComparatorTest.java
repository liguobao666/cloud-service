package com.dh.java8.comparator;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * reversed()，相反排序
 */
public class ComparatorTest {

    /**
     * 对list升序排序
     */
    @Test
    public void test1(){
       List<String> list=Arrays.asList("nihao","hello","world","welcome");
       //Collections.sort(list);
        Collections.sort(list,(item1,item2)->item1.length()-item2.length());

    }

    @Test
    public void test2(){
        List<String> list=Arrays.asList("nihao","hello","world","welcome");

        Collections.sort(list, Comparator.comparingInt(String::length).reversed());
        //lamada 类型推断,
        Collections.sort(list, Comparator.comparingInt((String item)->item.length()).reversed());
        list.sort(Comparator.comparingInt(String::length));
        System.out.println(list);

    }

    @Test
    public void test3(){
        List<String> list=Arrays.asList("nihao","hello","world","welcome");
        //list字符串长度排序，然后根据字符串Ascall排序
        //thenComparing:如果第一个比较器返回的结果为0（已经排好序），则运行第一个比较器，没有排好序的元素进入thenComparing
        Collections.sort(list,Comparator.comparingInt(String::length).thenComparing(String.CASE_INSENSITIVE_ORDER));

        Collections.sort(list,Comparator.comparingInt(String::length).thenComparing(
                    (item1,item2)->item1.compareTo(item2)
                )
        );

    }

    @Test
    public void test4(){
        List<String> list=Arrays.asList("nihao","hello","world","welcome");
        Collections.sort(list,Comparator.comparingInt(String::length).thenComparing(
                Comparator.comparing( String::toLowerCase,Comparator.reverseOrder() )
                )
        );

    }

    @Test
    public void test5(){
        //thenComparing:如果第一个比较器返回的结果为0（已经排好序），则运行第一个比较器，没有排好序的元素进入thenComparing
        List<String> list=Arrays.asList("nihao","hello","world","welcome");
        //第一个reversed  welcome world hello nihao    welcome  hello nihao  world
        Collections.sort(list,Comparator.comparingInt(String::length).reversed().
                thenComparing(Comparator.comparing(String::toLowerCase,Comparator.reverseOrder()))
        );
        System.out.println(list);

    }

}
