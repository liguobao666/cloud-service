package com.dh.singleton;
/**
 * 双重检索机制，线程安全单列模式
 * @author liguobao
 *
 */
public class SingletonDemo {
	private volatile static SingletonDemo singletonDemo;

	private SingletonDemo() {
		
	}

	public static SingletonDemo getSingletonDemo() {
		if (singletonDemo == null) {
			synchronized (SingletonDemo.class) {
				if (singletonDemo == null) {
					singletonDemo = new SingletonDemo();
				}
			}
		}
		return singletonDemo;
	}

}
