package com.dh.executor;

/**
 * Java通过Executors提供四种线程池，分别为：
		newCachedThreadPool创建一个可缓存线程池，如果线程池长度超过处理需要，可灵活回收空闲线程，若无可回收，则新建线程。
		newFixedThreadPool 创建一个定长线程池，可控制线程最大并发数，超出的线程会在队列中等待。
		newScheduledThreadPool 创建一个定长线程池，支持定时及周期性任务执行。
		newSingleThreadExecutor 创建一个单线程化的线程池，它只会用唯一的工作线程来执行任务，保证所有任务按照指定顺序(FIFO, LIFO, 优先级)执行。
 * @author liguobao
 * JDK7提供了7个阻塞队列。分别是:
		ArrayBlockingQueue ：一个由数组结构组成的有界阻塞队列。
		LinkedBlockingQueue ：一个由链表结构组成的有界阻塞队列。
		PriorityBlockingQueue ：一个支持优先级排序的无界阻塞队列。
		DelayQueue：一个使用优先级队列实现的无界阻塞队列。
		SynchronousQueue：一个不存储元素的阻塞队列。
		LinkedTransferQueue：一个由链表结构组成的无界阻塞队列。
		LinkedBlockingDeque：一个由链表结构组成的双向阻塞队列。
 *
 */
public class ThreadPoolExecutorTest {
	//ThreadPoolExecutor
	//Executors
}
