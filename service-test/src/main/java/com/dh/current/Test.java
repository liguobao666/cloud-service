package com.dh.current;

public class Test {
	public static void main(String args[]){
		A a=new B();
		test(a);
	}
	public static void test(A a){
		System.out.println("test A");
		a.whoAmi();
	}
	public static void test(B b){
		System.out.println("test B");
		b.whoAmi();
	}
	

}
class A{
	public void whoAmi(){
		System.out.println("I am A");
	}
	
}
class B extends A{
	public void whoAmi(){
		System.out.println("I am B");
	}
	
}
