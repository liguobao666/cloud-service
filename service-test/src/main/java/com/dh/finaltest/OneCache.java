package com.dh.finaltest;

import java.math.BigInteger;
import java.util.Arrays;

/**
 * 对于final域，编译器和处理器要遵守两个重排序规则：
	在构造函数内对一个final域的写入，与随后把这个被构造对象的引用赋值给一个引用变量，这两个操作之间不能重排序。
	初次读一个包含final域的对象的引用，与随后初次读这个final域，这两个操作之间不能重排序。
	与Volatile 有相似作用，不过Final主要用于不可变变量（基本数据类型和非基本数据类型），进行安全的发布（初始化）。
	而Volatile可以用于安全的发布不可变变量，也可以提供可变变量的可见性。
 * @author liguobao
 *
 */
public class OneCache {
	
	private final int lastNumber;
	private final BigInteger[] lastFactors;
	
	public OneCache(int i, BigInteger[] fac) {
		lastNumber=i;
		lastFactors=Arrays.copyOf(fac, fac.length);
	}
	
	public static void main(String args[]) {
		OneCache one=new OneCache(1, null);
	}
    
}
