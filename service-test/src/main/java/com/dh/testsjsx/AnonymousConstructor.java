package com.dh.testsjsx;

public class AnonymousConstructor {
   public Base getBase(int i){
       return new Base(i) {
           @Override
           public void f() {


           }
       };
   }
}

abstract  class Base{
    public Base(int i){

    }
    public static void  a(){}
    public abstract void f();
}
interface a{
    public static final int a=0;
    public default void a(){

    }
}
