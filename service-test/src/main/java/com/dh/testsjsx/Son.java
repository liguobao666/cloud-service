package com.dh.testsjsx;

public class Son extends Father{
    public void play(){
        System.out.println("Son");
    }
}
class Son1 extends Father{
    public void play(){
        System.out.println("Son1");
    }
}
class Father{


    public void play(){
        System.out.println("Father");
    }
}
class T{
    public void fn(Father f){
        if(f.getClass().equals(Son1.class)){
            f.play();
        }
    }
    public  static  void  main(String args[]){
        T t=new T();
        Father f=new Son1();
        t.fn(f);

    }
}

interface I1{
    void f();
}
interface I2{
    int f(int i);
}
interface  I3{
    int f();
}
class C1{
    public int f(){
        return 12;
    }
}
/*
class  C3 extends C1 implements I1{
    public int f(int i) {
        return 0;
    }
}*/
