package com.dh;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CountDownLaunch {

    //创建一个CountDownLatch
    private static CountDownLatch threadsSignal=new CountDownLatch(1000000);

    private static ExecutorService execPool = Executors.newFixedThreadPool(8);

    public static void main(String[] args) throws Exception{
        for (int i=0;i<100000;i++){
            execPool.submit( new Thread(()->{
                System.out.println("线程开启"+threadsSignal.getCount());
                threadsSignal.countDown();
            }));
        }
        threadsSignal.await();
    }

}


