package com.dh.chain;

public class User {
	private String username;
    private String password;
    private String email;
    
    public String getUsername() {
		return username;
	}

	public User setUsername(String username) {
		this.username = username;
		 return this;
	}

	public String getPassword() {
		return password;
	}

	public User setPassword(String password) {
		this.password = password;
		return this;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public static void main(String[] args) {
        User user = new User();
        user.setUsername("lss0555")
        	.setPassword("123456")
        	.setEmail("123");
        System.out.println("user:"+user.toString());
    }

}
