package com.dh.util;

import java.util.UUID;

/**
 * UUID工具类
 * 
 * @author lixiang
 *
 */
public class UuidTool {
	/**
	 * 获取32位UUID
	 * 
	 * @return
	 */
	public static synchronized String getUUID32() {
		UUID uuid = UUID.randomUUID();
		String str = uuid.toString();
		// 去掉"-"符号
		String temp = str.substring(0, 8) + str.substring(9, 13) + str.substring(14, 18) + str.substring(19, 23)
				+ str.substring(24);
		return temp;
	}
}
