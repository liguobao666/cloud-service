package com.dh.jvm;

import java.sql.Driver;
import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * 线程上下文类加载器的使用模式：（获取-使用-还原）
 *      //获取
 *      ClassLoader classLoader=Thread.currentThread().getContextClassLoader()
 *      try{
 *          //使用
 *          Thread.currentThread().setContextClassLoader(targetClassLoader);
 *          method();
 *      }finally{
 *          //还原
 *          Thread.currentThread().setContextClassLoader(classLoader);
 *      }
 *      当高层提供了统一的接口让低层去实现，同时又要高层加载（或者实例化）低层的类时，就必须要通过上下文类
 *      加载器帮助高层的CLassLoader 加载该实现类
 *
 *
 */
public class MyTest26 {
    public static void main(String[] args) {

        /**
         *   ServiceLoader 约定 META-INF/services
         *  放置需要加载的第三方实现，serviceLoader会根据上下文类加载器去加载实现，
         *  根据双亲委托机器（serviecLoader 由根类加载器加载，相关联的类也需要根类加载加载）
         *  所以这里想要加载第三方实现是不可能的。所以使用线程上下文加载（这里是系统类加载器）来加载第三方实现。
         *
         *  public static <S> ServiceLoader<S> load(Class<S> service) {
         *         ClassLoader cl = Thread.currentThread().getContextClassLoader();//系统类加载器
         *         return ServiceLoader.load(service, cl);
         * }
         */

        //Thread.currentThread().setContextClassLoader(MyTest26.class.getClassLoader().getParent());

        ServiceLoader<Driver> loader=ServiceLoader.load(Driver.class);

        Iterator<Driver> iterator=loader.iterator();

        while (iterator.hasNext()){
            Driver driver=iterator.next();
            System.out.println("driver:"+driver.getMajorVersion()+",Loader:"+driver.getClass().getClassLoader());
        }
        System.out.println("当前线程上下文类加载器："+Thread.currentThread().getContextClassLoader());
        System.out.println("ServiceLoader的类加载器"+ServiceLoader.class.getClassLoader());//启动类加载加载

    }
}























