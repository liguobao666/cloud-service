package com.dh.jvm.jvmparameter;

/**
 * 大对象直接进入老年代
 */
public class BigObjectToOld {
    private static final int _1MB=1024*1024;

    /**
     * -Xms20M
     * -Xmx20M
     * -Xmn10M
     * -XX:SurvivorRatio=8
     * -XX:+PrintGCDetails
     * -XX:PretenureSizeThreshold=3145728 我们设置3M,但是程序分配了4M，所以直接进入老年代
     * @param args
     */
    public static void main(String[] args) {
        byte[] bytes=new byte[4*_1MB];//大对象直接进入老年代
    }
}
