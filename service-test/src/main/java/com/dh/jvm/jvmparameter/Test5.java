package com.dh.jvm.jvmparameter;


/**
 * -Xms20M
 * -Xmx20M
 * -Xmn10M 新生代大小
 * -XX:SurvivorRatio=8   endn:survior=8
 * -XX:+PrintGCDetails
 *
 * 对象优先在eden去分配，无法分配时，触发一次minor gc
 */
public class Test5 {

    private static final int _1MB=1024*1024;

    public static void main(String[] args) {
        byte[] a1,a2,a3,a4;
        a1=new byte[2*_1MB];
        a2=new byte[2*_1MB];
        a3=new byte[2*_1MB];
        a4=new byte[4*_1MB];//出现一次minor gc

    }
}
