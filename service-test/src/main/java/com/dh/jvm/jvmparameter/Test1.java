package com.dh.jvm.jvmparameter;

/**
 * -Xms5m -Xmx20m
 * -XX:+PrintGCDetails
 * -XX:+UseSerialGC
 * -XX:+PrintCommandLineFlags 打印虚拟机参数
 * -XX+HeapDumpOnOutOfMemoryError
 * -XX:HeapDumpPath=d:/Test03.dump
 */
public class Test1 {

    /**
     * [GC (Allocation Failure) [DefNew: 1791K->138K(1856K), 0.0021239 secs]
     * 垃圾回收信息 DefNew新生代 1791k回收后138k  0.0021239 secs使用这么多毫秒
     * @param args
     */
    public static void main(String[] args) {

        //查看堆信息
        System.out.println("max memory"+Runtime.getRuntime().maxMemory());
        System.out.println("free memory"+Runtime.getRuntime().freeMemory());
        System.out.println("total memory"+Runtime.getRuntime().totalMemory());
        System.out.println("=================");

        byte[] b1=new byte[1*1024*1042];
        System.out.println("分配了1M");
        System.out.println("max memory"+Runtime.getRuntime().maxMemory());
        System.out.println("free memory"+Runtime.getRuntime().freeMemory());
        System.out.println("total memory"+Runtime.getRuntime().totalMemory());
        System.out.println("=================");

        byte[] b2=new byte[4*1024*1042];
        System.out.println("分配了4M");
        System.out.println("max memory"+Runtime.getRuntime().maxMemory());
        System.out.println("free memory"+Runtime.getRuntime().freeMemory());
        System.out.println("total memory"+Runtime.getRuntime().totalMemory());
        System.out.println("=================");

        int a=0x00000000fa0a0000;
        int b=0x00000000fa801000;
        System.out.println("结果为："+(b-a)/1024);
    }
}
