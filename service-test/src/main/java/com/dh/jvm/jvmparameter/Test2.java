package com.dh.jvm.jvmparameter;

/**
 * 在实际工作中，我们可以直接将出事的堆大小与最大堆大小设置相等，这样的好处是可以减少程序运行的垃圾回收次数，
 * 从而提高性能
 */
public class Test2 {
    public static void main(String[] args) {
        //Serial 单线程并行收集器  -XX:SurvivorRatio eden去和s区的比值
        // Xmn新生代的大小
        // -Xms20m -Xmx20m -Xmn1m -XX:SurvivorRatio=2 -XX:+PrintGCDetails -XX:+UseSerialGC

        // -Xms20m -Xmx20m -Xmn7m -XX:SurvivorRatio=2 -XX:+PrintGCDetails -XX:+UseSerialGC

        //-XX:NewRation=老年代/新生代
        // -Xms20m -Xmx20m -Xmn7m -XX:NewRation=2 -XX:+PrintGCDetails -XX:+UseSerialGC

        byte [] b=null;
        for(int i=0;i<10;i++){
            b=new byte[1*1024*1024];
        }
    }
}
