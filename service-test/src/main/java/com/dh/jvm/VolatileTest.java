package com.dh.jvm;

/**
 * volatil能保证线程可见，但是对于非原子操作并不一定能保证运行结果正确。
 * 如下代码所示，对于 race++操作，对应的字节码如下所示：
 *  0:getstatic
 *  3:iconst_1
 *  4:iadd
 *  5:putstatic
 *
 *  getstatic把指令推到站顶，volatile保证了race的值是正确的，但是执行iconst_1，iadd 指令时其他线程可能已经
 *  将race值修改
 *
 *
 *  java内存模型：
 *
 *   -----------       ---------------   -------
 *   - java线程 - ----->=  工作内存    = -> =save=
 *   -----------       ---------------    =    =
 *                                        =    =
 *   -----------       ---------------    =    =
 *   - java线程 - ----->|工作内存|变量副本|   =    = -------> 主内存
 *   -----------       ---------------    =    =
 *
 *   -----------       ---------------    =load=
 *   - java线程 - ----->=    工作内存  = -> =    =
 *   -----------       ---------------    ------

 *  内存间的交互操作
 *    lock(锁定)：作用于主内存的变量，把变量标识为一条线程独占的状态
 *    unlock（解锁）：作用于主内存变量，将锁定的变量释放。
 *    read（读取）：作用于主内存的变量，把一个变量的值从主内存传输到线程的工作内存，以便随后的load使用
 *    load（载入）：作用于工作内存的变量，把read读到的变量放入工作内存的变量副本中
 *    use（使用）：作用于工作内存的变量。把变量传递给执行引擎，每当虚拟机字节码指令需要这个值的时候，会执行这个操作。
 *    assgin(赋值)：作用于工作内存的变量，将执行引擎接收到的值赋值给工作内存中的变量。
 *    store（存储）：作用于工作内存的变量，把工作内存中的变量传递到住内存中。（传递）
 *    write（写入）：作用于主内存的变量，把store操作传递的值写入主内存变量中。
 *
 *    如果要把一个变量从主内存复制到工作内存，就要顺序执行read 和 load操作，同样的要把
 *    变量从工作内存同步回主内存，就要顺序执行store和write操作。
 *    java内存模型只要求上述操作顺序执行，而没有保证必须连续执行，也就是read和load操作
 *    之间是可以操作其他指令的。
 *    如对于主内存中的a,b的访问：read a ,read b,load b,load a。
 *
 *    java内存模型的特征
 *       -原子性
 *       -有序性
 *       -可见性
 *
 *    先行发生原则（happens-before）：
 *       是否发生竞争，线程是否安全的重要依据。先行发生是java内存模型中定义的俩相操作之间的偏序关系，
 *     java 内存模型下一些天然的先行发生关系：
 *       1：程序次序规则：在一个线程内，按照程序代码顺序，前面的操作优先发生于后面的操作
 *       2：管程锁定规则：一个unlock操作先行发生于后面对同一个锁的lock操作，这里必须强调是同一个锁，而“后面”是指时间上的先后顺序;
 *       3：volatile变量规则:对一个volatile变量的写操作先行发生于后面对这个变量的读操作，这里的“后面”同样是指时间上的先后顺序;
 *       4：线程启动规则: Thread对象的start()方法先行发生于此线程的每一个动作;
 *       5：线程终止规则: 线程中的所有操作都先行发生于对此线程的终止检测，我们可以通过Thread.join()方法结束、Thread.isAlive()的返回值等手段检测到线程已经终止执行;
 *       6：线程中断规则: 对线程interrupt()方法的调用先行发生于被中断线程的代码检测到中断事件的发生，可以通过Thread.interrupted()方法检测到是否有中断发生;
 *       7：对象终结规则:一个对象的初始化完成（构造函数执行结束）先行发生于它的finalize()方法的开始;
 *       8:传递性（Transitivity）： 如果操作A先行发生于操作B，操作B先行发生于操作C，那就可以得出操作A先行发生于操作C的结论。
 *
 *
 */
public class VolatileTest {

    public static volatile int race=0;

    public static void increase(){
        race++;
    }

    private static final int COUNT=20;

    public static void main(String[] args) {
        Thread[] threads=new Thread[COUNT];

        for(int i=0;i<COUNT;i++){
            threads[i]=new Thread(()->{
                for (int j=0;j<10000;j++){
                    increase();
                }
            });
            threads[i].start();
        }
        while (Thread.activeCount()>1)
            Thread.yield();

        System.out.println(race);

    }
}
