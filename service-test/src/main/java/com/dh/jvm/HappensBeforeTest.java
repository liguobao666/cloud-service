package com.dh.jvm;

public class HappensBeforeTest {

    private int value=10;

    public synchronized int getValue() {

        System.out.println("sdsdd");
        return value;
    }

    public synchronized void setValue(int value) {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("aaa");
        this.value = value;
    }

    public static void main(String[] args) throws Exception{
        HappensBeforeTest test=new HappensBeforeTest();
        Thread thread=new Thread(()->{
            test.setValue(1000);
        });
        Thread thread1=new Thread(()->{
            System.out.println(test.getValue());
        });

        thread.start();
        Thread.sleep(100);
        thread1.start();
    }
}
