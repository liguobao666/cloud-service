package com.dh.jvm.memory;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

/**
 * 元空间：本地的内存空间，和堆是不连续的
 * 方法区溢出的错误
 * 1：显示的设定元空间的大小  -XX:MaxMetaspaceSize=10m
 * 2：运行时动态生成类
 *
 *
 *
 *
 */
public class MyTest4 {
    public static void main(String[] args) {
        for(;;){
            Enhancer enhancer=new Enhancer();
            enhancer.setSuperclass(MyTest4.class);
            enhancer.setUseCache(false);
            enhancer.setCallback((MethodInterceptor)(obj, method, args1, proxy)->proxy.invokeSuper(obj,args1));
            enhancer.create();
            System.out.println("aaaaaaaaaa");
        }
    }
}
