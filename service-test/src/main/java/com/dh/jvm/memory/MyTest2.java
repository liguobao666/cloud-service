package com.dh.jvm.memory;

/**
 * 虚拟机栈溢出
 * -Xss100k
 */
public class MyTest2 {
    private int length;

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void test(){
        this.length++;
        test();
    }

    public static void main(String[] args) {
        MyTest2 test=new MyTest2();
        try {
            test.test();
        }catch (Throwable ex){
            System.out.println(test.length);
            ex.printStackTrace();
        }

    }
}
