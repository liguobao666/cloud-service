package com.dh.classtest;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

class Book{
    private Integer price;
    private String name;

    public Book (Integer price, String name) throws  Exception,IOException {
        this.price = price;
        this.name = name;
    }
}

/**
 * 这个代码执行速率很慢
 */
public class TestDemo {
    public static void  main(String args[])throws Exception{
        Class<?> cls=Class.forName("com.dh.test.Book");
        Constructor<?> cons[]=cls.getConstructors();//取得所有的构造方法名称
        for(int i=0;i<cons.length;i++){
            //Modifier 将构造方法的修饰符转换数字成具体的修饰符
            System.out.print(Modifier.toString(cons[i].getModifiers())+" " );
            //取得方法名称
            System.out.print(cons[i].getName()+"(");
            //取得方法参数
            Class<?> parms[]= cons[i].getParameterTypes();
            for(int k=0;k<parms.length;k++){
                System.out.print(parms[k].getSimpleName()+" arg"+k);
                if(k<parms.length-1){
                    System.out.print(",");
                }
            }
            System.out.print(") throws ");
            //取得异常
            Class<?> exce[]=  cons[i].getExceptionTypes();
            for(int y=0;y<exce.length;y++){
                System.out.print(exce[y].getSimpleName());
            }
        }
    }
}
