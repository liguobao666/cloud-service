package com.dh.classtest;

import java.io.IOException;
import java.lang.reflect.Constructor;

class Book1{
    private Integer price;
    private String name;

    public Book1 (Integer price, String name) throws  Exception,IOException {
        this.price = price;
        this.name = name;
    }
}

/**
 * 这个代码执行速率很慢
 */
public class TestDemo1 {
    public static void  main(String args[])throws Exception{
        Class<?> cls=Class.forName("com.dh.test.Book1");
        Constructor<?> con=cls.getConstructor(Integer.class,String.class);
        Book1 b=(Book1) con.newInstance(78,"java");
        System.out.println(b);
    }
 }

