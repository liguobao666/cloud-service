package com.dh.server.controller;

import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    /**
     * 请求头加入下信息可以访问
     * Authorization Bearer d8fb2972-8214-46e3-bfda-561d43db5972
     * @return
     */
    @RequestMapping("/users/me")
    public String user() {
        //System.out.println(principal);
        return "123";
    }
    public void  add() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }
}
