package com.dh.server.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {
	
 	@RequestMapping("/login.do")
    public Object login() {
        Map<String,String> map=new HashMap<>();
        map.put("code", "201");
        map.put("message", "请登录");
		return map;
    }
 	@RequestMapping("/oauth2Client")
    public Object oauth2Client(HttpServletRequest request) {
        
        Map result = new HashMap();
        result.put("code", 200);
        result.put("data", "");
        return result;
    }

}
