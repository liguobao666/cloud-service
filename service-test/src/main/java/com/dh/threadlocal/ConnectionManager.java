package com.dh.threadlocal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
	
	private static Connection connect = null;
	
	public static Connection openConnection() throws SQLException {
		if(connect == null){
            connect = DriverManager.getConnection(null);
        }
        return connect;
		
	}
	
	public static void closeConnection() throws SQLException {
	        if(connect!=null)
	            connect.close();
	}
	
	private static ThreadLocal<Connection> connectionHolder= new ThreadLocal<Connection>() {
		public Connection initialValue() {
		    try {
				return DriverManager.getConnection("");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
	};
	 
	public static Connection getConnection() {
		return connectionHolder.get();
	}
	public static void main(String args[]){
		getConnection();
		
	}
	
	
	/*private static final ThreadLocal threadSession = new ThreadLocal();
	 
	public static Session getSession() throws InfrastructureException {
	    Session s = (Session) threadSession.get();
	    try {
	        if (s == null) {
	            s = getSessionFactory().openSession();
	            threadSession.set(s);
	        }
	    } catch (HibernateException ex) {
	        throw new InfrastructureException(ex);
	    }
	    return s;
	}*/

}
