package com.dh.nio;

import org.junit.Test;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;

/**
 * 非阻塞Io,客户端接收服务端返回的消息
 * 一：使用NIO完成网络通信的三个核心：
 * 1:通道Channel负责连接
 *   java.nio.channels.Channel接口
 *        |--SelectableChannel
 *           |--SocketChannel
 *           |--ServerSocketChannel
 *           |--DatagramChannel
 *
 *           |--Pipe.SinkChannel
 *           |--Pipe.SourceChannel、
 *
 *           FileChannel 不能切换为非阻塞模式，不能被选择器监控。
 * 2：缓冲区Buffer负责数据的存取
 * 3：选择器selector：是SelectableChannel的多路复用器，用于监控SelectableChannel的IO状况。
 */
public class TestBlockingNBlokingNio {

    //客户端
    @Test
    public void client() throws  Exception{
        //1获取客户端通道
        SocketChannel sChannel = SocketChannel.open(new InetSocketAddress("127.0.0.1",9898));
        //2切换为非阻塞模式
        sChannel.configureBlocking(false);
        //3分配指定大小的缓冲区
        ByteBuffer buf=ByteBuffer.allocate(1024);
        //4发送数据到服务器
        Scanner scanner=new Scanner(System.in);
        while(scanner.hasNext()){
            String str=scanner.next();
            buf.put(str.getBytes());
            buf.flip();
            sChannel.write(buf);
            buf.clear();
        }
        //5关闭通道
        sChannel.close();
    }

    //服务端
    @Test
    public void server()throws  Exception{
        //1获取通道
        ServerSocketChannel ssChannel=ServerSocketChannel.open();

        //2切换为非阻塞模式
        ssChannel.configureBlocking(false);

        //3:绑定端口
        ssChannel.bind(new InetSocketAddress(9898));

        //4:获取选择器
        Selector selector=Selector.open();

        //5:将通道注册到选择器,指定监听事件SelectionKey四个常量类型：
        // OP_READ(1),OP_WRITE(4),OP_CONNECT(8),OP_ACCEPT(16)
        ssChannel.register(selector, SelectionKey.OP_ACCEPT);//连接事件

        //6:轮询获取选择器已经准备就绪的时间,大于0就有准备就绪的
        while(selector.select()>0){
            //7获取选择器中注册的事件（已经就绪的）
            Iterator<SelectionKey> it=selector.selectedKeys().iterator();

            while(it.hasNext()){
                //8获取准备就绪的事件
                SelectionKey sk= it.next();
                //9判断具体是什么事件
                if(sk.isAcceptable()){
                    //获取连接
                    SocketChannel sChannel = ssChannel.accept();
                    sChannel.configureBlocking(false);
                    //将该通道注册到选择器上
                    sChannel.register(selector,SelectionKey.OP_READ);
                }else if(sk.isReadable()){
                    //获取选择器上读就绪状态的通道
                    SocketChannel sChannel=(SocketChannel)sk.channel();
                    //读取数据
                    ByteBuffer buf= ByteBuffer.allocate(1024);
                    int len=0;
                    while((len=sChannel.read(buf))>0){
                        buf.flip();
                        System.out.println(new String(buf.array(),0,len));
                        buf.clear();
                    }
                }
                //取消事件
                it.remove();
            }
        }

    }
}
/*package com.dh.nio;

import org.junit.Test;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Date;
import java.util.Iterator;

*//**
 * 非阻塞Io,客户端接收服务端返回的消息
 * 一：使用NIO完成网络通信的三个核心：
 * 1:通道Channel负责连接
 *   java.nio.channels.Channel接口
 *        |--SelectableChannel
 *           |--SocketChannel
 *           |--ServerSocketChannel
 *           |--DatagramChannel(UDP发送)
 *
 *           |--Pipe.SinkChannel
 *           |--Pipe.SourceChannel、
 *
 *           FileChannel 不能切换为非阻塞模式，不能被选择器监控。
 * 2：缓冲区Buffer负责数据的存取
 * 3：选择器selector：是SelectableChannel的多路复用器，用于监控SelectableChannel的IO状况。
 *//*
public class TestBlockingNBlokingNio {

    //客户端
    @Test
    public void client() throws  Exception{
        //1获取客户端通道
        SocketChannel sChannel = SocketChannel.open(new InetSocketAddress("127.0.0.1",9898));
        //2切换为非阻塞模式
        sChannel.configureBlocking(false);
        //3分配指定大小的缓冲区
        ByteBuffer buf=ByteBuffer.allocate(1024);
        //4发送数据到服务器
        buf.put(new Date().toString().getBytes());
        buf.flip();
        sChannel.write(buf);
        buf.clear();
        //5关闭通道
        sChannel.close();
    }

    //服务端
    @Test
    public void server()throws  Exception{
        //1获取通道
        ServerSocketChannel ssChannel=ServerSocketChannel.open();

        //2切换为非阻塞模式
        ssChannel.configureBlocking(false);

        //3:绑定端口
        ssChannel.bind(new InetSocketAddress(9898));

        //4:获取选择器
        Selector selector=Selector.open();

        //5:将通道注册到选择器,指定监听事件SelectionKey四个常量类型：
        // OP_READ(1),OP_WRITE(4),OP_CONNECT(8),OP_ACCEPT(16)
        ssChannel.register(selector, SelectionKey.OP_ACCEPT);//连接事件

        //6:轮询获取选择器已经准备就绪的时间,大于0就有准备就绪的
        while(selector.select()>0){
            //7获取选择器中注册的事件（已经就绪的）
            Iterator<SelectionKey> it=selector.selectedKeys().iterator();

            while(it.hasNext()){
                //8获取准备就绪的事件
                SelectionKey sk= it.next();
                //9判断具体是什么事件
                if(sk.isAcceptable()){
                    //获取连接
                    SocketChannel sChannel = ssChannel.accept();
                    sChannel.configureBlocking(false);
                    //将该通道注册到选择器上
                    sChannel.register(selector,SelectionKey.OP_READ);
                }else if(sk.isReadable()){
                    //获取选择器上读就绪状态的通道
                    SocketChannel sChannel=(SocketChannel)sk.channel();
                    //读取数据
                    ByteBuffer buf= ByteBuffer.allocate(1024);
                    int len=0;
                    while((len=sChannel.read(buf))>0){
                        buf.flip();
                        System.out.println(new String(buf.array(),0,len));
                        buf.clear();
                    }
                }
                //取消事件
                it.remove();
            }
        }

    }
}
*/