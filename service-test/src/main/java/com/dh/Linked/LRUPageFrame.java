package com.dh.Linked;

/**
 * LRU淘汰算法
 * @author liguobao
 *
 */
public class LRUPageFrame {
	/**
	 * 节点数据
	 */
	private static class Node{
		Node prev;
		Node next;
		int pageNum;
		Node(){
			
		}
	}
	
	private int capacity;//容量
	
	private int currentSize;
	private Node first;
	private Node last;
	
	public LRUPageFrame(int capacity) {
		this.currentSize=0;
		this.capacity=capacity;
	}
    
	/**
	 * 查找Node节点
	 * @param data
	 * @return
	 */
	private Node find(int data) {
		Node node=first;
		while(node!=null) {
			if(node.pageNum==data)
				return node;
			node =node.next;
		}
		return null;
	}
	

}
