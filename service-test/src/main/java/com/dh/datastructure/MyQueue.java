package com.dh.datastructure;

public class MyQueue {
	private Object[] queArray;
	//队列总大小
	private int maxSize;
	//前端指针
	private int front;
	//后端指针
	private int rear;
	//对列中的元素的实际数目
	private int nItems;
	
	/**
	 * 初始化队列
	 * @param s
	 */
	public  MyQueue(int s) {
		maxSize=s;
		queArray=new Object[maxSize];
		front=0;
		rear=-1;
		nItems=0;
	}
	//队列中新增元素
	public void insert(int value) {
		if(isFull()) {
			System.err.println("队列已满");
		}else {
			//如果对尾指针等于数组最后一个，放在数组第一个位置
			if(rear==maxSize-1)
				rear=-1;
			//对尾指针+1，然后在对尾指针处插入新的数据
			queArray[++rear]=value;
			//队列个数+1
			nItems++;
		}
	}
	
	//移除数据,只能移除对头的数据
	public Object remove() {
		Object removeValue = null ;
		if(!isEmpty()) {
			removeValue=queArray[front];
			queArray[front]=null;
			front++;
			//如果对头指针等于数组大小队列已经为空，将指针初始化
			if(front==maxSize) {
				front=0;
				rear=-1;
			}
			nItems--;
			return  removeValue;
		}
		return removeValue;
	}
	
	//查看对头数据
	public Object peekFront() {
		return queArray[front];
	}
	
	//判断队列是否满了
	public boolean isFull() {
		//队列中的实际个数等于队列大小时判断队列已满
		return (nItems==maxSize);
	}
	
	//判断队列是否为空
	public boolean isEmpty() {
		return (nItems==0);
	}
	
	//返回队列的大小
	public int getSize() {
		return nItems;
	}
	
  public static void main(String[] args) {
        MyQueue queue = new MyQueue(3);
        queue.insert(1);
        queue.insert(2);
        queue.insert(3);//queArray数组数据为[1,2,3]
         
        System.out.println(queue.peekFront()); //1
        queue.remove();//queArray数组数据为[null,2,3]
        System.out.println(queue.peekFront()); //2
         
        queue.insert(4);//queArray数组数据为[4,2,3]
        queue.insert(5);//队列已满,queArray数组数据为[4,2,3]
    }

}
