package com.designmodel.strategypattern.Duck;

import com.designmodel.strategypattern.serviceimpl.FlyNoWay;

public class ModelDuck extends Duck {
    public ModelDuck(){
        flyBehavior=new FlyNoWay();
    }
}
