package com.designmodel.strategypattern.Duck;

import com.designmodel.strategypattern.service.FlyBehavior;

public abstract class Duck {

    FlyBehavior flyBehavior;

    public Duck(){}

    //鸭子的其他行为

    public void performFly(){
        flyBehavior.fly();
    }

    public void setFlyBehavior(FlyBehavior fb) {
        this.flyBehavior = fb;
    }
}
