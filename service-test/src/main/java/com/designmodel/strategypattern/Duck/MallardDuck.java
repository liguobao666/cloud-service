package com.designmodel.strategypattern.Duck;

import com.designmodel.strategypattern.serviceimpl.FlyWithWings;

public class MallardDuck extends Duck {

    public  MallardDuck(){
        flyBehavior=new FlyWithWings();
    }
}
