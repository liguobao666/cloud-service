package com.designmodel.strategypattern.serviceimpl;

import com.designmodel.strategypattern.service.FlyBehavior;

public class FlyRocketPowered implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("火箭动力飞行");
    }
}
