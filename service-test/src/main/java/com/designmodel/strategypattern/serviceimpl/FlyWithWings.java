package com.designmodel.strategypattern.serviceimpl;

import com.designmodel.strategypattern.service.FlyBehavior;

public class FlyWithWings implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("Im flying");
    }
}
