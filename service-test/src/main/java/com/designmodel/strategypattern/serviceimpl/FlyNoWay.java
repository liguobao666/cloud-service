package com.designmodel.strategypattern.serviceimpl;

import com.designmodel.strategypattern.service.FlyBehavior;

public class FlyNoWay implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("我不会飞");
    }
}
