package com.designmodel.strategypattern.service;

public interface FlyBehavior {
    public void fly();
}
