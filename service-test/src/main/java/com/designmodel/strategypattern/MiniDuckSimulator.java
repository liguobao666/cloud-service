package com.designmodel.strategypattern;

import com.designmodel.strategypattern.Duck.Duck;
import com.designmodel.strategypattern.Duck.MallardDuck;
import com.designmodel.strategypattern.Duck.ModelDuck;
import com.designmodel.strategypattern.serviceimpl.FlyRocketPowered;

public class MiniDuckSimulator {
    public static void main(String[] args) {
        Duck mallard=new MallardDuck();
        mallard.performFly();

        Duck model=new ModelDuck();
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
    }
}

