设计原则：1：找出应用中可能需要变化之处，把它们独立出来
        2：针对接口编程，而不是针对实现
        3：多用组合少用继承（Duck类中的飞不是继承而来的，而是和适当的行为对象组合而来的）
策略模式：
   鸭子类继承Duck,
   飞行行为实现FlyBehavior接口。

策略模式定义了算法族，分别封装起来，让他们之间可以相互替换，此模式让算法的变化独立于使用算法的客户。