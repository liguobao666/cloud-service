package com.designmodel.factory;

import com.designmodel.factory.rawMaterial.NYPizzaIngredinetFactory;
import com.designmodel.factory.rawMaterial.PizzaIngredinetFactory;

public class NYPizzaStore extends PizzaStore{

    /**
     *pizza商店实现具体的创建pizza
     * @param type
     * @return
     */
    @Override
    Pizza createPizza(String type) {
        PizzaIngredinetFactory factory=new NYPizzaIngredinetFactory();

        if(type.equals("cheese")){
            return new NYStyleChessePizza(factory);
        }else {
            return new NYStyleChessePizza(factory);
        }
    }
}
