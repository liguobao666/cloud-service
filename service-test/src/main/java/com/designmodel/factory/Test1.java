package com.designmodel.factory;

/**
 * 工厂方法模式：定义了一个创建对象的接口，但由子类决定要实例化的类是哪一个。
 * 工厂方法让类把实例化推迟到子类
 *
 * 依赖导置原则：
 *      变量不可以使用持有具体类的引用（new）
 *      不要让类派生自具体类（派生自一个抽象）
 *      不要覆盖基类中已经实现的方法
 */
public class Test1 {
    public static void main(String[] args) {
        PizzaStore nypizzaStore=new NYPizzaStore();

        nypizzaStore.orderPizze("cheese");
    }
}
