package com.designmodel.factory.rawMaterial;

/**
 * 原材料类
 */
public class Dough {
    private String name;

    public Dough(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
