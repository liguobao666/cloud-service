package com.designmodel.factory.rawMaterial;

public class NYPizzaIngredinetFactory implements PizzaIngredinetFactory {

    @Override
    public Dough createDough() {
        return new Dough("生面团");
    }
}
