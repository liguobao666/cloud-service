package com.designmodel.factory;

import com.designmodel.factory.rawMaterial.PizzaIngredinetFactory;

public class NYStyleChessePizza extends Pizza {
    //原材料
    PizzaIngredinetFactory pizzaIngredinetFactory;

    public NYStyleChessePizza(PizzaIngredinetFactory factory){
       this.pizzaIngredinetFactory=factory;
    }

    /**
     * 准备原材料
     */
    @Override
    public void prepare() {
        dough=pizzaIngredinetFactory.createDough();
        System.out.println(dough.getName());
    }

    @Override
    public void cut() {
        System.out.println("自己的Cut方法");
    }
}
