package com.designmodel.factory;


import com.designmodel.factory.rawMaterial.Dough;

import java.util.ArrayList;

public  abstract class Pizza {

    String name;
    Dough dough;
    String sauce;
    ArrayList toppings=new ArrayList();

    //抽象方法，在这个方法中，我们需要收集披萨所需的原料，这些原材料来自于工厂
    public abstract void prepare();

    public void bake(){
        System.out.println("BAKE");
    }
    public void cut(){
        System.out.println("cut");
    }
    public void box(){
        System.out.println("box");
    }

    public String getName() {
        return name;
    }
}
