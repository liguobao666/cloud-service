package com.designmodel.factory;

public abstract class PizzaStore {

    //PizzaStore的子类在createPizza方法中，处理对象的实例化
    public Pizza orderPizze(String type) {
        Pizza pizza;
        pizza=createPizza(type);

        //这几个步骤是固定的
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;
    }
    /**
     * 工厂方法用来处理对象的创建，并将这样的行为封装在子类中，客户程序中关于超类的代码就和子类
     * 对象创建代码解耦了
     *
     *工厂方法是抽象的，依赖子类处理对象的创建
     * 工厂方法必须返回一个产品，超类中定义的方法，通常使用到工厂方法的返回值
     * 工厂方法将客户（超类中的代码，如orderPizza()）和实际创建产品的代码分隔开。
     */
    abstract Pizza createPizza(String type);
}
