package com.designmodel.decorator;

/**
 * 超类
 */
public abstract class Beverage {
    String description="未知饮料";

    public String getDescription() {
        return description;
    }

    public abstract double cost();
}
