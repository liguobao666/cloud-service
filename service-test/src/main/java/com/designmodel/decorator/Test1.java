package com.designmodel.decorator;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.InputStream;

public class Test1 {

    public static void main(String[] args) {
        //咖啡
        Beverage beverage=new Espresso();
        System.out.println("一杯不加调料的"+beverage.getDescription()+"  $"+beverage.cost());

        Beverage beverage1=new Espresso();
        beverage1=new Mocha(beverage1);//加mocha
        beverage1=new Whip(beverage1);
        System.out.println("一杯加调料的"+beverage1.getDescription()+"  $"+beverage1.cost());

        /*InputStream;
        FilterInputStream;
        FileInputStream;
        BufferedInputStream*/


    }
}
