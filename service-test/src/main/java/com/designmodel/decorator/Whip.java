package com.designmodel.decorator;

public class Whip extends CondimentDecorator {

    /**
     * 要让Whip能够引用一个Beverage做法如下
     * 1：用一个实例变量记录饮料（被装饰者）
     * 2：通过构造方法将饮料实例变量记录到Mocha中。
     */
    Beverage beverage;

    public Whip(Beverage beverage){
        this.beverage=beverage;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription()+", Whip";
    }

    @Override
    public double cost() {
        //要计算带Mocha的咖啡，被装饰者对象+mocha的价钱
        return 0.8+beverage.cost();
    }
}