package com.designmodel.decorator;

/**
 * 装饰者父类，必须能够取代Beverage,所以继承Beverage
 */
public abstract class CondimentDecorator extends Beverage {

    /**
     * 所有调料装饰者都必须重新实现getDescription()方法。
     * @return
     */
    @Override
    public abstract String getDescription();


}
