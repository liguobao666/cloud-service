package com.designmodel.decorator.iodecorator;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class InputTest {

    public static void main(String[] args) throws Exception{
        int c;
        //设置FileInputStream ,先用BufferedInputStream装饰，再用LowerCaseInputStream装饰
        InputStream in=new LowerCaseInputStream(new BufferedInputStream(new FileInputStream("D:\\nio-data.txt")));

        while((c=in.read())>=0){
            System.out.print((char)c);
        }
    }
}
