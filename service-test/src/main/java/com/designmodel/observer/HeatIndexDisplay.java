package com.designmodel.observer;

import com.designmodel.observer.observer.DisplayElement;
import com.designmodel.observer.observer.Observer;
import com.designmodel.observer.subject.Subject;

/**
 * 布告板
 */
public class HeatIndexDisplay implements Observer, DisplayElement {

    private float temperature;

    private float humidity;

    private float pressure;

    private Subject weatherDate;

    public HeatIndexDisplay(Subject weatherDate){
        this.weatherDate=weatherDate;
        weatherDate.redisterObserver(this);
    }

    @Override
    public void update(float temp, float humidity, float pressure) {
        this.temperature=temp;
        this.humidity=humidity;
        this.pressure=pressure;
        display();

    }

    @Override
    public void display() {
        System.out.println(temperature+"。。。。。"+humidity+"....."+pressure);
    }
}
