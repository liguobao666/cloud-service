package com.designmodel.observer;

import com.designmodel.observer.observer.Observer;
import com.designmodel.observer.subject.Subject;

import java.util.ArrayList;

public class WeatherDate implements Subject {

    private ArrayList observers;

    private float temperature;

    private float humidity;

    private float pressure;

    public WeatherDate() {
        this.observers=new ArrayList();
    }

    @Override
    public void redisterObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        int i=observers.indexOf(observer);
        if(i>0){
            observers.remove(i);
        }
    }

    /**
     * 把状态告诉每一个观察者，因为观察者实现了Observer接口
     */
    @Override
    public void notifyObserver() {
        for(int i=0;i<observers.size();i++){
            Observer observer=(Observer) observers.get(i);
            observer.update(temperature,humidity,pressure);
        }
    }

    public void measurementsChanged(){
        notifyObserver();
    }

    public void setMeasurements(float temperature,float humidity,float pressure){
        this.temperature=temperature;
        this.humidity=humidity;
        this.pressure=pressure;
        measurementsChanged();
    }
}
