package com.designmodel.observer;

public class WeatherStation {

    public static void main(String[] args) {
        //建立通知者
        WeatherDate weatherDate=new WeatherDate();
        //建立布告板
        CurrentConditionsDisplay currentConditionsDisplay=new CurrentConditionsDisplay(weatherDate);
        HeatIndexDisplay zou =new HeatIndexDisplay(weatherDate);

        //发布数据
        weatherDate.setMeasurements(80,65,30);
        weatherDate.setMeasurements(23,222,33);
        weatherDate.setMeasurements(12,1,89);
    }
}
