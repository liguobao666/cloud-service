package com.designmodel.observer;

import com.designmodel.observer.observer.DisplayElement;
import com.designmodel.observer.observer.Observer;
import com.designmodel.observer.subject.Subject;

public class CurrentConditionsDisplay implements Observer, DisplayElement {
    private float temperature;

    private float humidity;

    private Subject weatherDate;

    public CurrentConditionsDisplay(Subject weatherDate){
        this.weatherDate=weatherDate;
        weatherDate.redisterObserver(this);
    }

    @Override
    public void update(float temp, float humidity, float pressure) {
        this.temperature=temp;
        this.humidity=humidity;
        display();

    }

    @Override
    public void display() {
        System.out.println(temperature+"----"+humidity);
    }
}
