package com.designmodel.observer.subject;

import com.designmodel.observer.observer.Observer;

public interface Subject {

    //观察者注册
    public void redisterObserver(Observer observer);
    //观察者删除
    public void removeObserver(Observer observer);

    //当主题改变时，这个方法会被调用，以通知所有的观察者
    public void notifyObserver();
}
