package com.designmodel.observer.observer;

/**
 * 布告板显示时调用此方法
 */
public interface DisplayElement {
    public void display();
}
