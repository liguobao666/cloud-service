package com.designmodel.command.manufacturer;

/**
 * 具体的厂商对象
 */
public class Light {
    public void on(){
        System.out.println("Light 的 on()方法");
    }
    public void off(){
        System.out.println("Light 的 off()方法");
    }
}
