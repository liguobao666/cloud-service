package com.designmodel.command.manufacturer;

public class GrageDoor {
    public void up(){
        System.out.println("GrageDoor 的 up()方法");
    }

    public void down(){
        System.out.println("GrageDoor 的 down()方法");
    }

    public void stop(){
        System.out.println("GrageDoor 的 stop()方法");
    }

    public void lightOn(){
        System.out.println("GrageDoor 的 lightOn()方法");
    }
}
