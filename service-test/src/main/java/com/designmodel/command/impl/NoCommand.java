package com.designmodel.command.impl;

import com.designmodel.command.commandinterface.Command;

public class NoCommand implements Command {

    @Override
    public void execute() {
        System.out.println("什么也不做");
    }

    @Override
    public void undo() {
        System.out.println("什么也不做");
    }
}
