package com.designmodel.command.impl;

import com.designmodel.command.commandinterface.Command;

/**
 * 遥控器具备party
 */
public class MacroCommand implements Command {

    Command[] commands;

    public MacroCommand(Command[] commands){
        this.commands=commands;
    }

    @Override
    public void execute() {
        for(int i=0;i<commands.length;i++){
            commands[i].execute();
        }

    }

    @Override
    public void undo() {

    }
}
