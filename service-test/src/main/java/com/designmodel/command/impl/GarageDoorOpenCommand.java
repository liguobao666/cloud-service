package com.designmodel.command.impl;

import com.designmodel.command.commandinterface.Command;
import com.designmodel.command.manufacturer.GrageDoor;

public class GarageDoorOpenCommand implements Command {

    GrageDoor grageDoor;


    public GarageDoorOpenCommand(GrageDoor grageDoor) {
        this.grageDoor = grageDoor;
    }

    @Override
    public void execute() {
        grageDoor.up();
    }

    @Override
    public void undo() {

    }
}
