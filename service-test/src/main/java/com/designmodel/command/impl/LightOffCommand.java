package com.designmodel.command.impl;

import com.designmodel.command.commandinterface.Command;
import com.designmodel.command.manufacturer.Light;

public class LightOffCommand implements Command {

    Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    /**
     * 调用接收对象
     */
    @Override
    public void execute() {
        light.off();
    }

    @Override
    public void undo() {
        light.on();
    }
}
