package com.designmodel.command.impl;

import com.designmodel.command.commandinterface.Command;
import com.designmodel.command.manufacturer.Light;

/**
 * 具体的命令类，封装厂商类
 */
public class LightOnCommand implements Command {

    Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    /**
     * 调用接收对象
     */
    @Override
    public void execute() {
        light.on();
    }

    @Override
    public void undo() {
        light.off();
    }
}
