package com.designmodel.command;

import com.designmodel.command.commandinterface.Command;
import com.designmodel.command.controller.RemoteController;
import com.designmodel.command.impl.GarageDoorOpenCommand;
import com.designmodel.command.impl.LightOffCommand;
import com.designmodel.command.impl.LightOnCommand;
import com.designmodel.command.impl.MacroCommand;
import com.designmodel.command.manufacturer.GrageDoor;
import com.designmodel.command.manufacturer.Light;
import org.junit.Test;

public class RemoteLoader {
    public static void main(String[] args) {
        RemoteController remoteController=new RemoteController();
        //电灯
        Light light=new Light();
        //门
        GrageDoor grageDoor=new GrageDoor();

        //创建命令对象
        LightOnCommand lightOnCommand=new LightOnCommand(light);
        LightOffCommand lightOffCommand=new LightOffCommand(light);

        GarageDoorOpenCommand opendoor=new GarageDoorOpenCommand(grageDoor);

        //将命令插入到遥控板
        remoteController.setCommand(0,lightOnCommand,lightOffCommand);

        remoteController.onButtonWaspushed(0);
        remoteController.offButtonWaspushed(0);
        remoteController.undoButtonWashPushed();//撤销操作
    }

    @Test
    public void test1(){
        RemoteController remoteController=new RemoteController();

        //厂商具体的类
        Light light=new Light();
        GrageDoor grageDoor=new GrageDoor();

        //具体命令
        LightOnCommand lightOnCommand=new LightOnCommand(light);
        LightOffCommand lightOffCommand=new LightOffCommand(light);

        GarageDoorOpenCommand opendoor=new GarageDoorOpenCommand(grageDoor);


        //创建数组，一个记录开命令，一个记录关命令
        Command[] partyOn={lightOnCommand,opendoor};
        Command[] partyOff={lightOffCommand};

        //创建俩个宏命令，一个放置开命令集合，一个放置关命令集合
        MacroCommand paron=new MacroCommand(partyOn);
        MacroCommand paroff=new MacroCommand(partyOff);

        //将宏命令插入到相应位置
        remoteController.setCommand(0,paron,paroff);

        //执行操作
        remoteController.offButtonWaspushed(0);
        System.out.println("==============================");
        remoteController.onButtonWaspushed(0);


    }
}
