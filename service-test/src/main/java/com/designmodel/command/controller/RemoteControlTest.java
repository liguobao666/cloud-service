package com.designmodel.command.controller;

import com.designmodel.command.impl.GarageDoorOpenCommand;
import com.designmodel.command.impl.LightOnCommand;
import com.designmodel.command.manufacturer.GrageDoor;
import com.designmodel.command.manufacturer.Light;

/**
 * 这是命令模式的客户
 */
public class RemoteControlTest {
    public static void main(String[] args) {
        SimpleRemoteControl remote=new SimpleRemoteControl();

        Light light=new Light();
        LightOnCommand lightOnCommand=new LightOnCommand(light);

        remote.setCommand(lightOnCommand);
        remote.buttonWasPressed();

        System.out.println("===============================");

        GrageDoor door=new GrageDoor();
        GarageDoorOpenCommand gara=new GarageDoorOpenCommand(door);

        remote.setCommand(gara);
        remote.buttonWasPressed();
    }
}
