package com.designmodel.command.controller;

import com.designmodel.command.commandinterface.Command;
import com.designmodel.command.impl.NoCommand;

/**
 * 遥控器
 */
public class RemoteController {
    Command[] oncommands;
    Command[] offcommands;
    Command undoCommand;//记录前一个命令


    public RemoteController() {
        oncommands=new Command[7];
        offcommands=new Command[7];

        Command noCommand=new NoCommand();

        for(int i=0;i<7;i++){
            oncommands[i]=noCommand;
            offcommands[i]=noCommand;
        }
        undoCommand=noCommand;
    }

    /**
     *
     * @param sloat 插槽位置
     * @param onCommand 开命令
     * @param offCommand 关命令
     */
    public void setCommand(int sloat,Command onCommand,Command offCommand){
        oncommands[sloat]=onCommand;
        offcommands[sloat]=offCommand;
    }

    public void onButtonWaspushed(int slot){
        oncommands[slot].execute();
        undoCommand=oncommands[slot];//记录下命令
    }

    public void offButtonWaspushed(int slot){
        offcommands[slot].execute();
        undoCommand=offcommands[slot];//记录下命令
    }

    /**
     * 执行撤销按钮
     */
    public void undoButtonWashPushed(){
        undoCommand.undo();
    }
}
