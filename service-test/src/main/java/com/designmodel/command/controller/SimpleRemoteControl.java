package com.designmodel.command.controller;

import com.designmodel.command.commandinterface.Command;

/**
 * 遥控器
 */
public class SimpleRemoteControl {
    Command slot;//有一个插槽持有命令，这个命令控制着一个装置

    public SimpleRemoteControl() {
    }


    /**
     * 这个方法用来设置插槽对象，如果这段代码的客户想要改变遥控器按钮的行为，可以多次调用这个行为
     * @param slot
     */
    public void setCommand(Command slot) {
        this.slot = slot;
    }

    /**
     * 当按下按钮时，这个方法会被调用，调用当前插槽的execute方法
     */
    public void buttonWasPressed(){
        slot.execute();
    }
}
