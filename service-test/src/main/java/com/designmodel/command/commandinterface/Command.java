package com.designmodel.command.commandinterface;

/**
 * 命令模式接口
 */
public interface Command {
    public void execute();
    public void undo();
}
