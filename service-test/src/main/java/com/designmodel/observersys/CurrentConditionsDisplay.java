package com.designmodel.observersys;


import java.util.Observable;
import java.util.Observer;

public class CurrentConditionsDisplay implements Observer {
    Observable observable;

    private float tem;

    private float hum;

    public CurrentConditionsDisplay(Observable observable) {
        this.observable=observable;
        observable.addObserver(this);
    }

    @Override
    public void update(Observable obs, Object arg) {

        if(obs instanceof WeatherDate){
            WeatherDate weatherDate=(WeatherDate)obs;
            this.tem=weatherDate.getTemperature();
            this.hum=weatherDate.getHumidity();
            System.out.println(toString());
            System.out.println(arg.toString());
        }

    }

    @Override
    public String toString() {
        return "CurrentConditionsDisplay{" +
                "tem=" + tem +
                ", hum=" + hum +
                '}';
    }
}
