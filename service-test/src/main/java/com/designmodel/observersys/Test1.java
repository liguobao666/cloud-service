package com.designmodel.observersys;

public class Test1 {

    public static void main(String[] args) {
        WeatherDate weatherDate=new WeatherDate();

        CurrentConditionsDisplay currentConditionsDisplay=new CurrentConditionsDisplay(weatherDate);

        weatherDate.setMeasurements(1,1,1);

        weatherDate.setMeasurements(100,11,123);
        weatherDate.setMeasurements(222,56,0);
    }
}
