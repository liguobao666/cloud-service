package com.designmodel.observersys;

import java.util.Observable;

public class WeatherDate extends Observable {

    private float temperature;

    private float humidity;

    private float pressure;


    public void measurementsChanged(Object str){
        setChanged();
        //notifyObservers();
        notifyObservers(str);
    }

    public void setMeasurements(float temperature,float humidity,float pressure){
        this.temperature=temperature;
        this.humidity=humidity;
        this.pressure=pressure;
        measurementsChanged("123123");
    }

    public float getTemperature() {
        return temperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getPressure() {
        return pressure;
    }
}
