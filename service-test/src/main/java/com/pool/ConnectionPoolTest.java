package com.pool;

import com.dh.CountDownLaunch;

import java.sql.Connection;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

public class ConnectionPoolTest {
    //构造一个10个连接的池子
    static  ConnectionPool pool = new ConnectionPool(10);

    //保证所有ConnectionRunner 能够同时开始
    static CountDownLatch start = new CountDownLatch(1);
    //main 线程将会等待所有ConnectionRunner 结束后才能继续执行
    static CountDownLatch end;

    public static void main(String[] args) throws Exception{
        int threadCount=50;
        end=new CountDownLatch(threadCount);

        int count=20;
        AtomicInteger got=new AtomicInteger();
        AtomicInteger notGot=new AtomicInteger();

        for(int i=0;i<threadCount;i++){
            Thread thread=new Thread(new ConnectionRunner(count,got,notGot));
            thread.start();
        }
        start.countDown();
        end.await();
        System.out.println("total invoke: "+(threadCount*count));
        System.out.println(got);
        System.out.println(notGot);
    }

    static class ConnectionRunner implements Runnable{
        int count;
        AtomicInteger got;
        AtomicInteger notGot;

        public ConnectionRunner(int count, AtomicInteger got, AtomicInteger notGot) {
            this.count = count;
            this.got = got;
            this.notGot = notGot;
        }

        @Override
        public void run() {
            try{
                start.await();
            }catch (Exception e){

            }
            while (count>0){
                try {
                    //从线程池中获取连接，如果1000ms内无法获取到，将会返回null;
                    //分别统计简介获取的数量 got 和 未获取到的数量 notGot
                    Connection connection=pool.fetchConnection(1000);
                    if(connection != null){
                        try {
                            connection.createStatement();
                            connection.commit();
                        }finally {
                            pool.relaseConnection(connection);
                            got.incrementAndGet();
                        }
                    }else {
                        notGot.incrementAndGet();
                    }
                }catch (Exception e){

                }finally {
                    count--;
                }

            }
            end.countDown();
        }
    }


}
