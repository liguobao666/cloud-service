package com.sort;

import java.util.Arrays;

public class InsertSort {
    public static void main(String[] args) {
        int[] arr={2,1,4,5,3};
        insertSort(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static void insertSort(int arr[]){
        for(int i=1;i<arr.length;i++){
            int inserttVal=arr[i];//定义待插入的数
            int inserttIndex=i-1;//arr[i]前面数的下标
            while(inserttIndex>=0&&inserttVal<arr[inserttIndex]){
                arr[inserttIndex+1]=arr[inserttIndex];
                inserttIndex--;
            }
            arr[inserttIndex+1]=inserttVal;
        }
    }
}
