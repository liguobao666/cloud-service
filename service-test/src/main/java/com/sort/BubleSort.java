package com.sort;

import java.util.Arrays;

/**
 * 冒泡排序
 */
public class BubleSort {
    public static void main(String[] args) {
        int arr[]= {9,7,6,3,2,1,-1};
        sort(arr);
        System.out.println(Arrays.toString(arr));
    }

    public  static  void sort(int arr[]){
        int temp =0;
        for(int i=0;i<arr.length-1;i++){
            for(int j=0;j<arr.length-i-1;j++){
                if(arr[j]>arr[j+1]){
                    temp=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=temp;
                }
            }
        }
    }
}
