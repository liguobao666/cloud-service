package com.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class TestThread implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        return 1;
    }

    public static void main(String[] args) throws Exception {
        TestThread tt = new TestThread();

        //启动线程
        FutureTask<Integer> ft=new FutureTask<Integer>(tt);
        Thread t=new Thread(ft);
        t.start();

        //获取返回值
        System.out.println(ft.isDone());
        Integer i=ft.get();
        System.out.println(i);
        System.out.println(ft.isDone());

    }
}
