package com.thread;

import com.sun.org.apache.xpath.internal.SourceTree;

public class Integertest {
    public static void main(String[] args) {
        Integer a=1,b=2;
        System.out.println("before:a="+a+",b="+b);
        swap(a,b);
        System.out.println("after:a="+a+",b="+b);

        Integer a1=1024;
        Integer a2=Integer.valueOf(1024);
        System.out.println(a1==a2);
    }
    public static void swap(Integer i1,Integer i2){
        Integer tmp=i1;
        i1=i2;
        i2=tmp;
    }
}
