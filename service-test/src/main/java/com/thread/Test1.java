package com.thread;

import javax.validation.constraints.Null;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class Test1 {
    public static void main(String[] args) {
        List<Integer> li= Collections.synchronizedList(new ArrayList<>());
        li.add(10);
        li.add(1);
        li.add(2);
        li.add(3);
        //第一种
        Iterator it = li.iterator();//把元素导入迭代器
        while (it.hasNext()) {
            Object o = it.next();
            System.out.println(o);
        }
        System.out.println("*******************");
        //第二种
        ListIterator it1 = li.listIterator();   //ListIterator继承了 Iterator,多了一个previous功能,双向遍历
        while (it1.hasNext()) {
            Object o = it1.next();
            System.out.println(o);
        }
        while (it1.hasPrevious()) {
            Object o = it1.previous();
            System.out.println(o);
        }

        List<String>  list=new ArrayList<>();

        list.iterator();


    }
}
