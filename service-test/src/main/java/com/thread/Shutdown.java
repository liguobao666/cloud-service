package com.thread;

/**
 * 安全终止线程
 */
public class Shutdown {
    public static void main(String[] args) throws InterruptedException {
        Runner one=new Runner();
        Thread countThread=new Thread(one,"CountThread");
        countThread.start();
        //睡眠1秒，main线程对CountThread进行中断，使CountThread 能够感知中断而结束
        Thread.sleep(1000);
        countThread.interrupt();

        Runner two=new Runner();
        countThread=new Thread(two,"CountThread");
        countThread.start();
        Thread.sleep(1000);
        System.out.println("123333333333");
        two.cancel();
    }

    private static  class Runner implements Runnable{
        private long i;
        private volatile boolean on=true;
        @Override
        public void run() {
            while (on && !Thread.currentThread().isInterrupted()){
                i++;
            }
            System.out.println("Count i="+i);
        }
        public void cancel(){
            on=false;
        }

    }
}
