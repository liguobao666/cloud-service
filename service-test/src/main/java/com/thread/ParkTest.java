package com.thread;

import java.util.concurrent.locks.LockSupport;

public class ParkTest {

    public static void main(String[] args) throws  Exception{

        System.out.println(false&&false);

        System.out.println("----------m1---------");

        Thread t1=new Thread(()->{
            System.out.println("--------1-------");

            LockSupport.park();

            System.out.println("--------2-------");
        });
        t1.start();

        Thread.sleep(5000);

        LockSupport.unpark(t1);
    }
}
