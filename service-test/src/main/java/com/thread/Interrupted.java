package com.thread;

import java.util.concurrent.TimeUnit;

public class Interrupted {
    public static void main(String[] args) throws Exception{
        //SleepRunner 不停的尝试睡眠
        Thread sleepThread=new Thread(new SleepRunner(),"SleepRunner");
        sleepThread.setDaemon(true);
        //BusyRunner不停的运行
        Thread busyThread=new Thread(new BusyRunner(),"BusyRunner");
        busyThread.setDaemon(true);
        sleepThread.run();
        busyThread.run();
        //休眠五秒
        TimeUnit.SECONDS.sleep(5);
        sleepThread.interrupt();
        busyThread.interrupt();
        System.out.println("Sleep:"+sleepThread.isInterrupted());
        System.out.println("busy:"+busyThread.isInterrupted());
        Thread.sleep(2000);
    }

    static class SleepRunner implements Runnable{
        @Override
        public void run() {
            while (true){
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class BusyRunner implements Runnable{
        @Override
        public void run() {
            while (true){

            }
        }
    }
}
