package com.thread;

import sun.misc.Unsafe;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class MyHashMap<K,V>  {

    private Entry[] table;

    public MyHashMap() {
        this.table = new Entry[8];
    }

    public int size() {
        return 0;
    }

    public Object get(Object key) {
        int hash=key.hashCode();
        int index=hash % table.length;
        for(Entry entry=table[index];entry!=null;entry=entry.next){
            if(key.equals(entry.k)){
                Object oldvalue=entry.v;
                return oldvalue;
            }
        }
        return null;
    }

    public Object put(Object key, Object value) {
        int hash=key.hashCode();
        int index=hash % table.length;
        for(Entry entry=table[index];entry!=null;entry=entry.next){
            if(key.equals(entry.k)){
                Object oldvalue=entry.v;
                entry.v=value;
                return oldvalue;
            }
        }
        table[index]=new Entry(key,value,table[index]);
        return null;
    }

    public void clear() {

    }

    public  static  class  Entry<K,V>{
        private K k;
        private V v;
        private Entry next;

        public Entry(K k, V v, Entry next) {
            this.k = k;
            this.v = v;
            this.next = next;
        }
    }

    public static void main(String[] args) {
        Unsafe s=Unsafe.getUnsafe();
        ConcurrentHashMap hash=new ConcurrentHashMap();
        hash.put('a'+1,"");
        hash.get("");
        MyHashMap<String,String> map=new MyHashMap<>();
        map.put("123","dfgsd");
        map.put("456","sdf");
        System.out.println(map.get("123"));
    }


}
