package com.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class TestPool {
    public static void main(String[] args) throws Exception{
        ExecutorService es= Executors.newFixedThreadPool(3);

        List list=new ArrayList();
        for(int i=1;i<=100;i++){
            Future f=es.submit(()->{
                return Thread.currentThread().getName();
            });
            Object s=f.get();
            System.out.println(s.toString());
        }

        es.shutdown();

    }
}
