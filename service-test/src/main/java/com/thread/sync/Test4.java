package com.thread.sync;

/**
 * 线程切换
 * 指令重排
 * 01
 * 10
 * 11
 * 00 指令重排序
 */
public class Test4 {
    public static int a=0, b=0;
    public static int x=0, y=0;

    public static void main(String[] args) throws InterruptedException {
        int count=0;
        while (true){
            count++;
            a=0;
            b=0;
            x=0;
            y=0;
            Thread t1= new Thread(()->{
                a=1;
                x=b;
            },"t1");

            Thread t2= new Thread(()->{
                b=1;
                y=a;
            },"t2");

            t1.start();
            t2.start();

            t1.join();
            t2.join();
            System.out.println(x+"-----"+y);
            if (x==0&&y==0){
                break;
            }
        }
    }
}
