package com.thread.sync;

import org.openjdk.jol.info.ClassLayout;

public class Test implements Runnable{
    private static  int count=0;

    /**
     * 实例数据
     * 对象头
     * 对齐填充
     */
    private static LgbLock lock=new LgbLock();

    public static void main(String[] args) {
        lock.state=100;
        System.out.println(Long.toHexString(lock.hashCode()));
        System.out.println(ClassLayout.parseInstance(lock).toPrintable());
    }

    /**
     * 特殊的偏向锁偏向锁
     */
    @org.junit.Test
    public void test1() throws Exception{
        Thread.sleep(5000);
        LgbLock locks=new LgbLock();
        System.out.println(Long.toHexString(lock.hashCode()));
        System.out.println(ClassLayout.parseInstance(locks).toPrintable());
    }

    /**
     * 偏向锁
     * @throws Exception
     */
    @org.junit.Test
    public void test2() throws Exception{
        Thread.sleep(5000);
        LgbLock a=new LgbLock();
        synchronized (a){
            System.out.println(Long.toHexString(lock.hashCode()));
            System.out.println(ClassLayout.parseInstance(a).toPrintable());
        }
    }


    /**
     * 偏向锁变成轻量锁
     * @throws Exception
     */
    @org.junit.Test
    public void test3() throws Exception{
        Thread.sleep(5000);
        LgbLock a = new LgbLock();

        Thread thread1= new Thread(){
            @Override
            public void run() {
                synchronized (a){
                    System.out.println("thread1 locking");
                    System.out.println(ClassLayout.parseInstance(a).toPrintable()); //偏向锁
                }
            }
        };
        thread1.start();
        thread1.join();
        Thread.sleep(10000);

        synchronized (a){
            System.out.println("main locking");
            System.out.println(ClassLayout.parseInstance(a).toPrintable());//轻量锁
        }
    }

    /**
     * 重量锁
     * @throws Exception
     */
    @org.junit.Test
    public void test4() throws Exception{
        Thread.sleep(5000);
        LgbLock a = new LgbLock();
        Thread thread1 = new Thread(){
            @Override
            public void run() {
                synchronized (a){
                    System.out.println("thread1 locking");
                    System.out.println(ClassLayout.parseInstance(a).toPrintable());
                    try {
                        //让线程晚点儿死亡，造成锁的竞争
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread thread2 = new Thread(){
            @Override
            public void run() {
                synchronized (a){
                    System.out.println("thread2 locking");
                    System.out.println(ClassLayout.parseInstance(a).toPrintable());
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
    }
    @Override
    public void run() {
        synchronized (lock){
            count++;
        }
    }
}
