package com.thread.sync;

public class Test3 {


    public static void main(String[] args) {
        Print print = new Print();

        new Thread(()->{
            try {
                print.print("a",1,2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        new Thread(()->{
            try {
                print.print("b",2,3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        new Thread(()->{
            try {
                print.print("c",3,1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();


    }

    static class Print{
        int flag =1;
        public void print(String content,int waitFlag,int nextFlag) throws InterruptedException {
            while (true){
                synchronized (this){
                    while (flag!=waitFlag){
                        this.wait();
                    }
                    Thread.sleep(1);
                    System.out.println(content);
                    flag=nextFlag;
                    this.notifyAll();
                }
            }
        }
    }
}
