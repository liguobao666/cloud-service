package com.thread.sync;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TestRet1 {

    static ReentrantLock lock =new ReentrantLock();

    static boolean pg=false;

    static boolean mon=false;

    static Condition c1=lock.newCondition();

    static Condition c2=lock.newCondition();

    public static void main(String[] args) throws InterruptedException {
        new Thread(()->{
            lock.lock();
            System.out.println("有没有女人:"+pg+"");
            if (!pg){
                System.out.println("等女人");
                try {
                    c1.await();
                    System.out.println("女人来了，我被唤醒了");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("有女人开始共工作");
            lock.unlock();

        },"jack").start();

        new Thread(()->{
            lock.lock();
            System.out.println("有没有钱:"+pg+"");
            if (!mon){
                System.out.println("等钱");
                try {
                    c2.await();
                    System.out.println("钱来了，我被唤醒了");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("有钱开始共工作");
            lock.unlock();

        },"tom").start();

        Thread.sleep(1000);

        new Thread(()->{
            lock.lock();
                //pg=true;
                //mon=true;
                c1.signal();
                c2.signal();
            lock.unlock();
        }).start();


    }
}
