package com.thread.sync;

public class Test2 {
    static  Object lock =new Object();
    public static void main(String[] args) throws InterruptedException {

        Thread thread = new Thread(()->{
            synchronized (lock) {
                if (true){
                    System.out.println("123");
                    try {
                        lock.wait(1);
                        System.out.println("123");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        thread.start();
        Test2 test2= new Test2();
        test2.a();
    }

    public synchronized  void  a() throws InterruptedException {
        System.out.println("22");
        this.wait();
    }
}
