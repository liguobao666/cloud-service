package com.thread.sync;

public class SynchronizedDemo {
    public int i;

    public static void main(String[] args) {
        SynchronizedDemo demo=new SynchronizedDemo(1);
        demo.method();

    }

    public void method() {
        synchronized (this) {
            while (true){
                System.out.println(i);
            }
        }
    }

    public synchronized void method1() {
        System.out.println("Hello World!");
    }

    public SynchronizedDemo(int i){
        this.i=i;
    }


}
