package com.thread.sync;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TestRet {

    public static void main(String[] args) throws InterruptedException {

        Lock lock =new ReentrantLock();

        Thread t1= new Thread(()->{
            try {
                lock.lockInterruptibly();
                TimeUnit.SECONDS.sleep(5);
                System.out.println("t1");
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
            System.out.println("11111111111111111");

        },"t1");
        t1.start();

        Thread.sleep(1000);

        Thread t2 = new Thread(()->{
            try {
                lock.lockInterruptibly();
                TimeUnit.SECONDS.sleep(2);
                System.out.println("t2");
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                lock.unlock();
            }

        },"t2");
        //t2.start();

        Thread.sleep(1000);
        t1.interrupt();


    }
}
