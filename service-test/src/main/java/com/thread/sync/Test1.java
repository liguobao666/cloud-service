package com.thread.sync;

public class Test1 {

    static Object key = new Object();

    private static boolean p1 = false;

    private static boolean p2 = false;

    public static void main(String[] args) {

        new Thread(()->{
            synchronized (key){
                while (!p1){
                    try {
                        key.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("A");
            }
        },"1").start();

        new Thread(()->{
            synchronized (key){
                while (!p2){
                    try {
                        key.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("B");
            }
        },"2").start();

        new Thread(()->{
            synchronized (key){
                p1=true;
                p2=true;
                key.notify();
            }
        },"3").start();

    }
}
