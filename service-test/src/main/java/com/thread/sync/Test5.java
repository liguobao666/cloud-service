package com.thread.sync;

import java.util.concurrent.ConcurrentHashMap;

public class Test5 {
    static int a=0;
    static boolean b=false;

    public static void main(String[] args) throws InterruptedException {
        //(32+32/2+1) 最大最近的2的幂次方
        ConcurrentHashMap map = new ConcurrentHashMap<String,String>(32);
        map.size();
        new Thread(()->{
            a=50;
            b=true;
        }).start();

        Thread.sleep(1000);

        System.out.println(a+"..."+b);
    }
}
