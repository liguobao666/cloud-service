package com.thread;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

public class ForkJoinTest {
    public static void main(String[] args) {
        ForkJoinPool forkJoinPool = new ForkJoinPool(5);

        forkJoinPool.invoke(new Task1(5));

    }
}

class Task1 extends RecursiveTask<Integer>{
    int n;

    public Task1(int n){
        this.n=n;
    }
    @Override
    protected Integer compute() {
        if (n==1){
            return n;
        }
        Task1 task1 =new Task1(n-1);
        //分叉
        task1.fork();
        //join()
        int result=n+task1.join();
        return result;
    }
}
