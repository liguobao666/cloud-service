MQTT 消息队列遥测传输，是ISO标准下基于 发布/订阅 范式的消息协议，他工作在 TCP/IP 上
是为硬件性能低下的远程设备以及网络状况糟糕的情况下设计的 发布/订阅 型消息协议，为此，他需要一个
消息中间件。

服务品质(QOS)

    主条目：服务品质
    
    服务品质指的是交通优先级和资源预留控制机制，而不是接收的服务品质。 服务品质是为不同应用程序，用户或数据流提供的不同优先级的能力，或者也可以说是为数据流保证一定的性能水平的能力。
    
    以下是每一个服务品质级别的具体描述
    
    最多一次传送 (只负责传送，发送过后就不管数据的传送情况)
    至少一次传送 (确认数据交付)
    正好一次传送 (保证数据交付成功)



**Mosquitto**
    mosquitto---an MQTT broker
    
    程序文件说明
    mosquitto – 代理器主程序
    mosquitto.conf – 配置文件
    mosquitto_passwd – 用户密码管理工具
    mosquitto_tls – very rough cheat sheet for helping with SSL/TLS
    mosquitto_pub – 用于发布消息的命令行客户端
    mosquitto_sub – 用于订阅消息的命令行客户端
    mqtt – MQTT的后台进程
    libmosquitto – 客户端编译的库文件
    
**纲要**
  `mosquitto [-c config file] [ -d | --daemon ] [-p port number] [-v]`
   
   **选项：**
   
      `-c 或者 -config-file`
            从文件中加载配置。如果没有给出，则使用mosquito .conf(5)中描述的默认值。
            
      `-d 或者 -daemon`
            以守护线程方式运行 mosquitto  
            
      `-p 或者 -port` 
              监听指定的端口，而不是默认的1883端口。这个操作将覆盖配置文件中指定的端口。可以多次指定，以打开监听不同端口的多个套接字。此套接字将绑定到所有网络接口
       
       -v 或者 -verbose 
             使用详细的日志记录。这相当于将log_type设置为配置文件中的所有值。这将覆盖配置文件中给出的日志选项。     
**Configuration**   
         
   可以使用mosquito .conf(5)中描述的配置文件配置代理，这是mosquito的主要信息来源。SSL/TLS支持所需的文件在mosquito - to- TLS(7)中进行了描述。         
            
**Broker Status**      
      
   
       客户端可以通过订阅$SYS层次结构中的主题来查找关于代理的信息，在订阅时，每个客户机只发送一次标记为静态的主题。所有其他主题每隔几秒更新一次sys_interval。如果sys_interval为0，则不发送更新。       
       
       1.`$SYS/broker/bytes/received ` ---自代理启动以来接收的字节总数      
       
       2.`$SYS/broker/bytes/sent` ---代理启动后发送的总字节数。
       
       3.`$SYS/broker/clients/connected, 
         $SYS/broker/clients/active (deprecated)`---当前连接的客户数量。
         
       4.`$SYS/broker/clients/maximum`---同时连接到代理的最大客户机数量。
       
       5.`$SYS/broker/clients/total`---当前连接到代理并在代理上注册的 活动和非活动 客户机的总数。
       
       6`$SYS/broker/connection/#`---当将桥接配置到代理或从代理配置到代理时，通常的做法是提供一个状态主题，该主题指示连接的状态。
                                            默认情况下，这在$SYS/broker/connection/中提供。如果主题的值为1，则连接是活动的，如果值为0，则连接不是活动的。有关桥梁的更多信息，请参阅下面的桥梁部分。
        
       7.`$SYS/broker/load/bytes/received/+`---代理在不同时间间隔内接收的字节数的移动平均值。层次结构的最后一个“+”可以是1min、5min或15min。返回的值表示在1分钟内接收的字节数，平均超过1、5或15分钟。      
        
       8.`$SYS/broker/load/messages/sent/+`---代理在不同时间间隔内发送的所有类型MQTT消息的移动平均数。层次结构的最后一个“+”可以是1min、5min或15min。返回的值表示在1分钟内发送的消息数量，平均超过1、5或15分钟。        
                
       9.`$SYS/broker/messages/received`---自代理启动以来接收的任何类型的消息的总数。
       
       10.`$SYS/broker/messages/sent`---自代理启动以来发送的任何类型的消息的总数。
       
       11.`$SYS/broker/publish/messages/dropped`---由于inflight/queuing限制而被删除的发布消息总数。有关更多信息，请参见 mosquitto.conf(5) 中的max_inflight_messages和max_queued_messages选项。
        
       12.`$SYS/broker/publish/messages/received`---自代理启动以来接收的发布消息总数。
       
       13.`$SYS/broker/subscriptions/count`---代理上活动的订阅总数。
   
   
**通配符主题订阅**     

    除了允许客户订阅特定的主题，mosquito还允许在订阅中使用两个通配符。+是用于匹配单个层次结构级别的通配符。例如，对于“a/b/c/d”主题，以下示例订阅将匹配:        
    a/b/c/d
    +/b/c/d
    a/+/c/d
    a/+/+/d
    +/+/+/+ 
    以下订阅将不匹配
    a/b/c
    b/+/c/d
    +/+/+
    
    第二个通配符是#，用于匹配所有后续层次结构级别。以“a/b/c/d”为主题，以下示例订阅将匹配:
    a/b/c/d
    #
    a/#
    a/b/#
    a/b/c/#
    +/b/c/#
    $SYS层次结构不匹配“#”订阅。如果您想观察整个$SYS层次结构，请订阅$SYS/#。
    注意，通配符只能单独使用，因此订阅“a/b+/c”不能有效地使用通配符。#通配符只能用作订阅的最后一个字符。       
    
**Bridges**  

    多个brokers可以通过桥接功能连接在一起 ,当需要在不同位置之间共享信息时，这是非常有用的，但并不是所有的信息都需要共享，
    例如，由于许多其他原因，许多用户运行broker来帮助记录电力使用情况，通过将所有用户broker连接到一个公共broker，可以共享电力使用，
    允许收集和比较所有用户的电力使用情况。其他信息对每个代理都是本地的。
    
    有关配置桥的信息，请参见mosquito .conf(5)。   
    
**Signals**
   
    SIGHUP
        
        在接收到SIGHUP信号后，假设在启动mosquitto时提供了-c参数，那么mosquito to将尝试重新加载配置文件数据。
        并非所有配置参数都可以在不重新启动的情况下重新加载。详见mosquito .conf(5)。 
        
    SIGUSR1
        
        当收到SIGUSR1信号时，mosquitto将把持久性数据库写入磁盘。只有在启用持久性时才对该信号进行操作。
     
    SIGUSR2
    
        SIGUSR2信号会导致mosquito打印出当前订阅树，以及关于保留消息存在于何处的信息。这只是一个测试特性，可以随时删除。
        
**Files**  

    /etc/mosquitto/mosquitto.conf
    Configuration file. See mosquitto.conf(5).
    
    /var/lib/mosquitto/mosquitto.db
    持久消息数据存储位置(如果启用了持久)。
    
    /etc/hosts.allow, 
    /etc/hosts.deny
    通过tcp-wrappers进行主机访问控制，如hosts_access(5)中所述。
    
                                                      
====================================================================================================================                                                      
<h1>mosquitto.conf</h1>

    mosquitto.conf是mosquitto的配置文件，这个文件能被放置在任何地方，只要mosquitto能读取他。

文件格式：

    配置行以变量名开始。变量值与名称之间用一个空格分隔
    
<h2>身份验证</h2>
    
    最简单的方式是不进行身份验证，这是默认选项。使用基于证书的SSL/TLS的选项cafile/capath、certfile和keyfile提供了未经身份验证的加密支持。
    
    MQTT提供 用户名/密码 身份验证作为协议的一部分，使用password_file选项定义有效的用户名和密码。一定要使用网络加密，如果您正在使用此选项，否则用户名和密码将容易被拦截。
    
    使用`per_listener_settings`控制是全局需要密码，还是基于每个侦听器需要密码。
    
    使用基于证书的加密时，有三个选项会影响身份验证。
    
            1.require_certificate：true|false。如果为false，客户机的SSL/TLS组件将验证服务器，但是客户机不需要为服务器提供任何东西(证书):身份验证仅限于在用户名/密码中构建的MQTT;
                                   如果设置为true,客户端必须提供有效的证书才能成功连接。
            2.use_identity_as_username 和 use_subject_as_username 参数密切相关。
              如果use_identity_as_username设置为true,use_identity_as_username将使用客户机证书中的公共名称(CN)，而不是MQTT用户名。并且password将不被使用。
              如果use_identity_as_username为false，客户机必须通过MQTT选项进行正常身份验证(如果password_file要求)。同样的原则也适用于use_subject_as_username选项，但是整个证书主题被用作用户名，而不仅仅是CN。
    
    psk加密：         
            当通过psk_hint和psk_file选项使用基于pre-shared-key(psk)的加密时，客户机必须提供有效的标识和密钥，以便在进行任何MQTT通信之前连接到代理。
            如果 use_identity_as_username 设置为true,出于访问控制的目的，使用PSK标识而不是MQTT用户名.
            如果use_identity_as_username为false，那么如果使用password_file选项，客户机仍然可以使用MQTT 用户名/密码 进行身份验证。
    
    基于证书和PSK的加密都是基于每个侦听器配置的。
    
    可以创建身份验证插件来增加password_file、acl_file和psk_file选项，例如基于SQL的查找。
    
    可以同时支持多个身份验证方案，可以为上面描述的所有不同加密选项创建一个侦听器，从而创建大量的身份验证方法。
    
<h2>通用参数<h2>

 `acl_file file path`
 
        设置访问控制列表文件的路径。如果定义了，文件的内容将用于控制客户机对代理上的主题的访问。
        如果定义了此参数，则只有列出的题主具有访问权限。主题访问添加了格式行:`topic [read|write|readwrite] <topic>`，<topic>选项可以匹配通配符
        
        假设allow_anonymous为true，第一组主题应用于匿名客户机。用户特定主题acl添加在用户行之后，如下所示:user <username>，这里提到的用户名与password_file中的用户名相同。它不是clientid。
    
        还可以基于主题中的模式替换定义acl。表单与topic关键字相同，但是使用pattern作为关键字。
        pattern [read|write|readwrite] <topic>
            可以替代的模式有:
                %c 匹配客户端的client id
                %u 匹配客户端的用户名
            替换模式必须是该层次结构的唯一文本。模式acl适用于所有用户，即使之前已经给出了“user”关键字 
                例如：pattern write sensor/%u/data
                允许访问桥梁连接消息:pattern write $SYS/broker/connection/%c/state
                
 `allow_anonymous [ true | false ]（允许匿名访问）`
 
        boolean，该值确定不提供用户名而连接的客户机是否允许连接。如果设置为false，则应该创建另一种连接方法来控制经过身份验证的客户机访问。
        
        如果没有设置其他安全选项，则默认值为true。如果设置了password_file或psk_file，或者加载了实现 用户名/密码 或 TLS-PSK 检查的身份验证插件，
        则allow_anonymous默认值为false。
        
 `allow_duplicate_messages [ true | false ]（允许接收重复的消息）`
 
       如果客户端订阅了多重重叠的主题，例如 foo/# 和 foo/+/baz,然后MQTT接收了一个消息，这个消息能够同时匹配俩个主题，那么客户端应当只接收一次消息。
       
       Mosquitto 跟踪向客户发送的消息，来保证只发送一次的需求。此选项允许禁用此行为，如果您有大量客户机订阅了相同的主题集，
       并且希望将内存使用量降到最低，则此选项可能非常有用（禁用了此选项客户端将会只接收一次消息）
       
       如果您事先知道您的客户端永远不会有重叠订阅，则可以安全地将其设置为true。如果为false，即使QoS=2，您的客户机也必须能够正确处理重复消息。
       
       默认值true。
       
 `allow_zero_length_clientid [ true | false ]`
 
        MQTT 3.1.1允许客户机连接一个零长度的 client id，并让broker 为它们生成一个客户机id。使用此选项允许/不允许此行为。默认值为true。
        还请参见auto_id_prefix选项。
        
 `auth_opt_* value`
 
        要传递给auth插件的选项。参见特定的**插件**说明。
        
 `auth_plugin file path`
 
        指定一个外部的模块用于身份验证和访问控制，这允许创建自定义 用户名/密码 和 访问控制函数。
        可以指定多次加载多个插件，插件将按照指定的顺序处理。
        
        如果配置文件中使用password_file或acl_file和size auth_plugin，插件检查将在内置检查之后运行。
        
        遇到 reload signal 不会重新加载。
        
 `auth_plugin_deny_special_chars [ true | false ]`
 
        如果为true，则在ACl检查之前执行，将搜索需要检查的客户机的 username/client id，以确定是否存在'+'或'#'字符，
        如果在用户名或客户端id中找到这两个字符中的任何一个，那么在将ACL检查发送到插件之前，ACL检查将被拒绝。
        
        这种检查可以防止恶意用户通过使用这些字符中的一个作为用户名或客户id来绕过ACL检查。这与mosquitto 自身CVE-2017-7650报告的问题相同。、
        
        如果你完全确定,你使用的插件不容易受到这种攻击(即如果你不使用用户名或客户机id在话题)设置该选项为false。
        
        默认值true。
        
        ACL:访问控制列表，是路由器和交换机接口的指令列表，用来控制端口进出的数据包。ACL适用于所有的被路由协议。
            ACL可以过滤网络中的流量，是控制访问的一种网络技术手段（非授权用户只能访问特定网路资源）
            ACL是物联网中保障系统安全性的重要技术。 
 
 `auto_id_prefix prefix`   
        
        如果allow_zero_length_clientid为真，则此选项允许您设置一个字符串，该字符串将前缀做为客户机id，
        以帮助日志中的可视性。默认为自动。 
        
        Reloaded on reload signal.
 
 `autosave_on_changes [ true | false ]`
 
        如果为真，那么mosquitto将计算订阅更改、接收到的保留消息和排队消息的数量，存储在内存中
        如果总数超过autosave_interval，则内存中的数据库将保存到磁盘。如果为false，
        则通过将autosave_interval作为时间(以秒为单位)处理，从而将内存数据库保存到磁盘。
        
        Reloaded on reload signal.  
        
 `clientid_prefixes prefix`   
 
        如果定义了clientid，则只允许具有与clientid_prefixes匹配的前缀的客户端连接到代理，
        例如，在这里设置“secure-”意味着客户机“secure-client”可以连接，但是另一个使用clientid“mqtt”的客户机不能连接。
        默认情况下，所有客户机id都是有效的。
    
 `connection_messages [ true | false ]`
 
        如果设置为true，日志将包含客户机连接和断开连接时的条目。如果设置为false，这些条目将不会出现。
        
        Reloaded on reload signal.
        
 `include_dir dir`
 
        可以使用include_dir选项包含外部配置文件,该目录下的所有.conf文件都将作为配置文件加载。
        
        主配置文件不能包含在这个目录下。
        
        include_dir中的配置文件按大小写敏感的字母顺序加载，每个字母的大写顺序先于相同字母的小写顺序。
        
        如果这个选项被多次使用，那么每个include_dir选项都将按照在主配置文件中写入的顺序被完全处理。
        
 `log_dest destinations`
 
        将日志消息发送到特定的目的地。可能的目标是:stdout stderr syslog topic。
        
        stdout和stderr在指定的输出上登录到控制台。
        
        例如：log_dest file /var/log/mosquitto.log
        
 `log_facility local facility`
         
         如果使用syslog日志记录(不在Windows上)，消息将被默认记录到“守护进程”工具中。使用log_facility选项来选择要
         登录到local0到local7中的哪一个。选项值应该是一个整数值，例如。"log_facility 5"来使用local5。
 
 `log_timestamp [ true | false ]`
 
        如果设置为true，将向每个日志条目添加一个时间戳值。默认值为true
        
        Reloaded on reload signal.
        
 `log_timestamp_format format`
 
        设置时间戳格式，例如：log_timestamp_format %Y-%m-%dT%H:%M:%S
        
        Reloaded on reload signal.
        
 `log_type types`
 
        选择记录的日志类型，可选的日志类型有：debug, error, warning, notice, information, subscribe, unsubscribe, websockets, none, all、
        
        默认是 error warning notice information
        
        Reloaded on reload signal.
        
 `max_inflight_bytes count`
 
        QoS 1和2消息将被允许在flight中，直到达到此字节限制为止。默认值为0(没有限制)参见max_inflight_messages选项。
        
        QoS（Quality of Service，服务质量）是网络的一种安全机制， 是用来解决网络延迟和阻塞等问题的一种技术
        
        QOS 012 参考网址：https://www.cnblogs.com/myfrank/p/10808683.html
        
        最多一次（0）：没有回应，在协议中灭有定义重传的语义，消息可能到大服务器1次，也可能根本不会到达。
        
        最少一次（1）：服务器接收到消息会确认，通过传输一个PUBACk信息。如果有一个可以辨认的传输失败，无论是通讯连接还是发送设备，
                     还是过了一段时间确认信息没有收到，发送方都会将消息头的DUP位置1，然后再次发送消息。消息最少一次到达服务器。
                     SUBSCRIBE和UNSUBSCRIBE都使用level 1 的QoS。 
                     如果客户端没有接收到PUBACK信息（无论是应用定义的超时，还是检测到失败然后通讯session重启），
                     客户端都会再次发送PUBLISH信息，并且将DUP位置1。 当它从客户端接收到重复的数据，
                     服务器重新发送消息给订阅者，并且发送另一个PUBACK消息。
                     
        只一次（2）：只有一次的传输 在QoS level 1上附加的协议流保证了重复的消息不会传送到接收的应用。这是最高级别的传输，当重复的消息不被允许的情况下使用。这样增加了网络流量，但是它通常是可以接受的，因为消息内容很重要。 QoS level 2在消息头有Message ID。
        
        Reloaded on reload signal.
 
 `max_inflight_messages count`
         
        客户端心跳消息的最大并发数
        可同时传输的QoS 1或 2 最大数量。这包括当前正在握手的消息和正在重试的消息。默认为20。设置为0，没有最大值。
        如果设置为1，这将保证按顺序传递消息。
        
        Reloaded on reload signal.
        
 `max_keepalive value`
       
        对于MQTT v5 客户端，server可能发送一个 “server keeplive”的值覆盖客户机的 keepalive。
        这是一种机制，用于表明服务器将比预期更早地断开客户机连接，客户端应该使用新的keepalive值，
        max_keepalive选项允许您指定客户端与服务器的连接只能小于或等于keepalive，这只适用于MQTT v5客户机
        
        Reloaded on reload signal.
        
 `max_packet_size value`   
 
        对于MQTT v5 客户端 ，该值指示客户机不会接收大于此指定值的数据包。
        这个参数适用于完整的MQTT包，而不仅仅是有效负载。将此选项设置为正值将把最大数据包大小设置为该字节数
        如果客户机发送的数据包大于此值，则断开连接    
        
        禁止将其设置为低于20字节，因为即使使用较小的有效负载，也可能会干扰正常的客户机操作。
        
        Reloaded on reload signal.
        
 `max_queued_bytes count`
 
 
 `max_queued_messages count`
        
       客户端心跳消息的最大并发数。
       
       队列中(每个客户端)所要持有的 QoS 1或2 消息的最大数量。默认值100，如果为0则不设置最大值（不建议）
       
 `memory_limit limit`
 
        此选项设置broker将分配的堆内存字节的最大数量，对代理使用内存做了严格限制。超过此值的内存请求将被拒绝
    
        此选项仅在存在内存跟踪支持时可用，
    
        Reloaded on reload signal.重新加载时不会释放内存。
        
 `message_size_limit limit`
 
        此选项设置broker将允许的最大发布有效负载大小。默认值为0，这意味着所有有效的MQTT消息都被接受。
        MQTT的最大有效负载大小为268435455字节。
        
 `password_file file path`
        
        密码文件的路劲。可以使用mosquitto_passwd(1)实用程序创建该文件
        如果mosquitto没有TLS支持,然后密码文件应该是文本文件，每一行的格式为“username:password”(可选操作)。
        如果allow_anonymous设置为false，只有在此文件中定义的用户才能连接。
        allow_anonymous设置为true：???????
        
        Reloaded on reload signal,当前加载的用户名和密码数据将被释放并重新加载。已经连接的客户端不会受到影响。
        
        
 <h2>Listeners</h2>
        
        mosquitto收听的网络端口可以用监听器控制。可以重写默认侦听器选项，并创建更多的侦听器。
        
 `bind_address address`
 
        只监听指定 IP address/hostname 上的传入网络连接,这对于限制对某些网络接口的访问非常有用.\
        如果只监听本机则使用："bind_address localhost"。
        
        Not reloaded on reload signal.
        
 `bind_interface device` 
 
        只监听指定接口上的传入网络连接。这类似于bind_address选项，但是当一个接口有多个地址或者地址可能发生变化时，它很有用。
        将此选项与bind_address一起用于默认侦听器。应该注意确保绑定到的地址位于绑定到的接口上，如果将bind_interface设置为eth0，
        并将bind_address设置为127.0.0.1，然后代理将正确启动，但您将无法连接
        
        这个选项只在Linux有用，切需要权限。
        
        Not reloaded on reload signal.
        
 `http_dir directory`
 
        当侦听器使用websockets协议时，允许提供HTTP数据，将http_dir设置为目录，其中包含您希望提供的文件，
        如果未指定此选项，则无法进行正常的http连接。
        Not reloaded on reload signal.
        
 `listener port [bind address/host]`
 
        监听指定端口上的传入网络连接。第二个可选参数允许侦听器绑定到特定的ip address/hostname,
        如果使用该变量，且不使用全局bind_address或端口选项，则默认侦听器将不会启动。
        
        bind address/host选项允许通过传递IP地址或主机名将此侦听器绑定到特定的IP地址,
        对于websockets侦听器，这里只能传递一个IP地址。
        
        此选项可以指定多次。
        
        Not reloaded on reload signal.
        
 `max_connections count`
 
        为当前侦听器设置连接的最大客户机总数。设置为-1具有“无限”连接
        
        Not reloaded on reload signal.
        
 `maximum_qos count`
 
        使用此侦听器时所允许的QoS值,默认为2，可以使用任何的QoS。MQTT v5客户端将被限制
        MQTT v3.1.1 不会受到限制。向此侦听器发布服务质量过高的客户机将断开连接。
        
        Not reloaded on reload signal.
        
 `max_topic_alias number`   
 
        此选项设置MQTT v5客户机允许创建的最大数量主题别名,它适用于每个别名。默认是10，设置0禁用主题别名。
        
        Not reloaded on reload signal.
        
 `mount_point topic prefix`
        
        此选项用于隔离客户端组，当客户机连接到使用此选项的侦听器时，string参数附加到此客户机的所有主题的开头。
        当向客户机发送任何消息时，将删除此prefix，这意味着连接到具有挂载点示例的侦听器的
        客户机只能看到在主题层次结构示例和下面发布的消息。
        
        Not reloaded on reload signal.
        
 `port port number`
 
        设置网络端口，默认值为1883
        
        Not reloaded on reload signal.
        
 `protocol value`
 
 <h2>基于证书的SSL/TLS支持</h2>
 
 所有侦听器都可以使用以下选项来配置基于证书的SSL支持。参见“Pre-shared-key based SSL/TLS support”。
 
 `cafile file path`
 
        必须提供至少一个cafile或capath来支持SSL。
        
        cafile用于定义包含受信任的PEM编码CA证书的文件的路径。
        
 `capath directory path`
 
        必须提供至少一个cafile或capath来支持SSL。
        
        capth 被定义成一个目录，包含PEM 编码 CA 证书，要使capath正确工作，证书文件必须以".pem"
        作为文件结尾，每次添加/删除证书时，必须运行“openssl rehash <path to capath>”。
 
 `certfile file path`
 
        PEM编码的服务器证书的路径。
        
 `ciphers cipher:list`
 
        密码列表，用冒号分隔，可用的密码可以使用“opensslciphers”命令获得。
        
 `crlfile file path`
 
        如果require_certificate 设置为true，您可以创建一个证书撤销列表文件来撤销对特定客户端证书的访问。
        如果您已经这样做了，请使用crlfile指向PEM编码的撤销文件。
        
 `dhparamfile file path`
 
        允许使用临时 DH key,保证了远期安全性，监听器必须加载DH参数，通过dhparamfile选项指定
        dhparamfile可以用命令生成，例如：
        
            openssl dhparam -out dhparam.pem 2048
 
 `keyfile file path` 
 
        到PEM编码密钥文件的路径。
 
 `require_certificate [ true | false ]`
        
        默认情况下，启用SSL/TLS的侦听器将以类似于启用https的web服务器的方式运行，
        在这种情况下，服务器有一个由CA签名的证书，客户机将验证它是否是受信任的证书，总的目标是加密网络流量
        设置 require_certificate 为true，客户端必须提供有效的证书，以便进行网络连接，
        这允许在MQTT提供的机制之外控制对broker的访问。
        
 `tls_engine engine`
        
        一个有效的openssl引擎id。这些可以用openssl引擎命令列出。
        
 `tls_engine_kpass_sha1 engine_kpass_sha1`
 
        SHA1 私有key作为TLS引擎，一些TLS引擎(如TPM引擎)可能需要使用密码才能访问。
        该选项允许直接向引擎输入十六进制编码的密码SHA1散列，而不是提示用户输入密码。
        
 `tls_keyform [ pem | engine ]`
        
        指定在进行TLS连接时使用的私钥的类型。这可以是pem或者engine
        
 `tls_version version`
 
        配置TLS协议的版本，可能的值是tlsv1.3, tlsv1.2 and tlsv1.1。如果未设置，则使用默认值，即允许所有TLS v1.3、v1.2和v1.1。
        
 `use_identity_as_username [ true | false ]`
 
        如果require_certificate 设置为true，use_identity_as_username必须设置为true。
        此侦听器将不使用password_file选项。
        如果 use_identity_as_username，use_subject_as_username都设置为true，那么use_identity_as_username优先级要高。
        
 `use_subject_as_username [ true | false ]`
 
        如果require_certificate为true，可以将use_subject_as_username设置为true，以使用客户机证书中的完整subject值作为用户名。如果设置为true
        将不使用password_file选项。
        
        主题将以类似于CN=test client,OU=Production,O=Server,L=Nottingham,ST=Nottingham,C=GB的形式生成。
        
 <h2>基于预共享密钥的SSL/TLS支持</h2>
 
 `ciphers cipher:list`
        
        当我们使用 PSK时，使用的加密密码将从可用的PSK密码列表中选择，可用密码的列表可以使用“openssl密码”命令获取
 
 `psk_hint hint`
 
 
 `tls_version version`
        
        配置要用于此侦听器的TLS协议的版本。tlsv1.3, tlsv1.2 and tlsv1.1，如果不设置允许所有
        
 `use_identity_as_username [ true | false ]`
 
        是否使用psk标识用作用户名，用户名将被正常检查，因此必须使用password_file或其他身份验证检查方法。不使用密码。
        
       
 =============================================================================================
<h1>mosquitto_passwd</h1>
 
    管理密码文件的工具，用户名不能包含 “:”。
    
    mosquitto_passwd [ -c | -D ] passwordfile username
    
    mosquitto_passwd -b passwordfile username password
    
    mosquitto_passwd -U passwordfile
    
    --------------------------------------------------------------------------
    -b
    
    以批处理模式运行。这允许在命令行中提供密码，这很方便，但是应该谨慎使用，因为密码将在命令行和命令历史记录中可见。
    --------------------------------------------------------------------------
    -c
    
    创建一个新的密码文件，如果密码文件存在则覆盖。
    --------------------------------------------------------------------------
    -D
    
    从密码文件中删除指定的用户。
    --------------------------------------------------------------------------
    -U
    
    此选项可用于将具有纯文本密码的密码文件升级/转换为使用散列密码的密码文件,它将修改指定的文件。
    它不检测密码是否已经散列,因此，在已经包含散列密码的密码文件上使用它将基于旧散列生成新的散列，
    并使密码文件不可用。
    ---------------------------------------------------------------------------
    passwordfile
    
    要修改的密码文件
    
    username
    The username to add/update/delete.
    
    password
    The password to use when in batch mode.
    
    ----------------------------------------------------------------------------
    Examples
    Add a user to a new password file:
    
    mosquitto_passwd -c /etc/mosquitto/passwd ral
    Delete a user from a password file
    
    mosquitto_passwd -D /etc/mosquitto/passwd ral
    ---------------------------------------------------------------------------
    
       