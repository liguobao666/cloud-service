package com.dh.mqtt.service;

import com.dh.mqtt.configure.MqttSubscriber;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class MqttService {

    private String str;

    @Resource
    private MqttPahoMessageHandler mqttHandler;


    public void send(String topic, String content) {
        // 构建消息
        Message<String> messages = MessageBuilder.withPayload(content).
                setHeader(MqttHeaders.TOPIC, topic).
                setHeader("liguobao","456").build();
        // 发送消息
        mqttHandler.handleMessage(messages);

    }



}
