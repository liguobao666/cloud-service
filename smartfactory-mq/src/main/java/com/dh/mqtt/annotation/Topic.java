package com.dh.mqtt.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 说明：name:主题名称
 *      value：beanName
 *
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Topic {

    String name() default "";

    String value() default "";
}
