package com.dh.mqtt.configure;

import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.endpoint.MessageProducerSupport;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Map;


@Configuration
public class MqttConfigure extends  MqttResource{

    @Resource
    private  ReceiveHandler receiveHandler;

    private static final byte[] WILL_DATA;

    static {
        WILL_DATA = "offline".getBytes();
    }
    private volatile  MqttPahoMessageDrivenChannelAdapter adapter;
    /**
     * 订阅的bean名称
     */
    public static final String CHANNEL_NAME_IN = "mqttInboundChannel";
    /**
     * 发布的bean名称
     */
    public static final String CHANNEL_NAME_OUT = "mqttOutboundChannel";

    private static   String[] topic;




    @Bean(name = "factory")
    public  DefaultMqttPahoClientFactory factory(){
        topic= StringUtils.split(consumerDefaultTopic, ",");
        MqttConnectOptions options=new MqttConnectOptions();
        //每次连接都清空会话信息
        options.setCleanSession(true);
        // 设置连接的用户名
        options.setUserName(username);
        // 设置连接的密码
        options.setPassword(password.toCharArray());
        options.setServerURIs(StringUtils.delimitedListToStringArray(url,","));
        // 设置超时时间 单位为秒
        options.setConnectionTimeout(10);
        // 设置会话心跳时间 单位为秒 服务器会每隔1.5*20秒的时间向客户端发送心跳判断客户端是否在线，但这个方法并没有重连的机制
        options.setKeepAliveInterval(20);
        // 设置“遗嘱”消息的话题，若客户端与服务器之间的连接意外中断，服务器将发布客户端的“遗嘱”消息。
        options.setWill("willTopic", WILL_DATA, defaultQos, false);

        DefaultMqttPahoClientFactory factory=new DefaultMqttPahoClientFactory();
        factory.setConnectionOptions(options);
        return factory;
    }

    /**
     * MQTT信息通道（生产者）
     *
     * @return {@link org.springframework.messaging.MessageChannel}
     */
    @Bean(name = CHANNEL_NAME_OUT)
    public MessageChannel mqttOutboundChannel() {
        return new DirectChannel();
    }

    /**
     * MQTT信息通道（消费者）
     *
     * @return {@link org.springframework.messaging.MessageChannel}
     */
    @Bean(name = CHANNEL_NAME_IN)
    public MessageChannel mqttInboundChannel() {
        return new DirectChannel();
    }

    /**
     * MQTT消息处理器（生产者）
     *
     * @return {@link org.springframework.messaging.MessageHandler}
     */
    @Bean
    @ServiceActivator(inputChannel = CHANNEL_NAME_OUT)
    public MessageHandler mqttOutbound() {
        MqttPahoMessageHandler messageHandler = new MqttPahoMessageHandler(
                producerClientId,
                factory());
        messageHandler.setAsync(true);
        messageHandler.setDefaultQos(defaultQos);
        messageHandler.setDefaultTopic(producerDefaultTopic);
        return messageHandler;
    }

    /**
     * MQTT消息订阅绑定（消费者）
     *
     * @return {@link org.springframework.integration.core.MessageProducer}
     */
    @Bean
    public MessageProducer inbound() {
        // 可以同时消费（订阅）多个Topic
        adapter = new MqttPahoMessageDrivenChannelAdapter(
                        consumerClientId, factory(),
                StringUtils.split(consumerDefaultTopic, ","));
        adapter.setCompletionTimeout(5000);
        adapter.setConverter(new DefaultPahoMessageConverter());
        adapter.setQos(defaultQos);
        // 设置订阅通道
        adapter.setOutputChannel(mqttInboundChannel());

        return adapter;
    }


    /**
     * MQTT消息处理器（消费者）
     *
     * @return {@link org.springframework.messaging.MessageHandler}
     */
    @Bean
    @ServiceActivator(inputChannel = CHANNEL_NAME_IN)
    public MessageHandler handler() {
        return new MessageHandler() {
            @Override
            public void handleMessage(Message<?> message) {
                Object object=message.getHeaders().get("mqtt_receivedTopic");
                String key="";
                for(int i=0;i<topic.length;i++){
                   String str="";
                   if(topic[i].contains("#")){
                        str=topic[i].substring(0, topic[i].indexOf("/#"));
                   }else{
                       str=topic[i];
                   }
                   if(object.toString().contains(str)){
                       key=topic[i];
                       break;
                   }
                }
                Map<String, String> map = receiveHandler.getMap();
                String beanName = map.get(key);
                MqttReceiveHandler mqttReceiveService=(MqttReceiveHandler) receiveHandler.getApplicationContext().getBean(beanName);
                mqttReceiveService.handlerMqttMessage(object.toString(),message.getPayload());
            }
        };
    }

    /**
     * 添加订阅主题
     * @param topicArr
     */
    public void addListerTopic(String[] topicArr){
        if(adapter==null){
            synchronized (MqttPahoMessageDrivenChannelAdapter.class){
                if(adapter==null){
                    adapter = new MqttPahoMessageDrivenChannelAdapter(
                            consumerClientId, factory(),
                            "");
                }

            }
        }
        for(String topic:topicArr){
            adapter.addTopic(topic,defaultQos);
        }
    }

    /**
     * 删除订阅主题
     * @param topic
     */
    public void removeListenTopic(String topic){
        adapter.removeTopic(topic);
    }

   /* @Bean
    public MessageProducerSupport mqttInbound() {
        MqttSubscriber adapter = new MqttSubscriber(consumerClientId,
                factory(), StringUtils.split(consumerDefaultTopic, ","));
        adapter.setCompletionTimeout(5000);
        adapter.setConverter(new DefaultPahoMessageConverter());
        adapter.setQos(defaultQos);
        return adapter;
    }

    @Bean
    public IntegrationFlow mqttInFlow() {
        return IntegrationFlows.from(mqttInbound())
                .transform(p -> p + ", received from MQTT")
                .handle(logger())
                .get();
    }

    private LoggingHandler logger() {
        LoggingHandler loggingHandler = new LoggingHandler("INFO");
        loggingHandler.setLoggerName("siSample");
        return loggingHandler;
    }*/

}
