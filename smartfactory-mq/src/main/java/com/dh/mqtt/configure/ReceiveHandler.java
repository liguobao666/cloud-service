package com.dh.mqtt.configure;

import com.dh.mqtt.annotation.Topic;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ReceiveHandler implements  ApplicationContextAware, InitializingBean {

    private ApplicationContext applicationContext;

    private List<String> topic = new ArrayList<>();

    private Map<String,String> map=new HashMap<>();

    @Override
    public void afterPropertiesSet() throws Exception {
        scanTopicClass();
        System.out.println(topic);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * 查找 用 Topic 注解的 类
     * @throws Exception
     */
    private void scanTopicClass() throws Exception {
        final Map<String, Object> permissionMap = applicationContext.getBeansWithAnnotation(Topic.class);

        for (Map.Entry<String, Object> entry : permissionMap.entrySet()) {
            //serviceName
            String keyName=entry.getKey();
            Object permissionObject=entry.getValue();
            //取得注解信息
            final Class<? extends Object> permissionClass = permissionObject.getClass();
            final Topic annotation = permissionClass.getAnnotation(Topic.class);
            if(annotation != null) {
                topic.addAll(Arrays.asList(annotation.name()));
                map.put(annotation.name(),keyName);
            }
        }
       /* for (final Object permissionObject : permissionMap.values()) {
            final Class<? extends Object> permissionClass = permissionObject.getClass();
            final Topic annotation = permissionClass.getAnnotation(Topic.class);
            if(annotation != null) {
                topic.addAll(Arrays.asList(annotation.name()));
                map.put(annotation.name(),permissionClass.getName());
            }
        }*/
    }

    public Map<String, String> getMap() {
        return map;
    }

}
