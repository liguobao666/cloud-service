package com.dh.mqtt.configure;

import org.springframework.stereotype.Service;

/**
 * mqtt 订阅消息处理接口
 */
public interface MqttReceiveHandler {

    public String handlerMqttMessage(String topic,Object payLoad);
}
