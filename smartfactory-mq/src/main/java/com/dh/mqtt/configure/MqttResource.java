package com.dh.mqtt.configure;

import org.springframework.beans.factory.annotation.Value;

public class MqttResource {

    @Value("${mqtt.username}")
    public String username;

    @Value("${mqtt.password}")
    public String password;

    @Value("${mqtt.url}")
    public String url;

    @Value("${mqtt.producer.clientId}")
    public String producerClientId;

    @Value("${mqtt.producer.defaultTopic}")
    public String producerDefaultTopic;

    @Value("${mqtt.consumer.clientId}")
    public String consumerClientId;

    @Value("${mqtt.consumer.defaultTopic}")
    public  String consumerDefaultTopic;

    @Value("${mqtt.cleanSession}")
    public boolean cleanSession;

    @Value("${mqtt.defaultQos}")
    public int defaultQos;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getProducerClientId() {
        return producerClientId;
    }

    public void setProducerClientId(String producerClientId) {
        this.producerClientId = producerClientId;
    }

    public String getProducerDefaultTopic() {
        return producerDefaultTopic;
    }

    public void setProducerDefaultTopic(String producerDefaultTopic) {
        this.producerDefaultTopic = producerDefaultTopic;
    }

    public String getConsumerClientId() {
        return consumerClientId;
    }

    public void setConsumerClientId(String consumerClientId) {
        this.consumerClientId = consumerClientId;
    }

    public String getConsumerDefaultTopic() {
        return consumerDefaultTopic;
    }

    public void setConsumerDefaultTopic(String consumerDefaultTopic) {
        this.consumerDefaultTopic = consumerDefaultTopic;
    }

    public boolean isCleanSession() {
        return cleanSession;
    }

    public void setCleanSession(boolean cleanSession) {
        this.cleanSession = cleanSession;
    }

    public int getDefaultQos() {
        return defaultQos;
    }

    public void setDefaultQos(int defaultQos) {
        this.defaultQos = defaultQos;
    }
}
