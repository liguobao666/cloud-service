package com.dh.mqtt.receiveImpl;

import com.dh.mqtt.annotation.Topic;
import com.dh.mqtt.configure.MqttReceiveHandler;

/**
 * 自定义流程处理mqtt订阅，必须实现 MqttReceiveHandler接口
 * 使用 Topic注解
 */
@Topic(name = "mytopic/news",value = "receiveService")
public class ReceiveServiceImpl implements MqttReceiveHandler {

    @Override
    public String handlerMqttMessage(String topic, Object payLoad) {
        System.out.println(topic+"...."+payLoad.toString());

        return "";
    }
}
