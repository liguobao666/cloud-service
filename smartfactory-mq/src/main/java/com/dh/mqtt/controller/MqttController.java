package com.dh.mqtt.controller;


import com.dh.mqtt.service.IMqttSender;
import com.dh.mqtt.service.MqttService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class MqttController {

    @Resource
    private MqttService mqttService;

    /**
     * 注入发送MQTT的Bean
     */
    @Resource
    private IMqttSender iMqttSender;

    @RequestMapping("/send1")
    public String sendMessage(String topic, String content) {

        mqttService.send("mytopic/news", "hahahah");

        return "发送成功";
    }




    /**
     * 发送MQTT消息
     * @param message 消息内容
     * @return 返回
     */
    @RequestMapping("/send2")
    public String sendMqtt(String message) {
        iMqttSender.sendToMqtt("mytopic/news","123");
        return "发送成功";
    }

}