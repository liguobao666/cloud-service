<h1>**mosquitto 密码访问配置**</h1>

        mosquitto 配置文件位于 /etc/mosquitto/mosquitto.conf
        
        allow_anonymous 允许匿名
        
        password_file 密码文件
        
        acl_file 访问控制列表，访问控制列表（ACL）是一种基于包过滤的访问控制技术，
                它可以根据设定的条件对接口上的数据包进行过滤，允许其通过或丢弃。
        
<table>
    <tr>
        <th>allow_anonymous</th>
        <th>password_file</th>
        <th>acl_file</th>
        <th>result</th>
    </tr>
    <tr>
        <th>true</th>
        <th></th>
        <th></th>
        <th>允许匿名访问</th>
    </tr>
    <tr>
        <th>false</th>
        <th>password_file pwfile.example</th>
        <th></th>
        <th>开启用户验证机制</th>
    </tr>
    <tr>
        <th>false</th>
        <th>password_file pwfile.example</th>
        <th>acl_file acl.example</th>
        <th>开启用户验证，但是访问控制不起作用</th>
    </tr>
    <tr>
        <th>true</th>
        <th>password_file pwfile.example</th>
        <th>acl_file acl.example</th>
        <th>
            用户密码不为空，会自动进行用户验证，且受到访问控制的限制，
            用户名密码为空，将不进行用户验证，但是受到访问控制的限制
        </th>
    </tr>
    <tr>
        <th>false</th>
        <th></th>
        <th></th>
        <th>无法启动服务</th>
    </tr>
</table>

<h3>1:修改mossquitto.conf 配置文件</h3>

        allow_anonymous false
        password_file /etc/mosquitto/pwfile.example
        acl_file /etc/mosquitto/aclfile.example
        
<h3>2:向 pwfile 添加 用户</h3>
        
        ```
        #mosquitto_passwd -c /etc/mosquitto/pwfile.example admin
        Password: 
        Reenter password: 
        ​```
 
<h3>3:aclfile.example文件中配置用户可访问的topic信息</h3>

        #用户信息
        user admin
        #用户可访问的topic,这里为可读可写
        topic mytopic/news
        #订阅用户可访问权限
        user sub
        #这里为只读
        topic read mytopic/news
<h3>4:重启mosquitto</h3>

        # mosquitto -c -d /etc/mosquitto/mosquitto.conf -d
        
<h3>5:订阅客户端启动</h3>
        
        #mosquitto_sub -h localhost  -t mytopic/news  -u admin -P 554288
        
<h3>6:发布消息</h3>
       
       #mosquitto_pub -h localhost -t  mytopic/news -u admin -P 554288 -m "test"
        


