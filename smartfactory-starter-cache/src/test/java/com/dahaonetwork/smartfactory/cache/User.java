package com.dahaonetwork.smartfactory.cache;

import java.io.Serializable;
//@JsonSerialize
//@JsonNaming(PropertyNamingStrategy.LowerCaseWithUnderscoresStrategy.class)
public class User implements Serializable{
	
	private static final long serialVersionUID = -1L;
	private String id;
	private String username;
	private Integer age;
	private String ids;

	/**
	 * 默认构造方法
	 */
	public User() {

	}
	
	/**
	 * 全属性构造方法
	 * 
	 * @param id
	 * @param username
	 * @param age
	 */
	public User(String id, String username, Integer age) {
		this.id=id;
		this.username = username;
		this.age = age;
	}
	
	public User(String username, Integer age) {
		this.username = username;
		this.age = age;
	}

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}
	
	
	

}
