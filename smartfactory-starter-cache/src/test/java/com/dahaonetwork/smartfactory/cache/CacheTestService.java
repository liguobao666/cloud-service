package com.dahaonetwork.smartfactory.cache;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Service;

/**
 * 缓存测试
 * @author liguobao
 *
 */
//开启缓存  
@EnableCaching 
@Service("cacheTestService")
public class CacheTestService {
	
	Log log = LogFactory.getLog(CacheTestService.class);
	
	/**
	 * @Cacheable:在方法执行前Spring先查看缓存中是否有数据，如果有数据，则直接返回缓存数据；
	 * 			     若没有数据，调用方法，并且将返回值放进缓存
	 * @CachePut:无论怎样，都会将方法的返回值放到缓存中 @CachePut的属性与@Cacheable保持一致
	 * @CacheEvict:将一条或者多条数据从缓存中删除
	 * @Caching:可以通过@Caching 注解组合多个注解策略在一个方法上
	 */
	 /**
	  * 键的生成策略:
		       键的生成策略有两种，一种是默认策略，一种是自定义策略。
		1.默认策略
		       默认的key生成策略是通过KeyGenerator生成的，其默认策略如下：
			1.1.如果方法没有参数，则使用0作为key。
			1.2.如果只有一个参数的话则使用该参数作为key。
			1.3.如果参数多余一个的话则使用所有参数的hashCode作为key
		2.自定义策略keyGenerator

	  */
	@Cacheable(value = "usercache",key = "#id")  
    public User findUser(String id,String username,Integer age){  
        log.info("无缓存的时候调用这里");
        return new User(id,username,age);  
    }  
	
	 /*key属性是用来指定Spring缓存方法的返回结果时对应的key的。
	  * 该属性支持SpringEL表达式。当我们没有指定该属性时，
	  * Spring将使用默认策略生成key。我们这里先来看看自定义策略，至于默认策略会在后文单独介绍。
	  * 自定义策略是指我们可以通过Spring的EL表达式来指定我们的key。
	  * 这里的EL表达式可以使用方法参数及它们对应的属性。
	  * 使用方法参数时我们可以直接使用“#参数名”或者“#p参数index”。
	  * 下面是几个使用参数作为key的示例。
	  */
	 @Cacheable(value="users", key="#id")
	 public User find1(String id) {
	    return new User(id,"张三",123);  
	 }

	 @Cacheable(value="users", key="#p0")
	 public User find2(String id) {
		 log.info("无缓存的时候调用这里");
		 return new User(id,"张三",123); 
	 }

	 @Cacheable(value="users", key="#user.ids")
	 public User find3(User user) {
		log.info("无缓存的时候调用这里");
	    return user;
	 }

	 @Cacheable(value="users", key="#p0.id")
	 public User find4(User user) {
	    return user;
	 }
	 
	 /*condition属性指定发生的条件
                有的时候我们可能并不希望缓存一个方法所有的返回结果。
                通过condition属性可以实现这一功能。condition属性默认为空，
                表示将缓存所有的调用情形。其值是通过SpringEL表达式来指定的，当为true时表示进行缓存处理；
                当为false时表示不进行缓存处理，即每次调用该方法时该方法都会执行一次。
                如下示例表示只有当user的id为偶数时才会进行缓存。
	  */
	 @Cacheable(value={"users"}, key="#user.id", condition="#user.id%2==0")
	 public User find(User user) {
	    System.out.println("find user by user " + user);
	    return user;
	 }
	 
	 /**
	  *  ⊙ value ： 必须属性。用于指定该方法用于清除哪个缓存区的数据。

　　　　　   ⊙ allEntries ： 该属性指定是否清空整个缓存区。

　　　　　　⊙ beforeInvocation ： 该属性指定是否在执行方法之前清除缓存。默认是在方法成功完成之后才清除缓存。

　　　　　　⊙ condition ： 该属性指定一个SpEL表达式，只有当该表达式为true时才清除缓存。

　　　　　　⊙ key ： 通过SpEL表达式显示指定缓存的key。多个参数组合的key 用#name + #age + ...
	  * @param id
	  */
	 @CacheEvict(value="usercache",key="#id",allEntries=true)
	 public void delete(String id) {
		 System.out.println("删除缓存");
	 }

}
