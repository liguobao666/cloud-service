package com.dahaonetwork.smartfactory.cache;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dahaonetwork.smartfactory.cache.constant.PublisherService;

/**
 * 发布订阅，功能
 * @author liguobao
 *
 */

@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=CacheApplication.class)
public class Redispublish {
	
	@Autowired
	private PublisherService publisherService;

	@Test
	public void test() throws Exception {
		// 保存对象
		publisherService.sendMessage("Hello from Redis!");
		publisherService.sendMessage("1231231");
		publisherService.sendMessage("222");
		publisherService.sendMessage("323");
		publisherService.sendMessage("423231");
		publisherService.sendMessage("5123");
		publisherService.sendMessage("6asd");
		publisherService.sendMessage("71231");
	}

}
