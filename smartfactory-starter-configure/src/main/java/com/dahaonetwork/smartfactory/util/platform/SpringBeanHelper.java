package com.dahaonetwork.smartfactory.util.platform;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 获取spring Bean帮助类
 * @author liguobao
 *
 */
public class SpringBeanHelper implements ApplicationContextAware{
	
	/** 持有的spring上下文 */
	private static ApplicationContext applicationContext = null;

	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		// TODO Auto-generated method stub
		if (SpringBeanHelper.applicationContext == null) {
			SpringBeanHelper.applicationContext = arg0;
		}
		
	}
	
	// 获取applicationContext
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}
	
	/**
	 * 
	 * 根据Bean名称得到Bean
	 * 
	 * @param clazz
	 *            bean的类型
	 * @param beanName
	 *            bean名称
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getBean(Class<T> clazz, String beanName) {
		return (T) applicationContext.getBean(beanName);
	}
    
	//通过name获取 Bean.
    public static Object getBean(String name){
        return getApplicationContext().getBean(name);
    }

}
