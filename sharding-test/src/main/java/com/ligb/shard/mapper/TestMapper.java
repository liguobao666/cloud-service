package com.ligb.shard.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TestMapper {

    @Insert("insert into t_test (id,name) values (#{id},#{name})")
    void insert(@Param("id")Integer id,@Param("name") String name);
}
