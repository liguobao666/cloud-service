package com.ligb.shard.mapper;

import com.ligb.shard.model.OrderModel;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderMapper {

    @Insert("insert into t_order (name,user_id) values (#{name},#{userId})")
    void insert(@Param("name") String name,@Param("userId")Integer userId);

    @Select("select order_id orderId,name from t_order")
    List<OrderModel> select();
}
