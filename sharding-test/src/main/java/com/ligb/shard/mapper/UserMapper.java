package com.ligb.shard.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper {

    @Insert("insert into t_user (name) values (#{name})")
    void insert(@Param("name") String name);

    @Select("select name from t_user")
    List<String> select();

}
