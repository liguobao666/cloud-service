package com.ligb.shard.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface DictMaper {

    @Insert("insert into t_dict (name) values (#{name})")
    void insert(@Param("name") String name);

    @Select("select name from t_dict where id=#{id}")
    String select(Integer id);
}
