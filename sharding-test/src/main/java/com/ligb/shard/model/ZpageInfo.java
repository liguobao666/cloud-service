
package com.ligb.shard.model;

import com.github.pagehelper.Page;

import java.io.Serializable;
import java.util.List;

/**
 * 重写 PageInfo，自定义数据
 * @author Zoser
 */
public class ZpageInfo<T> implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//总记录数
    protected long total;
    //结果集
    protected List<T> list;
    //总页数
    protected int pageCount;

    public ZpageInfo() {
    }

    public ZpageInfo(List<T> list) {
        this.list = list;
        if(list instanceof Page){
        	Page<T> page = (Page<T>) list;
            this.total = page.getTotal();
        	this.pageCount = page.getPages();
        } else {
            this.total = list.size();
        }
    }

    public static <T> ZpageInfo<T> of(List<T> list){
        return new ZpageInfo<T>(list);
    }

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	@Override
    public String toString() {
        return "PageSerializable{" +
                "total=" + total +
                ", data=" + list +
                ", pageCount=" + pageCount +
                '}';
    }
}
