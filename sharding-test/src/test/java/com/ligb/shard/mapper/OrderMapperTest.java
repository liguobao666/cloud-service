package com.ligb.shard.mapper;

import com.github.pagehelper.PageHelper;
import com.ligb.shard.AccountApplication;
import com.ligb.shard.model.OrderModel;
import com.ligb.shard.model.ZpageInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,classes= AccountApplication.class)
@AutoConfigureMockMvc
//@Transactional
public class OrderMapperTest {
    @Autowired
    OrderMapper orderMapper;

    @Test
    public void insert(){
        for(int i=1;i<20;i++){
            orderMapper.insert("i",2);
        }
    }

    @Test
    public void select(){
        List<OrderModel> list=orderMapper.select();
        list.forEach(model->{
            System.out.println("=========="+model.getName()+":"+model.getOrderId());
        });
    }

    @Test
    public void selectPage(){
        PageHelper.startPage(1, 10);
        List<OrderModel> list=orderMapper.select();
        ZpageInfo<OrderModel> result = new ZpageInfo<>(list);
        System.out.println(result);
    }

}
