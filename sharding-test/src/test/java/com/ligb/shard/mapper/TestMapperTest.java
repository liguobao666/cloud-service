package com.ligb.shard.mapper;

import com.ligb.shard.AccountApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,classes= AccountApplication.class)
@AutoConfigureMockMvc
public class TestMapperTest {

    @Autowired
    TestMapper testMapper;

    @Test
    public void insert(){
        for(int i=1;i<20;i++){
            testMapper.insert(i,"test");
        }
    }
}
