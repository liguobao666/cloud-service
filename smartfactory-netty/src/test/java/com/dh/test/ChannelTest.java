package com.dh.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.util.Set;

import org.junit.Test;
/**
 * http://www.iteye.com/magazines/132-Java-NIO
 * @author liguobao
 * 
 * Channel的实现 
	这些是Java NIO中最重要的通道的实现： 
		FileChannel：从文件中读写数据。
		DatagramChannel：能通过UDP读写网络中的数据。
		SocketChannel：能通过TCP读写网络中的数据。
		ServerSocketChannel：可以监听新进来的TCP连接，像Web服务器那样。对每一个新进来的连接都会创建一个SocketChannel
 *
 */
public class ChannelTest {
	@Test
	public void channel1() {
		try {
			RandomAccessFile aFile = new RandomAccessFile("D:/nio-data.txt", "rw");
			FileChannel inChannel = aFile.getChannel();
			
			Selector selector = Selector.open();  
            
			/**
			 * 缓冲区本质上是一块可以写入数据，然后可以从中读取数据的内存。
			 * 这块内存被包装成NIO Buffer对象，并提供了一组方法，用来方便的访问该块内存。 
			 *	为了理解Buffer的工作原理，需要熟悉它的三个属性： 
			 *	capacity
			 *	position
			 *	limit
			 *
			 *capacity 
				作为一个内存块，Buffer有一个固定的大小值，也叫“capacity”.
				你只能往里写capacity个byte、long，char等类型。一旦Buffer满了，
				需要将其清空（通过读数据或者清除数据）才能继续写数据往里写数据。 
			  position 
				当你写数据到Buffer中时，position表示当前的位置。初始的position值为0.当一个byte、long等数据写到Buffer后， 
				position会向前移动到下一个可插入数据的Buffer单元。position最大可为capacity – 1。 
				当读取数据时，也是从某个特定位置读。当将Buffer从写模式切换到读模式，position会被重置为0。
				当从Buffer的position处读取数据时，position向前移动到下一个可读的位置。 
			  limit 
				在写模式下，Buffer的limit表示你最多能往Buffer里写多少数据。
				 写模式下，limit等于Buffer的capacity。 
				当切换Buffer到读模式时， limit表示你最多能读到多少数据。
				因此，当切换Buffer到读模式时，limit会被设置成写模式下的position值。
				换句话说，你能读到之前写入的所有数据（limit被设置成已写数据的数量，这个值在写模式下就是position） 
				
			   使用Buffer读写数据一般遵循以下四个步骤： 
				写入数据到Buffer
				调用flip()方法
				从Buffer中读取数据
				调用clear()方法或者compact()方法
			 */
			//Buffer的分配 
			ByteBuffer buf = ByteBuffer.allocate(48);
			
			//从Channel写到Buffer的例子  buf.put(b)
			int bytesRead = inChannel.read(buf);//read into buffer.
			
			//read from buffer into channel.  
			//int bytesWritten = inChannel.write(buf); 
			while (bytesRead != -1) {
				System.out.println("Read " + bytesRead);
				/**
				 * flip方法将Buffer从写模式切换到读模式。调用flip()方法会将position设回0，并将limit设置成之前position的值。 
					换句话说，position现在用于标记读的位置，limit表示之前写进了多少个byte、char等 —— 现在能读取多少个byte、char等。 
				 */
				buf.flip();
				//当前buf是否有任何元素
				while (buf.hasRemaining()) {
					System.out.print((char) buf.get());//从buf中读取数据
				}
				//一旦读完Buffer中的数据，需要让Buffer准备好再次被写入。可以通过clear()或compact()方法来完成。 
				/**
				 * 如果调用的是clear()方法，position将被设回0，limit被设置成 capacity的值。
				 * 换句话说，Buffer 被清空了,Buffer中的数据并未清除，
				 * 只是这些标记告诉我们可以从哪里开始往Buffer里写数据。 
				 */
				buf.clear();
				bytesRead = inChannel.read(buf);
			}
			aFile.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void selectorTest() {
		/*Selector selector = Selector.open();  
		channel.configureBlocking(false);  
		SelectionKey key = channel.register(selector, SelectionKey.OP_READ);  
		while(true) {  
		  int readyChannels = selector.select();  
		  if(readyChannels == 0) continue;  
		  Set selectedKeys = selector.selectedKeys();  
		  Iterator<E> keyIterator = selectedKeys.iterator();  
		  while(keyIterator.hasNext()) {  
		    SelectionKey key1 = keyIterator.next();  
		    if(key1.isAcceptable()) {  
		        // a connection was accepted by a ServerSocketChannel.  
		    } else if (key1.isConnectable()) {  
		        // a connection was established with a remote server.  
		    } else if (key1.isReadable()) {  
		        // a channel is ready for reading  
		    } else if (key1.isWritable()) {  
		        // a channel is ready for writing  
		    }  
		    keyIterator.remove();  
		  }  
		}*/
	}

}
