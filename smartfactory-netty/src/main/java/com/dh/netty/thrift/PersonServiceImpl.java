package com.dh.netty.thrift;

import com.dh.netty.thrift.generated.DateException;
import com.dh.netty.thrift.generated.Person;
import com.dh.netty.thrift.generated.PersonService;
import org.apache.thrift.TException;

public class PersonServiceImpl implements PersonService.Iface{

    @Override
    public Person getPersonByUsername(String username) throws DateException, TException {
        Person person=new Person();
        person.setAge(12).setMarried(true).setUsername("zhangsan");
        System.out.println("getPersonByUsername");
        return  person;
    }

    @Override
    public void savePerson(Person person) throws DateException, TException {
        System.out.println("保存person");
    }
}
