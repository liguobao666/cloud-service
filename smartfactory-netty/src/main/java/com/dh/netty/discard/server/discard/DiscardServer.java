package com.dh.netty.discard.server.discard;

import io.netty.bootstrap.ServerBootstrap;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class DiscardServer {

	 private int port;
	    
    public DiscardServer(int port) {
        this.port = port;
    }
    
    public void run() throws Exception {
        /**
            1.NioEventLoopGroup 是用来处理I/O操作的多线程事件循环器，Netty 提供了许多不同的
            EventLoopGroup 的实现用来处理不同的传输
         */
        //boss用来接收进来的连接。
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        //一旦‘boss’接收到连接，就会把连接信息注册到‘worker’上。
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            //是一个启动 NIO 服务的辅助启动类。你可以在这个服务中直接使用
            //Channel，但是这会是一个复杂的处理过程，在很多情况下你并不需要这样做。
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    //这里我们指定使用 NioServerSocketChannel 类来举例说明一个新的 Channel 如何接收进来的连接。
             .channel(NioServerSocketChannel.class)
                    //这里的事件处理类经常会被用来处理一个最近的已经接收的 Channel
                    //ChannelInitializer 是一个特殊的处理类，
                    //他的目的是帮助使用者配置一个新的 Channel
             .childHandler(new ChannelInitializer<SocketChannel>() {
                 @Override
                 public void initChannel(SocketChannel ch) throws Exception {
                     ch.pipeline().addLast(new DiscardServerHandler());
                 }
             })
                    //设置指定的 Channel 实现的配置参数
                    //option() 是提供给NioServerSocketChannel 用来接收进来的连接
             .option(ChannelOption.SO_BACKLOG, 128)
                    //childOption() 是提供给由父管道 ServerChannel
                    // 接收到的连接，在这个例子中也是 NioServerSocketChannel
             .childOption(ChannelOption.SO_KEEPALIVE, true);
    
            //绑定端口
            ChannelFuture f = b.bind(port).sync();

            f.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }
    
    public static void main(String[] args) throws Exception {
        int port;
        if (args.length > 0) {
            port = Integer.parseInt(args[0]);
        } else {
            port = 8080;
        }
        new DiscardServer(port).run();
    }

}
