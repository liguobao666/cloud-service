package com.dh.netty.discard.server.time;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class TimeServerHandler  extends ChannelInboundHandlerAdapter {

    /**
     * 连接建立时被调用
     * @param ctx
     */
    @Override
    public void channelActive(final ChannelHandlerContext ctx) {
        //创建4子节点ByteBuf
        final ByteBuf time = ctx.alloc().buffer(4);

        time.writeInt((int) (System.currentTimeMillis() / 1000L + 2208988800L));
        /**
         * 1：我们使用 NIO 发
         * 送消息时不是调用 java.nio.ByteBuffer.flip() 吗？
         * ByteBuf 之所以没有这个方法因为有两个指针，一个对应读操作一个对应写操作。
         * 当你向 ByteBuf 里写入数据的时候写指针的索引就会增加，同时读指针的索引没有变化。
         * 读指针索引和写指针索引分别代表了消息的开始和结束。
         *
         * 2：比较起来，NIO 缓冲并没有提供一种简洁的方式来计算出消息内容的开始和结尾，除非你调
         * 用 flip 方法。当你忘记调用 flip 方法而引起没有数据或者错误数据被发送时，你会陷入困境。
         * 这样的一个错误不会发生在 Netty 上，因为我们对于不同的操作类型有不同的指针。
         *
         * 一个 ChannelFuture 代表了一个还没有发生的 I/O 操作。这意味着
         * 任何一个请求操作都不会马上被执行，因为在 Netty 里所有的操作都是异步的
         */
        final ChannelFuture f = ctx.writeAndFlush(time);
        /**
         * 当一个写请求已经完成是如何通知到我们？这个只需要简单地在返回的 ChannelFuture 上
         * 增加一个ChannelFutureListener。这里我们构建了一个匿名的 ChannelFutureListener 类用来
         * 在操作完成时关闭 Channel
         */
        f.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) {
                assert f == future;
                ctx.close();
            }
        });
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}
