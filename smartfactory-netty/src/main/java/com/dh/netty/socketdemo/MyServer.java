package com.dh.netty.socketdemo;

import com.dh.netty.protobuf.MyServerInitializer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class MyServer {
    public static void  main(String args[]) throws Exception{
        //接收客户端的连接，在事件循环当中，在进行selector的时候，用于注册连接
        EventLoopGroup boss=new NioEventLoopGroup();
        //真正完成处理
        EventLoopGroup worker=new NioEventLoopGroup();
        try{
            ServerBootstrap serverBootstrap=new ServerBootstrap();
            //成员变量的赋值
            serverBootstrap.group(boss,worker).channel(NioServerSocketChannel.class)
                    .childHandler(new MyServerInitializer());

            ChannelFuture channelFuture=serverBootstrap.bind(8899).sync();
            channelFuture.channel().closeFuture().sync();
        }finally {
            boss.shutdownGracefully();
            worker.shutdownGracefully();
        }

    }
}
