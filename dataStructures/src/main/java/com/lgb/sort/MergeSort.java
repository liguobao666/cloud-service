package com.lgb.sort;

import java.util.Arrays;

/**
 * 归并排序
 */
public class MergeSort {
    public static void main(String[] args) {
        int arr[]={8,4,5,7,1,3,6,2};
        int temp[]=new int[arr.length];
        mergerSort(arr,0,arr.length-1,temp);

        System.out.println(Arrays.toString(arr));
    }


    public static void mergerSort(int arr[],int left,int right,int temp[]){
        if (left<right){
            int mid =(left+right)/2;
            //向左拆分
            mergerSort(arr,left,mid,temp);
            //向右拆分
            mergerSort(arr,mid+1,right,temp);
            merger(arr,left,mid,right,temp);
        }
    }

    /**
     * 合并方法
     * 1 2 3 4 5    0+4/2=2
     * 1 2 3 4 5 6  0+5/2=2
     * @param arr 原始数组
     * @param left 左边有序序列的初始索引，
     * @param mid 中间索引
     * @param right 右边索引
     * @param temp  中转的数组
     */
    public static void merger(int arr[],int left,int mid,int right,int[] temp){
        int i=left;//左边初始索引
        int j=mid+1;//右边的初始索引
        int t=0;//指向 temp 数组的当前位置
        //(1)先把左右俩边的数据按照规则拷贝到temp数组，直到左右俩边的有序序列有一边处理完毕为止
        while (i<=mid&&j<=right){
            //如果左边的 小于右边的当前元素
            if (arr[i]<=arr[j]){
                temp[t]=arr[i];
                t++;
                i++;
            }else {//反之，将右边有序序列的
                temp[t]=arr[j];
                t++;
                j++;
            }
        }
        //(2)把有剩余的一方填充到temp
        while (i<=mid){
            temp[t]=arr[i];
            t++;
            i++;
        }
        while (j<=right){
            temp[t]=arr[j];
            t++;
            j++;
        }
        //(3)将temp数组拷贝到arr,并不是每次都拷贝八个
        t=0;
        int templeft=left;
        while (templeft<=right){
            arr[templeft]=temp[t];
            t++;
            templeft++;
        }
    }
}
