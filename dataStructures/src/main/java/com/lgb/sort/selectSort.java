package com.lgb.sort;

/**
 * 选择排序
 *  第一次arr[0]~arr[n-1],选取最小的与arr[0]先交换。
 *  第二次arr[1]~arr[n-1],选择的最小的与arr[1]交换
 *
 *  说明
 *     1 选择排序一共有数组大小-1轮排序
 *     2 每一轮排序，又是一个循环
 *       2.1先假定当前数是这个数的最小数
 *       2.2然后和后面的每个数进行比较，如果发现比当前数更小的数，就重新确定最小数，并得到下标
 *       2.3当遍历到最后的时候，就得到最小数的下标
 *       2.4交换
 */
public class selectSort {

    public static void main(String[] args) {
        int arr[]={3,9,-1,10,20};
        selectSort(arr);

        for (int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
        }
    }
    public static int[] selectSort(int arr[]){

        for (int i=0;i<arr.length-1;i++){
            int minIndex=i;
            int min =arr[i];
            for(int j=i+1;j<arr.length;j++){
                if (min>arr[j]){
                    minIndex=j;
                    min=arr[j];
                }
            }
            //交换一下
            arr[minIndex]=arr[i];
            arr[i]=min;
        }
        return arr;
    }
}
