package com.lgb.sort;

import java.util.Arrays;

/**
 * 快速排序对冒泡的改进：
 * 先分隔成俩部分，小的放到左边，大的放到右边。按照此方法，按照此方法进行递归排序。
 */
public class QuickSort {
    public static void main(String[] args) {
        int arr[]={-9,78,0,23,-567,70};
        quickSort(arr,0,arr.length-1);
        System.out.println(Arrays.toString(arr));

    }
    public static void quickSort(int arr[],int left,int right){
        int l=left;//左下标
        int r=right;//右下标
        int pivot=arr[(left+right)/2];
        int temp=0;
        //比pivot小的放到左边，比pivot大的放到右边
        while (l<r){
            //找打大于等于pivot的值就退出循环，没找到
            while (arr[l]<pivot){
                l++;
            }
            //找到比pivot小的则退出循环
            while (arr[r]>pivot){
                r--;
            }
            //l>=R说明 pivot左边全部是小于pivot，右边全部大于pivot
            if (l>=r){
                break;
            }
            //交换
            temp=arr[l];
            arr[l]=arr[r];
            arr[r]=temp;
            //如果交换完后arr[l]==pivot,r--,防止死循环
            if (arr[l]==pivot){
                r--;
            }
            if (arr[r]==pivot){
                l++;
            }
            System.out.println(Arrays.toString(arr));
        }
        if (l==r){
            l++;
            r--;
        }
        //左递归
        if (left<r){
            quickSort(arr,left,r);
        }
        if (right>l){
            quickSort(arr,l,right);
        }

    }
}
