package com.lgb.sort;

import java.util.Arrays;

/**
 * 插入排序：
 * 把n个元素看成为一个有序表和一个无序表，开始时有序表中只包含一个元素
 * ，无序表中有n-1个元素，排序过程中每次从无序表中取出第一个元素，与有序表比较，
 * 插入适当位置
 */
public class InsertSort {
    public static void main(String[] args) {
        int[] arr={101,34,119,1};
        insertSort(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static void insertSort(int arr[]){
        for(int i=1;i<arr.length;i++){
            int insertval=arr[i];//要插入的值从，1开始
            int insertIndex=i-1;//要插入的位置
            while (insertIndex>=0&&insertval<arr[insertIndex]){
                arr[insertIndex+1]=arr[insertIndex];
                insertIndex--;
            }
            arr[insertIndex+1]=insertval;
        }
    }
}
