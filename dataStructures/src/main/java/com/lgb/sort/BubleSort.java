package com.lgb.sort;

public class BubleSort {
    public static void main(String[] args) {
        int arr[]={3,9,-1,10,20};
        sort(arr);

        for (int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
        }
    }

    /***
     * 俩俩比较进行交换，然后将各自指针向后移动一位
     *  3 9 -1 10 20
     * 第一趟 将最大的排在最后
     *              3 9 -1 10 20
     *              3 -1 9 10 20
     *              3 -1 9 10 20
     *              3 -1 9 10 20
     *
     * 第二趟 将第二大排在倒数第二
     *              -1 3 9 10 20
     *              -1 3 9 10 20
     *              -1 3 9 10 20
     * 第三趟
     *               -1 3 9 10 20
     *               -1 3 9 10 20
     * 第四趟
     *               -1 3 9 10 20
     *
     *  一共进行数组大小减一次大循环
     *  每一趟排序次数逐渐减少
     * @param arr
     * @return
     */
    public static int[] sort(int arr[]){
        for (int i=0;i<arr.length-1;i++){
            int temp=0;
            for (int j=0;j<arr.length-1-i;j++){
                if (arr[j]>arr[j+1]){
                    temp=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=temp;
                }
            }
        }
        return arr;

    }

}
