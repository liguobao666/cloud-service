package com.lgb.sort;

import java.util.Arrays;

/**
 * 基数排序，有十个桶，将个位数取出，放到相应桶，排序
 *                     将十位数取出，放到相应桶，排序
 *
 */
public class RadixSort {
    public static void main(String[] args) {
            int[] arr={53,3,542,748,14,214};
            radixSort(arr);
    }

    public static void radixSort(int[] arr){
        int max=arr[0];
        for (int i=0;i<arr.length;i++){
            if (arr[i]>max){
                max=arr[i];
            }
        }
        //得到最大的数的位数
        int maxlength=(max+"").length();

        //定义一个二维数组标识十个桶
        int[][] bucket=new int[10][arr.length];

        //为了记录每个桶实际存放的数据，定义一个数组，记录每个桶的数据个数
        int[] bucketElementCounts=new int[10];


       for (int i=0,n=1;i<maxlength;i++,n*=10){
            for(int j=0;j<arr.length;j++){
                //取出每个元素的个位数
                int digitOfElement=arr[j] /n %10;
                /***
                 * 如果第一个个为数为0 bucketElementCounts[0]=0
                 *      bucketElementCounts[0]=bucketElementCounts[0]+1//存放了一个元素
                 *  第二个数个位数为0，此时bucketElementCounts[0]=1，
                 **/
                bucket[digitOfElement][bucketElementCounts[digitOfElement]]=arr[j];
                bucketElementCounts[digitOfElement]=bucketElementCounts[digitOfElement]+1;
            }
            int index=0;
            //遍历每一个桶，将桶中的数据放入原数组
            for (int k=0;k<bucketElementCounts.length;k++){
                //如果桶中有数据才放入原数组
                if (bucketElementCounts[k]!=0){
                    //循环第k个桶,一共有bucketElementCounts[k]个元素
                    for (int l=0;l<bucketElementCounts[k];l++){
                        //取出元素，放入arr中
                        arr[index]=bucket[k][l];
                        index++;
                    }
                }
                bucketElementCounts[k]=0;
            }
            System.out.println(Arrays.toString(arr));
        }



       /*for(int j=0;j<arr.length;j++){
            //取出每个元素的个位数
            int digitOfElement=arr[j]%10;
            *//***
             * 如果第一个个为数为0 bucketElementCounts[0]=0
             *      bucketElementCounts[0]=bucketElementCounts[0]+1//存放了一个元素
             *  第二个数个位数为0，此时bucketElementCounts[0]=1，
             **//*
            bucket[digitOfElement][bucketElementCounts[digitOfElement]]=arr[j];
            bucketElementCounts[digitOfElement]=bucketElementCounts[digitOfElement]+1;
        }
        int index=0;
        //遍历每一个桶，将桶中的数据放入原数组
        for (int k=0;k<bucketElementCounts.length;k++){
            //如果桶中有数据才放入原数组
            if (bucketElementCounts[k]!=0){
                //循环第k个桶,一共有bucketElementCounts[k]个元素
                for (int l=0;l<bucketElementCounts[k];l++){
                    //取出元素，放入arr中
                    arr[index]=bucket[k][l];
                    index++;
                }
            }
            //bucketElementCounts[k]
            bucketElementCounts[k]=0;
        }
        System.out.println(Arrays.toString(arr));*/

    }

}
