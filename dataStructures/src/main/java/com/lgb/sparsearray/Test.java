package com.lgb.sparsearray;


public class Test {
    private static Object a=new Object();
    private static Object b=new Object();
    public static void main(String[] args) {
        new Thread(()->{
            synchronized (a){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (b){
                    System.out.println("12322");
                }
            }
        }).start();


        new Thread(()->{
            synchronized (b){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (a){
                    System.out.println("123");
                }
            }
        }).start();

    }


}
