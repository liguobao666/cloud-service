package com.lgb.sparsearray;

/**
 * 将一个字符串转换为二进制
 */
public class Solution {
    public static void main(String[] args) {
        StringBuilder str=binaryToDecimal(-123);
        System.out.println(str);
        System.out.println(solution(new int[]{1,2,3,4,1,2,3,4,5}));
    }

    public static StringBuilder binaryToDecimal(int n) {
        StringBuilder str=new StringBuilder();
        if (n>7){
            for (int i = 31; i >= 0; i--) {
                str.append(n >>> i & 1);
            }
        }
        return str;
    }

    public static int solution (int[] nums) {
        int res = 0;
        for (int value: nums) {
            res=res ^ value;
        }
        return res;
    }

}
