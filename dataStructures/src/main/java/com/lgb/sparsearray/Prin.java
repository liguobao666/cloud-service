package com.lgb.sparsearray;

public class Prin {
    public static void main(String[] args) {
      Thread t1=new Thread(new Print(0,"A"));
      Thread t2=new Thread(new Print(1,"B"));
      Thread t3=new Thread(new Print(2,"C"));
      t1.start();
      t2.start();
      t3.start();
    }
}

class Print implements Runnable{
    private static Object lock=new Object();
    //当前打印的值
    private int flag;     //表示第几个线程
    private String value; //

    public Print(int flag, String value) {
        this.flag = flag;
        this.value = value;
    }

    private static int count=0;

    @Override
    public void run() {
        while (true){
            synchronized (lock){
                while (count%3 != flag){
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(value+":"+Thread.currentThread().getName());
                count++;
                lock.notifyAll();
            }
        }
    }
}
