package com.lgb.sparsearray;

public class SparseArray {
    public static void main(String[] args) {
        //构建原始的二维数组
        int[][] chessArr1=new int[11][11];
        chessArr1[1][2]=1;//黑子
        chessArr1[2][3]=2;//白子
        //输出二维数组
        for(int[] row:chessArr1){
            for(int data:row){
                System.out.printf("%d\t",data);
            }
            System.out.println();
        }
        System.out.println("=================================");
        //将二维数组转换成稀疏数组
        int sum=0;
        for(int i=0;i<chessArr1.length;i++){
            for(int j=0;j<chessArr1[0].length;j++){
                if(chessArr1[i][j]!=0){
                    sum++;
                }
            }
        }
        System.out.println("二维数组的数据个数 sum="+sum);
        //创建稀疏数组
        int[][] sparseArray=new int[sum+1][3];

        //给稀疏数组赋值
        sparseArray[0][0]=chessArr1.length;
        sparseArray[0][1]=chessArr1[0].length;
        sparseArray[0][2]=sum;

        int count =0;
        for(int i=0;i<chessArr1.length;i++){
            for(int j=0;j<chessArr1[0].length;j++){
                if(chessArr1[i][j]!=0){
                    count++;
                    sparseArray[count][0]=i;
                    sparseArray[count][1]=j;
                    sparseArray[count][2]=chessArr1[i][j];
                }
            }
        }

        //稀疏数组输出
        for(int i=0;i<sparseArray.length;i++){
            System.out.printf("%d\t%d\t%d\t\n",sparseArray[i][0],sparseArray[i][1],sparseArray[i][2]);

        }
        /**
         * 将稀疏数组恢复成二维数组
         */
        int row=sparseArray[0][0];
        int col=sparseArray[0][1];
        int chessArr2[][]=new int[row][col];

        for(int i=1;i<sparseArray.length;i++){
            chessArr2[sparseArray[i][0]][sparseArray[i][1]]=sparseArray[i][2];
        }

        for(int[] row1:chessArr2){
            for(int data:row1){
                System.out.printf("%d\t",data);
            }
            System.out.println();
        }


    }
}
