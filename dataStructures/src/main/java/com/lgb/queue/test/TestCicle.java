package com.lgb.queue.test;


/**
 * 循环队列
 */
public class TestCicle {
    private int front;//指向队列头
    private int real;//指向队列最后一个位置的下一个位置
    private int maxSize;//队列的容量
    private int arr[];


    public TestCicle(int maxSize) {
        this.maxSize=maxSize;
        arr=new int[maxSize];
    }

    /**
     * 判断对列是否已满
     * @return
     */
    public boolean isFull(){
        return (real+1)%maxSize==front;
    }

    /**
     * 队列为空
     */
    public boolean isEmpty(){
        if (real==front){
            return true;
        }
        return false;
    }

    /**
     * 入队
     * @param val
     */
    public void add(int val){
        if(isFull()){
            System.out.println("队列已满");
            return;
        }
        arr[real]=val;
        real=(real+1)%maxSize;
    }

    /**
     * 出队
     * @return
     */
    public int get(){
        if(isEmpty()){
            throw new RuntimeException("队列为空");
        }
        int val=arr[front];
        front=(front+1)%maxSize;
        return val;
    }

    /**
     * 展示列表
     */
    public void show(){
        if(isEmpty()){
            System.out.println("队列为空");
        }
        for(int i=front;i<front+size();i++){
            System.out.println("队列中的元素:"+arr[i%maxSize]);
        }
    }
    public int size(){
        return (real+maxSize-front)%maxSize;
    }

    public static void main(String[] args) {
        TestCicle test=new TestCicle(4);
        test.add(1);
        test.add(2);
        test.add(3);
        test.add(4);
        test.show();
        System.out.println(test.get());
        System.out.println("=====================");
        test.show();
        System.out.println(test.get());
        System.out.println(test.get());
        test.show();
    }
}
