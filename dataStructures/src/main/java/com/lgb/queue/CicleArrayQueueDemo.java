package com.lgb.queue;

import java.util.Scanner;

/**
 * 循环队列
 */
public class CicleArrayQueueDemo {
    public static void main(String[] args) {
        CicleArray arrayQueue=new CicleArray(3);
        char key=' ';
        Scanner scanner=new Scanner(System.in);

        boolean loop=true;

        while (loop){
            System.out.println("s:显示队列");
            System.out.println("e:退出队列");
            System.out.println("a:添加队列");
            System.out.println("g:取数据");
            System.out.println("h:对头数据");
            key=scanner.next().charAt(0);
            switch (key){
                case 's':
                    arrayQueue.showQueue();
                    break;
                case 'a':
                    System.out.println("入队");
                    int val=scanner.nextInt();
                    arrayQueue.addQueue(val);
                    break;
                case 'g':
                    try {
                        int res=arrayQueue.getQueue();
                        System.out.println("取出的数据："+res);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                case 'h':
                    int res=arrayQueue.headQueue();
                    System.out.println(res);
                    break;
                case 'e':
                    scanner.close();
                    loop=false;
                    break;
            }

        }


    }
}
class CicleArray{
    private int maxSize;//队列的容量

    private int front;//指向队列的第一个位置

    private int real;//队列的最后一个位置的下一个位置，空出一个空间作为约定

    private int[] arr;//队列数组

    public CicleArray(int maxSize) {
        this.maxSize = maxSize;
        this.front = 0;
        this.real = 0;
        this.arr = new int[maxSize];
    }

    /**
     * 判断队列是否已满
     * @return
     */
    public boolean isFull(){
        return (real+1)%maxSize==front;
    }

    public boolean isEmpty(){
        if(real==front)
            return true;
        return false;
    }

    public void addQueue(int n){
        if(isFull()){
            System.out.println("队列已经满了");
        }
        //让real后移动
        arr[real]=n;
        //real 的实际位置必须考虑取模
        real=(real+1)%maxSize;
    }

    /**
     * 出队
     * @return
     */
    public int getQueue(){
        if(isEmpty()){
            throw new RuntimeException("队列为空");
        }
        int res= arr[front];
        front=(front+1)%maxSize;
        return res;
    }

    public void showQueue(){
        if(isEmpty()){
            System.out.println("没有数据");
            return;
        }
        for(int i=front;i<front+size();i++){
            System.out.printf("arr[%d]=%d\n",i%maxSize,arr[i%maxSize]);
        }
    }

    public int size(){
        return (real+maxSize-front)%maxSize;
    }

    public int headQueue(){
        if(isEmpty()){
            throw new RuntimeException("队列为空");
        }
        return arr[front];
    }
}