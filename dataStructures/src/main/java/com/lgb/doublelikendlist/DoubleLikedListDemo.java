package com.lgb.doublelikendlist;


public class DoubleLikedListDemo {
    private HeroNode2 head=new HeroNode2(0,"",null,null);

    public HeroNode2 getHead() {
        return head;
    }

    //遍历链表
    public void list(){
        if(head.next==null){
            System.out.println("链表为空");
            return;
        }
        HeroNode2 temp=head.next;
        while (true){
            if(temp==null){
                break;
            }
            System.out.println(temp);
            temp=temp.next;
        }
    }

    public void addOrder(HeroNode2 heroNode){
        //头节点不能移动
        HeroNode2 temp=head;
        //temp位于加入位置的前一个节点
        boolean flag=false;//添加的编号是否存在

        while(true){
            if(temp.next==null){//temp的下一个节点为null，添加在最后即可
                break;
            }
            temp=temp.next;
        }
        temp.next=heroNode;
        heroNode.prev=temp;
    }

    public void update(HeroNode2 newHeroNode){
        if(head.next==null)
            System.out.println( "null");

        HeroNode2 temp=head.next;
        boolean flag=false;

        while (true){
            if (temp ==null)
                break;

            if(temp.no==newHeroNode.no){
                flag=true;
                break;
            }
            temp=temp.next;
        }
        if(flag){
            temp.nickename=newHeroNode.nickename;
        }
    }

    public void del(int no){
        HeroNode2 temp=head;
        boolean flag=false;

        while (true){
            if(temp.next==null)
                break;
            if(temp.no==no){
                flag=true;
                break;
            }
            temp=temp.next;
        }
        if(flag){
            temp.prev.next=temp.next;
            if(temp.next!=null)
                temp.next.prev=temp.prev;
        }
    }

}
//创建
class HeroNode2{
    public int no;
    public String nickename;
    public HeroNode2 next;//
    public HeroNode2 prev;

    public HeroNode2(int no, String nickename, HeroNode2 next, HeroNode2 prev) {
        this.no = no;
        this.nickename = nickename;
        this.next = next;
        this.prev = prev;
    }
}