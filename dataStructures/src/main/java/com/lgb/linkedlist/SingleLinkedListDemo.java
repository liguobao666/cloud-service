package com.lgb.linkedlist;

public class SingleLinkedListDemo {
    public static void main(String[] args) throws InterruptedException {
        SingleLinkedList list=new SingleLinkedList();

        HeroNode n1=new HeroNode(1,"张三");
        HeroNode n2=new HeroNode(2,"李四");
        HeroNode n3=new HeroNode(3,"王五");
        HeroNode n4=new HeroNode(4,"赵六");

        list.addOrder(n1);
        list.addOrder(n4);
        list.addOrder(n2);
        list.addOrder(n3);


        Thread thread = new Thread();
        thread.start();
        thread.join();

        list.list();
    }
}

/**
 * 定义以个单链表
 */
class SingleLinkedList{
    private HeroNode head=new HeroNode(0,"");

    /**
     * 链表尾部添加节点
     * @param heroNode
     */
    public void add(HeroNode heroNode){
        HeroNode temp=head;
        while (true){
            if(temp.next==null){
                break;
            }
            temp=temp.next;
        }
        temp.next=heroNode;
    }


    /**
     * 顺序添加节点
     * @param heroNode
     */
    public void addOrder(HeroNode heroNode){
        //头节点不能移动
        HeroNode temp=head;
        //temp位于加入位置的前一个节点
        boolean flag=false;//添加的编号是否存在

        while(true){
            if(temp.next==null){//temp的下一个节点为null，添加在最后即可
                break;
            }
            if(temp.next.no>heroNode.no){
                break;
            }else if(temp.next.no==heroNode.no){
                flag=true;
                break;
            }
            temp=temp.next;
        }
        if(flag){
            System.out.println("存在相同的节点");
            return;
        }
        heroNode.next=temp.next;
        temp.next=heroNode;
    }

    public void list(){
        if(head.next==null){
            System.out.println("链表为空");
            return;
        }
        HeroNode temp=head;
        while (true){
            if(temp==null){
                break;
            }
            System.out.println(temp);
            temp=temp.next;
        }
    }


    /**
     * 删除一个节点
     * @param no
     */
    public void del(int no){
        HeroNode temp = head;
        boolean flag=false;//找到节点的标记
        while (true){
            if(temp.next==null){
                System.out.println("节点不存在");
                break;
            }
            if(temp.next.no==no){//节点找到
                flag=true;
                break;
            }
            temp=temp.next;
        }
        if(flag){
            temp.next=temp.next.next;
        }else {
            System.out.println("节点不存在");
        }
    }

    /**
     * 链表中有效节点的个数
     * @param head
     * @return
     */
    public static int getLength(HeroNode head){
        if(head.next==null){
            return 0;
        }
        int length=0;
        HeroNode cur=head.next;
        while (cur!=null){
            length++;
            cur=cur.next;
        }
        return length;
    }


}
class HeroNode{
    public  int no;
    public  String name;
    public  HeroNode next;

    public HeroNode(int no, String name) {
        this.no = no;
        this.name = name;
    }

    @Override
    public String toString() {
        return "HeroNode{" +
                "no=" + no +
                ", name='" + name + '\'' +
                ", "+
                '}';
    }
}
