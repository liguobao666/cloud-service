package com.it.test;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class MyBatisTest {

    /**
     * 一级缓存：1相同的sql和参数  2相同session，3相同的方法 4相同的mapper（namespace）
     *              5.所有的update,insert,delete语句都会删除整个缓存
     * 二级缓存：静态表，参数表很少修改。
     *         1 配置CacheNamespace注解
     *         2 只有再会话关闭时才会去设置二级缓存（为什么？因为事务还没提交不能被其它事务看到，不能够破坏食物的隔离级别）
     *         3 相同的方法
     *         4 相同的namespace
     *         5 不能再查询前执行clearCache
     *         6 不能执行任何的增删改
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        //最后返回DefaultSqlSessionFactory 此方法实现了 SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            UserMapper mapper = sqlSession.getMapper(UserMapper.class);
            User user = mapper.getUserById(1);
            System.out.println(user);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

}
