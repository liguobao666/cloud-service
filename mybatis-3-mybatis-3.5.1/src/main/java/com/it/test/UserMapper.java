package com.it.test;

import org.apache.ibatis.annotations.CacheNamespace;

/**
 * 二级缓存
 */
@CacheNamespace
public interface UserMapper {
    User getUserById(int id);
}
